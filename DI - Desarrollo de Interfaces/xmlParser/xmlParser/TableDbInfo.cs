﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xmlParser
{
    class TableDbInfo
    {
        public String name,xml;
        public LinkedList<String> originalCols = new LinkedList<String>();
        public LinkedList<String> toDeleteCols = new LinkedList<String>();
        public Boolean enabled = true;
        TableDbInfo() { }

        public TableDbInfo(String name)
        {
            this.name = name;
        }

        public void addOriginalCols(String col)
        {
            this.originalCols.AddLast(col);
        }
        public void addToDeleteCols(String col)
        {
            this.toDeleteCols.AddLast(col);
        }

        public void removeOriginalCols(String node)
        {
            this.originalCols.Remove(this.originalCols.Find(node));
        }
        public void removeToDeleteCols(String node)
        {
            this.toDeleteCols.Remove(this.toDeleteCols.Find(node));
        }

        public void setXml(String xml)
        {
            this.xml = xml;
        }
    }
}
