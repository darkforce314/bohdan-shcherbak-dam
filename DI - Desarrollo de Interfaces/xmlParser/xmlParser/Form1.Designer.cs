﻿namespace xmlParser
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputText = new System.Windows.Forms.TextBox();
            this.abrirModal = new System.Windows.Forms.OpenFileDialog();
            this.openFile = new System.Windows.Forms.Button();
            this.filename = new System.Windows.Forms.Label();
            this.radioAll = new System.Windows.Forms.RadioButton();
            this.radioTags = new System.Windows.Forms.RadioButton();
            this.exportFile = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.xmlLabel = new System.Windows.Forms.Label();
            this.openXmlBtn = new System.Windows.Forms.Button();
            this.columnasList = new System.Windows.Forms.ListBox();
            this.guardarModal = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputText
            // 
            this.outputText.Location = new System.Drawing.Point(207, 13);
            this.outputText.Multiline = true;
            this.outputText.Name = "outputText";
            this.outputText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputText.Size = new System.Drawing.Size(287, 248);
            this.outputText.TabIndex = 0;
            // 
            // abrirModal
            // 
            this.abrirModal.FileName = "openFileDialog1";
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(13, 13);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 23);
            this.openFile.TabIndex = 1;
            this.openFile.Text = "Abrir BD";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // filename
            // 
            this.filename.AutoSize = true;
            this.filename.Location = new System.Drawing.Point(94, 18);
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(57, 13);
            this.filename.TabIndex = 2;
            this.filename.Text = "Sin fichero";
            // 
            // radioAll
            // 
            this.radioAll.AutoSize = true;
            this.radioAll.Checked = true;
            this.radioAll.Location = new System.Drawing.Point(9, 52);
            this.radioAll.Name = "radioAll";
            this.radioAll.Size = new System.Drawing.Size(50, 17);
            this.radioAll.TabIndex = 3;
            this.radioAll.TabStop = true;
            this.radioAll.Text = "Todo";
            this.radioAll.UseVisualStyleBackColor = true;
            this.radioAll.CheckedChanged += new System.EventHandler(this.radioAll_CheckedChanged);
            // 
            // radioTags
            // 
            this.radioTags.AutoSize = true;
            this.radioTags.Location = new System.Drawing.Point(65, 52);
            this.radioTags.Name = "radioTags";
            this.radioTags.Size = new System.Drawing.Size(75, 17);
            this.radioTags.TabIndex = 5;
            this.radioTags.Text = "Seleciónar";
            this.radioTags.UseVisualStyleBackColor = true;
            // 
            // exportFile
            // 
            this.exportFile.Location = new System.Drawing.Point(13, 43);
            this.exportFile.Name = "exportFile";
            this.exportFile.Size = new System.Drawing.Size(188, 23);
            this.exportFile.TabIndex = 6;
            this.exportFile.Text = "Exportar a XML";
            this.exportFile.UseVisualStyleBackColor = true;
            this.exportFile.Click += new System.EventHandler(this.exportFile_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.xmlLabel);
            this.groupBox1.Controls.Add(this.openXmlBtn);
            this.groupBox1.Controls.Add(this.columnasList);
            this.groupBox1.Controls.Add(this.radioAll);
            this.groupBox1.Controls.Add(this.radioTags);
            this.groupBox1.Location = new System.Drawing.Point(12, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 189);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuración de la Exportación";
            // 
            // xmlLabel
            // 
            this.xmlLabel.AutoSize = true;
            this.xmlLabel.Location = new System.Drawing.Point(90, 27);
            this.xmlLabel.Name = "xmlLabel";
            this.xmlLabel.Size = new System.Drawing.Size(57, 13);
            this.xmlLabel.TabIndex = 15;
            this.xmlLabel.Text = "Sin fichero";
            // 
            // openXmlBtn
            // 
            this.openXmlBtn.Location = new System.Drawing.Point(9, 20);
            this.openXmlBtn.Name = "openXmlBtn";
            this.openXmlBtn.Size = new System.Drawing.Size(75, 26);
            this.openXmlBtn.TabIndex = 14;
            this.openXmlBtn.Text = "Abrir XML";
            this.openXmlBtn.UseVisualStyleBackColor = true;
            this.openXmlBtn.Click += new System.EventHandler(this.openXmlBtn_Click);
            // 
            // columnasList
            // 
            this.columnasList.Enabled = false;
            this.columnasList.FormattingEnabled = true;
            this.columnasList.Location = new System.Drawing.Point(9, 80);
            this.columnasList.Name = "columnasList";
            this.columnasList.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.columnasList.Size = new System.Drawing.Size(173, 95);
            this.columnasList.TabIndex = 13;
            this.columnasList.SelectedIndexChanged += new System.EventHandler(this.columnasList_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 273);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.exportFile);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.outputText);
            this.Name = "Form1";
            this.Text = "Access2XML";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox outputText;
        private System.Windows.Forms.OpenFileDialog abrirModal;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.Label filename;
        private System.Windows.Forms.RadioButton radioAll;
        private System.Windows.Forms.RadioButton radioTags;
        private System.Windows.Forms.Button exportFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox columnasList;
        private System.Windows.Forms.FolderBrowserDialog guardarModal;
        private System.Windows.Forms.Label xmlLabel;
        private System.Windows.Forms.Button openXmlBtn;
    }
}

