﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace xmlParser
{
    public partial class Form1 : Form
    {
        String fileRoute,xmlRoute;
        String ConnStr;
        LinkedList<String> virtualDb = new LinkedList<String>();
        OleDbConnection connection;
        XmlDocument docOriginal;

        public Form1()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            abrirModal.Title = "Abrir fichero de Base de Datos Access";
            abrirModal.Filter = "Access Database|*.mdb";
            abrirModal.InitialDirectory = @"C:\";
            if (abrirModal.ShowDialog() == DialogResult.OK)
            {
                this.fileRoute = abrirModal.FileName.ToString();
                this.ConnStr = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + fileRoute + ";User Id=admin;Password=;";
                filename.Text = abrirModal.SafeFileName;
                getInfo();
            }
        }

        private void getInfo(){
            DataTable userTables = null;
            connection = new OleDbConnection(ConnStr);
            string[] restrictions = new string[4];
            restrictions[3] = "Table";
            connection.Open();
            userTables = connection.GetSchema("Tables",restrictions);
            for (int i = 0; i < userTables.Rows.Count; i++)
            {
                String tableName = userTables.Rows[i][2].ToString();
                virtualDb.AddLast(tableName);                
            }
            if (virtualDb.Count > 0)
            {
                exportFile.Enabled = true;
            }
            connection.Close();
        }

        private void radioAll_CheckedChanged(object sender, EventArgs e)
        {
            if (radioAll.Checked == true)
            {
                columnasList.Enabled = false;
                if (xmlRoute  != null)
                {
                    showDoc(this.docOriginal);
                }
            }
            else
            {
                columnasList.Enabled = true;
                if(xmlRoute != null)
                {
                    showDoc(filterDoc());
                }
            }
        }

        private void exportFile_Click(object sender, EventArgs e)
        {
            if (virtualDb.Count > 0)
            {
                String folderRoute = "";
                DialogResult result = guardarModal.ShowDialog();
                if( result == DialogResult.OK )
                {
                    folderRoute = guardarModal.SelectedPath;
                    
                    connection = new OleDbConnection(ConnStr);
                    connection.Open();
                    OleDbDataAdapter adapter = new OleDbDataAdapter();
                    foreach (String tab in virtualDb)
                    {
                        DataSet dataSet = new DataSet(tab);
                        DataTable data = new DataTable(tab.Substring(0, tab.Length - 1));
                        OleDbCommand commandselectA = new OleDbCommand("SELECT * FROM [" + tab + "]", connection);
                        adapter.SelectCommand = commandselectA;
                        adapter.Fill(data);
                        dataSet.Tables.Add(data);
                        dataSet.WriteXml(folderRoute + "/" + tab + ".xml");
                    }
                }
            }
        }

        private void columnasList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioAll.Checked == false)
            {
                showDoc(filterDoc());
            }
        }

        private void openXmlBtn_Click(object sender, EventArgs e)
        {
            abrirModal.Title = "Abrir fichero XML";
            abrirModal.Filter = "XML|*.xml";
            abrirModal.InitialDirectory = @"C:\";
            if (abrirModal.ShowDialog() == DialogResult.OK)
            {
                columnasList.Items.Clear();
                this.xmlRoute = abrirModal.FileName.ToString();
                xmlLabel.Text = abrirModal.SafeFileName;
                this.docOriginal = new XmlDocument();
                this.docOriginal.Load(abrirModal.FileName);
                foreach (XmlElement element in this.docOriginal.DocumentElement.ChildNodes[0].ChildNodes)
                {
                    columnasList.Items.Add(element.Name);
                }
                showDoc(this.docOriginal);
            }
        }

        private void selectAllCols()
        {
            for (int i = 0; i < columnasList.Items.Count; i++)
            {
                columnasList.SetSelected(i, true);
            }
        }

        private XmlDocument filterDoc()
        {
            XmlDocument doc = (XmlDocument)this.docOriginal.Clone();
            foreach (XmlElement element in doc.DocumentElement.ChildNodes)
            {
                int x = 0;
                for (int i = 0; i < columnasList.Items.Count; i++)
                {
                    if (columnasList.GetSelected(i) == false)
                    {
                        if (element.ChildNodes[i-x] != null)
                        {
                            element.RemoveChild(element.ChildNodes[i-x]);
                            x++;
                        }
                    }
                }
            }
            return doc;
        }

        private void showDoc(XmlDocument doc)
        {
            XPathNavigator nav = doc.CreateNavigator();
            String xml = "";
            XPathNodeIterator nodes = nav.Select("/" + doc.DocumentElement.Name);
            while (nodes.MoveNext())
            {
                xml += nodes.Current.OuterXml;
                xml += Environment.NewLine;
            }
            outputText.Text = xml;
        }
    }
}
