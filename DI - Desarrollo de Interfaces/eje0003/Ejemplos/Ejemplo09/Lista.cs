using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo09
{
    class ListaCadenas
    {
        const int TAM_POR_DEFECTO=100;
        const string VALOR_ERROR = "*** ERROR ***";

        int nElem;
        string []items;

        public ListaCadenas()
        {
           items=new string[TAM_POR_DEFECTO];
        }

        public ListaCadenas(int size)
        {
            items=new string[size];
        }

        /// <summary>
        /// Crea una copia de una lista
        /// </summary>
        /// <param name="otra">Lista de la que copiamos la informacion</param>
        public ListaCadenas(ListaCadenas otra)
        { // Copia el contenido de otra lista en una nueva lista
            items = new string[otra.items.Length];
            nElem = otra.nElem;
            for(int i=0; i<nElem; i++)
            {
                items[i] = otra.items[i];
            }
        }

        /// <summary>
        ///   Inserta un elemento a la lista
        /// </summary>
        /// <param name="unElemento">Elemento a insertar</param>
        /// <returns>True si se ha insertado false en caso contrario</returns>
        public bool Inserta(string unElemento)
        {
            if (nElem < items.Length)
            {
                items[nElem++] = unElemento;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        ///  Borra el elemento que est� en la posici�n posElem y modifica el orden
        ///  actual de la lista
        /// </summary>
        /// <param name="posElem">Posicion a borrar</param>
        /// <returns>True si se ha borrado false en caso contrario</returns>
        public bool Borra(int posElem)
        {
            if (posElem >= 0 && posElem < nElem)
            {
                // Es una posicion valida
                if (nElem > 1)
                {
                    // Desplazamos una posici�n para abajo todos los que esten despues
                    // de posElem
                    for (int i = posElem; i < nElem - 1; i++)
                    {
                        items[i] = items[i + 1];
                    }
                } // de for
                nElem--; // Tenemos un elemento menos
                return true;
            }
            else
                return false;
        }
        
        /// <summary>
        ///  Busqueda secuencial en una lista NO ORDENADA
        /// </summary>
        /// <param name="clave">Elemento a buscar</param>
        /// <returns>Posici�n del elemento, -1 si no se ha encontrado</returns>
        public int  Busca(string clave )
        {
            int pos; // Posicion en la que se encuentra el elemento
            for (pos = 0; pos < nElem; pos++)
            {
                if (items[pos] == clave)
                {
                    // Se ha encontrado un elemento
                    return pos;
                }
            }
            // Si sale del bucle es que no ha encontrado ning�n elemento
            return -1;
        }

        /// <summary>
        /// Indica si existe o no un elemento
        /// </summary>
        /// <param name="clave">elemento a buscar</param>
        /// <returns>True si existe false en caso contrario</returns>
        public bool Existe(string clave )
        { 
            return Busca(clave)!=-1; 
        }

        /// <summary>
        /// Vacia la lista
        /// </summary>
        public void VaciaLista()
        {
            nElem = 0;
        }


        /// <summary>
        /// Devuelve el n�mero de elementos que tiene la lista
        /// </summary>
        /// <returns></returns>
        public int GetNElem() 
        { 
            return nElem; 
        }

        // Definimos propiedades para obtener el tama�o y el n�mero de elementos 
        // de la lista
        
        /// <summary>
        /// Tama�o m�ximo de la lista (solo lectura)
        /// </summary>
        public int Size
        {
            get
            {
                return items.Length;
            }
        }

        /// <summary>
        /// Numero de elementos que contiene la lista (solo lectura)
        /// </summary>
        public int Count
        {
            get
            {
                return nElem;
            }
        }


        // 
        // INDIZADORES
        //

        /// <summary>
        /// Accede a los elementos de la lista
        /// </summary>
        /// <param name="pos">Posici�n a la que queremos acceder</param>
        /// <returns>Elemento situado en la posici�n pos</returns>
        public string this[int pos]
        {
            set
            {
                if (pos >= 0 && pos < nElem)
                {
                    items[pos] = value;
                } // else no hacemos nada
            }
            get
            {
                // Validamos que la posici�n sea correcta
                if (pos >= 0 && pos < nElem)
                {
                    // Devolvemos el valor situado en la posici�n pos
                    return items[pos];
                }
                else
                {
                    // Un valor erroneo. Lo ideal ser�a lanzar una excepci�n
                    return VALOR_ERROR;
                }
            }
        }

    }
}
