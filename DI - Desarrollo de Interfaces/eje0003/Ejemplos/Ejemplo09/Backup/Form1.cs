﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/* Ejercicio 9
 * 
 *  Programa que utiliza un objeto ListaCadenas para guardar información leida por
    el teclado

    Muestra la información en un TextBox multilinea y va cambiando los botones dependiendo
    del estado
 * 
 *  Ejemplo de como combinar una interfaz y una estructura de datos que almacena los datos
 * 
 * 
 * Contenedor TabedControl:
 *   - Formado por objetos TabbedPage
 *   - Utilizamos evento Enter para actualizar el contenido de la lista
 * 
 * IMAGELIST
 *   - Control que permite almacenar imagenes
 *   - El control botón enlaza con este control a través de la propiedad ImageList
 * 
 * TEXTBOX
 *   - Multiline = true --> Permite tener un cuadro de texto con varias líneas
 *   - ScrollBars --> Muestra las barras de desplazamiento en el cuadro de texto
 */
namespace Ejemplo09
{
    public partial class Form1 : Form
    {
        ListaCadenas lista; // Lista donde almacenaremos los valores
        int pos;            // Posición en la que estamos modificando

        public Form1()
        {
            InitializeComponent();

            // Inicializamos datos

            lista = new ListaCadenas();
            pos = 0;
        }

        void ActualizaEstadoBotones()
        {
           bool noVacia = lista.Count> 0;

            // Cambiamos el color del cuadro de texto que edita los elementos
           if (noVacia)
           {
               textBoxCadModif.BackColor = Color.White;
           }
           else
           {
               textBoxCadModif.BackColor = Color.Gray;
           }
              
            textBoxCadModif.Enabled = noVacia;
           // Boton de modificar
            buttonModificar.Enabled =noVacia;
            buttonBorrar.Enabled =noVacia;
           // Boton anterior
            buttonAnt.Enabled= noVacia && pos > 0;
           // Boton siguiente
            buttonSig.Enabled = noVacia && (pos < lista.Count-1);
        }

        void VerElemento()
        {
            // Actualizamos el número de elementos
            // Si permitimos borrar también tendriamos que actualizar el número allí
            textBoxNElem.Text = lista.Count.ToString();

            // actualizamos el control que muestra la cadena
            textBoxCadModif.Text = lista[pos];

            labelPos.Text = pos.ToString();
        }

        private void textBoxCadena_TextChanged(object sender, EventArgs e)
        {
            // Solo habilitamos el botón cuando tenemos información
            // Quitamos espacios laterales y miramos la longitud
            buttonInsertar.Enabled = textBoxCadena.Text.Trim().Length > 0;
        }

        private void buttonInsertar_Click(object sender, EventArgs e)
        {
                // Insertamos cadena en la lista
            lista.Inserta(textBoxCadena.Text);

            // Actualizamos el estado de los botones
            ActualizaEstadoBotones();

            // Solo es necesario cuando la lista está vacia, pero lo hay que poner
            VerElemento();
        }

        private void buttonSig_Click(object sender, EventArgs e)
        {
            // Este evento solo se activará cuando sea posible
            pos++;
            VerElemento();
            ActualizaEstadoBotones();
        }

        private void buttonAnt_Click(object sender, EventArgs e)
        {
            // Este evento solo se activará cuando sea posible
            pos--;
            VerElemento();
            ActualizaEstadoBotones();
        }

        private void buttonModificar_Click(object sender, EventArgs e)
        {
            // Este evento solo se activará cuando sea posible
            // Modifica en la lista la cadena
            lista[pos] = textBoxCadModif.Text;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            // Actualizamos el estado de los botones al activar el formulario
            // Esta invocación también se podría haber realizado en el contructor
            ActualizaEstadoBotones();
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            // Este evento solo se activará cuando sea posible
            lista.Borra(pos);

            // Si hemos borrado el ultimo tenemos que mover una posición para atrás siempre
            // que no sea el ultimo
            if (pos > 0 && pos > (lista.Count - 1))
            {
                pos--;
            }
            ActualizaEstadoBotones();
            VerElemento();
        }

        private void tabPageLista_Enter(object sender, EventArgs e)
        {
            // Al recibir el foco la la página actualiza la lista
            StringBuilder cad = new StringBuilder();
            for (int i = 0; i < lista.Count; i++)
            {
                if ( i>0)
                {
                    // Insertamos salto de línea
                    cad.Append("\r\n");
                }
                cad.Append(lista[i]);
            }
            textBoxLista.Text = cad.ToString();
        }
    }
}