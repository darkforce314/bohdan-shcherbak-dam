﻿namespace Ejemplo09
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonInsertar = new System.Windows.Forms.Button();
            this.textBoxCadena = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageLista = new System.Windows.Forms.TabPage();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNElem = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPos = new System.Windows.Forms.Label();
            this.buttonAnt = new System.Windows.Forms.Button();
            this.buttonModificar = new System.Windows.Forms.Button();
            this.buttonBorrar = new System.Windows.Forms.Button();
            this.buttonSig = new System.Windows.Forms.Button();
            this.textBoxCadModif = new System.Windows.Forms.TextBox();
            this.textBoxLista = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPageLista.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPageLista);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(467, 242);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.textBoxNElem);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.buttonInsertar);
            this.tabPage1.Controls.Add(this.textBoxCadena);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(459, 216);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Insertar";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonInsertar
            // 
            this.buttonInsertar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInsertar.ImageIndex = 1;
            this.buttonInsertar.ImageList = this.imageList1;
            this.buttonInsertar.Location = new System.Drawing.Point(308, 12);
            this.buttonInsertar.Name = "buttonInsertar";
            this.buttonInsertar.Size = new System.Drawing.Size(106, 23);
            this.buttonInsertar.TabIndex = 2;
            this.buttonInsertar.Text = "Insertar";
            this.buttonInsertar.UseVisualStyleBackColor = true;
            this.buttonInsertar.Click += new System.EventHandler(this.buttonInsertar_Click);
            // 
            // textBoxCadena
            // 
            this.textBoxCadena.Location = new System.Drawing.Point(79, 15);
            this.textBoxCadena.Name = "textBoxCadena";
            this.textBoxCadena.Size = new System.Drawing.Size(223, 20);
            this.textBoxCadena.TabIndex = 1;
            this.textBoxCadena.TextChanged += new System.EventHandler(this.textBoxCadena_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cadena";
            // 
            // tabPageLista
            // 
            this.tabPageLista.Controls.Add(this.textBoxLista);
            this.tabPageLista.Location = new System.Drawing.Point(4, 22);
            this.tabPageLista.Name = "tabPageLista";
            this.tabPageLista.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLista.Size = new System.Drawing.Size(459, 216);
            this.tabPageLista.TabIndex = 1;
            this.tabPageLista.Text = "Lista";
            this.tabPageLista.UseVisualStyleBackColor = true;
            this.tabPageLista.Enter += new System.EventHandler(this.tabPageLista_Enter);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Sample 2.png");
            this.imageList1.Images.SetKeyName(1, "Sample 7.png");
            this.imageList1.Images.SetKeyName(2, "Sample 3.png");
            this.imageList1.Images.SetKeyName(3, "Obj 12.png");
            this.imageList1.Images.SetKeyName(4, "Obj 01.png");
            this.imageList1.Images.SetKeyName(5, "Obj 02.png");
            this.imageList1.Images.SetKeyName(6, "Obj 03.png");
            this.imageList1.Images.SetKeyName(7, "Obj 04.png");
            this.imageList1.Images.SetKeyName(8, "Obj 07.png");
            this.imageList1.Images.SetKeyName(9, "Obj 09.png");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nº de elementos en la lista";
            // 
            // textBoxNElem
            // 
            this.textBoxNElem.Location = new System.Drawing.Point(156, 45);
            this.textBoxNElem.Name = "textBoxNElem";
            this.textBoxNElem.ReadOnly = true;
            this.textBoxNElem.Size = new System.Drawing.Size(56, 20);
            this.textBoxNElem.TabIndex = 4;
            this.textBoxNElem.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBoxCadModif);
            this.panel1.Controls.Add(this.buttonSig);
            this.panel1.Controls.Add(this.buttonBorrar);
            this.panel1.Controls.Add(this.buttonModificar);
            this.panel1.Controls.Add(this.buttonAnt);
            this.panel1.Controls.Add(this.labelPos);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 113);
            this.panel1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Posición actual en la lista";
            // 
            // labelPos
            // 
            this.labelPos.AutoSize = true;
            this.labelPos.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPos.Location = new System.Drawing.Point(173, 9);
            this.labelPos.Name = "labelPos";
            this.labelPos.Size = new System.Drawing.Size(16, 18);
            this.labelPos.TabIndex = 1;
            this.labelPos.Text = "0";
            // 
            // buttonAnt
            // 
            this.buttonAnt.BackColor = System.Drawing.Color.Transparent;
            this.buttonAnt.ImageIndex = 6;
            this.buttonAnt.ImageList = this.imageList1;
            this.buttonAnt.Location = new System.Drawing.Point(23, 41);
            this.buttonAnt.Name = "buttonAnt";
            this.buttonAnt.Size = new System.Drawing.Size(75, 23);
            this.buttonAnt.TabIndex = 2;
            this.buttonAnt.UseVisualStyleBackColor = false;
            this.buttonAnt.Click += new System.EventHandler(this.buttonAnt_Click);
            // 
            // buttonModificar
            // 
            this.buttonModificar.Location = new System.Drawing.Point(124, 41);
            this.buttonModificar.Name = "buttonModificar";
            this.buttonModificar.Size = new System.Drawing.Size(75, 23);
            this.buttonModificar.TabIndex = 3;
            this.buttonModificar.Text = "Modificar";
            this.buttonModificar.UseVisualStyleBackColor = true;
            this.buttonModificar.Click += new System.EventHandler(this.buttonModificar_Click);
            // 
            // buttonBorrar
            // 
            this.buttonBorrar.Location = new System.Drawing.Point(237, 41);
            this.buttonBorrar.Name = "buttonBorrar";
            this.buttonBorrar.Size = new System.Drawing.Size(75, 23);
            this.buttonBorrar.TabIndex = 4;
            this.buttonBorrar.Text = "Borrar";
            this.buttonBorrar.UseVisualStyleBackColor = true;
            this.buttonBorrar.Click += new System.EventHandler(this.buttonBorrar_Click);
            // 
            // buttonSig
            // 
            this.buttonSig.ImageIndex = 7;
            this.buttonSig.ImageList = this.imageList1;
            this.buttonSig.Location = new System.Drawing.Point(348, 41);
            this.buttonSig.Name = "buttonSig";
            this.buttonSig.Size = new System.Drawing.Size(75, 23);
            this.buttonSig.TabIndex = 5;
            this.buttonSig.UseVisualStyleBackColor = true;
            this.buttonSig.Click += new System.EventHandler(this.buttonSig_Click);
            // 
            // textBoxCadModif
            // 
            this.textBoxCadModif.Location = new System.Drawing.Point(24, 70);
            this.textBoxCadModif.Name = "textBoxCadModif";
            this.textBoxCadModif.Size = new System.Drawing.Size(398, 20);
            this.textBoxCadModif.TabIndex = 6;
            // 
            // textBoxLista
            // 
            this.textBoxLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLista.Location = new System.Drawing.Point(3, 3);
            this.textBoxLista.Multiline = true;
            this.textBoxLista.Name = "textBoxLista";
            this.textBoxLista.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLista.Size = new System.Drawing.Size(453, 210);
            this.textBoxLista.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 242);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPageLista.ResumeLayout(false);
            this.tabPageLista.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPageLista;
        private System.Windows.Forms.Button buttonInsertar;
        private System.Windows.Forms.TextBox textBoxCadena;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNElem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelPos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCadModif;
        private System.Windows.Forms.Button buttonSig;
        private System.Windows.Forms.Button buttonBorrar;
        private System.Windows.Forms.Button buttonModificar;
        private System.Windows.Forms.Button buttonAnt;
        public System.Windows.Forms.TextBox textBoxLista;
    }
}

