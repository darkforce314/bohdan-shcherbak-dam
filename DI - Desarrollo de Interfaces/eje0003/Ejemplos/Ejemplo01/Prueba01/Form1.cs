﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Prueba01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Boton_Click(object sender, EventArgs e)
        {
            // Esta función será llamada cuando pulsemos el botón, es la respuesta
            // a un envento

            // MessageBox.Show muestra una ventana con un mensaje.
            // Consulta la ayuda y encontraras más información

            MessageBox.Show(this, "Has pulsado el botón", "Título mensaje", MessageBoxButtons.OK);

        }

        private void label1_Click(object sender, EventArgs e)
        {
            // Cuando tengáis un método vacio, generado como respuesta
            // a un evento, no eliminéis sin más su función
            // Debéis eliminar antes la entrada en el evento correcpondiente
            // Propiedades + Eventos para evitar que al compilar se muestr un error
        }
    }
}