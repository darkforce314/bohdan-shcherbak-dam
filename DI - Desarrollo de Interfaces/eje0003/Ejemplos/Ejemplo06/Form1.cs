﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo06
{
    /* Notas de interés:
     * 
     * El control "imageList1" guarda la imagen que se muestra en el botón.
     * La propiedades "imageXXXX" del botón paramétrizan la imagen en el botón
     * 
     * TEXTBOX
     *  Los textBox con los números convertidos tienen puesta la propiedad "ReadOnly" a true para no 
     * permitir que el usuario modifique su valor.
     * 
     *  La propiedad textAlign de los textBox fija la alineación del texto
     * 
     *  Con el evento KeyPress controlamos los valores permitidos
     * 
     *  Con el evento TextChange filtramos el contenido de un textBox
     * 
     * FORMULARIO
     * La propiedad "text" del formulario establece el título de la ventana
     * FormBorderStyle --> Establece el tipo de borde
     * MaximizeBox --> Impide que se muestre el botón de maximizar
     */
    public partial class Form1 : Form
    {
        string antValor = ""; // Valor del textBox antes de la modificación
        

        public Form1()
        {
            InitializeComponent();
        }

        private void textNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Metodo asociado a los eventos KeyPress de ambos textBox
            //
            // Ignoramos pulsación de teclas que no sean números. En el parametro 'e' recibimos la tecla
            // que han pulsado, si no es la que nos interesa la sustituimos por el caracter nulo
            //
            if ((e.KeyChar < '0' || e.KeyChar > '9') && e.KeyChar != '\n' && e.KeyChar != '\b')
            {
                e.KeyChar = '\0';
            }
        }

        /// <summary>
        /// Devuelve el caracter (código) que representa un dígito
        /// </summary>
        /// <param name="digito">Valor numérico del dígito</param>
        /// <returns>Valor del caracter</returns>
        private char GetDigito(int digito)
        {
            if (digito < 10)
            {
                // Es un número
                return (char)('0' + digito);
            } else {
                // Es una letra
                return (char)('A' + digito - 10);
            }
        }
        /// <summary>
        /// Devuelve la cadena que representa a un número en una base indicada
        /// </summary>
        /// <param name="num">Número a mostrar</param>
        /// <param name="b">Base de numeración</param>
        /// <returns></returns>
        private string Itoa(int num, int baseCambio)
        {
            // Convierte un entero a la base indicada
            if (baseCambio <= 0)
            {
                return "*ERROR*";
            }

            StringBuilder resuReves = new StringBuilder();
            while (num > 0)
            {
                resuReves.Append(GetDigito(num % baseCambio));
                num /= baseCambio;
            }
            // En resuReves tenemos el número al reves
            StringBuilder resu = new StringBuilder();
            for (int i = resuReves.Length-1; i >= 0; i--)
            {
                resu.Append(resuReves[i]);
            }
            return resu.ToString();
           
        }

        private void textOtraBase_TextChanged(object sender, EventArgs e)
        {
            // Este evento se dispara cada vez que cambia el textBox
            if (textOtraBase.Text.Length == 0)
            {
                // Campo vacio no hacemos nada
                return;
            }

            int baseCambio = int.Parse(textOtraBase.Text);
            if (baseCambio > 30)
            {
                // ERROR base mayor de que la soportada
                MessageBox.Show("Error. La base debe ser menor o igual que 30", "Información", MessageBoxButtons.OK);
                textOtraBase.Text = antValor;
            }
            antValor = textOtraBase.Text;
        }

        private void buttonConvertir_Click(object sender, EventArgs e)
        {
            // Método que se dispará al pulsar el botón "Convertir"
            int baseCambio = 0;
            if (textOtraBase.Text.Length > 0)
            {
                baseCambio = int.Parse(textOtraBase.Text);
            }
            int num = 0;
            if (textNumero.Text.Length > 0)
            {
                num = int.Parse(textNumero.Text);
            }
            // El evento TextChanged se dispara cada vez que se modifica el textBox
            textBinario.Text = Itoa(num, 2);
            textOctal.Text = Itoa(num, 8);
            textHexadecimal.Text = Itoa(num, 16);
            textOtra.Text = Itoa(num, baseCambio);
        }




    }
}