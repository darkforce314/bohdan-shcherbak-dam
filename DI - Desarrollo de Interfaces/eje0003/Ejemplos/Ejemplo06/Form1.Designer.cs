﻿namespace Ejemplo06
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textOtraBase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonConvertir = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.textNumero = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textOtra = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textHexadecimal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textOctal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBinario = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.textOtraBase);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonConvertir);
            this.panel1.Controls.Add(this.textNumero);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 53);
            this.panel1.TabIndex = 0;
            // 
            // textOtraBase
            // 
            this.textOtraBase.Location = new System.Drawing.Point(363, 15);
            this.textOtraBase.MaxLength = 2;
            this.textOtraBase.Name = "textOtraBase";
            this.textOtraBase.Size = new System.Drawing.Size(50, 20);
            this.textOtraBase.TabIndex = 4;
            this.textOtraBase.Text = "20";
            this.textOtraBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textOtraBase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumero_KeyPress);
            this.textOtraBase.TextChanged += new System.EventHandler(this.textOtraBase_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Otra base";
            // 
            // buttonConvertir
            // 
            this.buttonConvertir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonConvertir.ImageIndex = 0;
            this.buttonConvertir.ImageList = this.imageList1;
            this.buttonConvertir.Location = new System.Drawing.Point(180, 12);
            this.buttonConvertir.Name = "buttonConvertir";
            this.buttonConvertir.Size = new System.Drawing.Size(98, 23);
            this.buttonConvertir.TabIndex = 2;
            this.buttonConvertir.Text = "Convertir";
            this.buttonConvertir.UseVisualStyleBackColor = true;
            this.buttonConvertir.Click += new System.EventHandler(this.buttonConvertir_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "PC03D.ICO");
            // 
            // textNumero
            // 
            this.textNumero.Location = new System.Drawing.Point(68, 15);
            this.textNumero.MaxLength = 6;
            this.textNumero.Name = "textNumero";
            this.textNumero.Size = new System.Drawing.Size(92, 20);
            this.textNumero.TabIndex = 1;
            this.textNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textNumero_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.textOtra);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.textHexadecimal);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.textOctal);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.textBinario);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 53);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(518, 60);
            this.panel2.TabIndex = 1;
            // 
            // textOtra
            // 
            this.textOtra.Location = new System.Drawing.Point(415, 30);
            this.textOtra.Name = "textOtra";
            this.textOtra.ReadOnly = true;
            this.textOtra.Size = new System.Drawing.Size(100, 20);
            this.textOtra.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(414, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Otra";
            // 
            // textHexadecimal
            // 
            this.textHexadecimal.Location = new System.Drawing.Point(302, 30);
            this.textHexadecimal.Name = "textHexadecimal";
            this.textHexadecimal.ReadOnly = true;
            this.textHexadecimal.Size = new System.Drawing.Size(100, 20);
            this.textHexadecimal.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Hexadecimal";
            // 
            // textOctal
            // 
            this.textOctal.Location = new System.Drawing.Point(196, 30);
            this.textOctal.Name = "textOctal";
            this.textOctal.ReadOnly = true;
            this.textOctal.Size = new System.Drawing.Size(100, 20);
            this.textOctal.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(195, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Octal";
            // 
            // textBinario
            // 
            this.textBinario.Location = new System.Drawing.Point(3, 30);
            this.textBinario.Name = "textBinario";
            this.textBinario.ReadOnly = true;
            this.textBinario.Size = new System.Drawing.Size(187, 20);
            this.textBinario.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Binario";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 113);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Convertir números";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonConvertir;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TextBox textNumero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textOtraBase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textHexadecimal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textOctal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBinario;
        private System.Windows.Forms.TextBox textOtra;
        private System.Windows.Forms.Label label6;
    }
}

