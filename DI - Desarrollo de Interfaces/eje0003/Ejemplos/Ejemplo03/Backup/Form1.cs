﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/*  EJEMPLO 3
   Programa que muestra una ficha con dos botones y en un componente Label
   nos muestra un mensaje indicando que botón se ha pulsado la última vez.

*/
namespace UltimoBoton
{
    public partial class Form1 : Form
    {
        // Almacenamos el texto que tiene el botón que vamos a modificar
        string textoBoton=null;
        // Ultimo boton pulsado
        Button ultBoton = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void boton_Click(object sender, EventArgs e)
        {
            // Esta función será llamada cuando pulsemos calquiera de los tres botónes
            // El evento Click apunta a este método
            // Sender nos indicará el botón pulsado

            // labelMsg es el control de Tipo Label

            // Modificamos la propiedad en tiempo de ejecución

            int nBoton = 0;      // Número de botón pulsado

            // sender es una referencia igual que Boton1, Boton2, Boton3
            if (sender == boton1)
                nBoton = 1;
            else if (sender == boton2)
                nBoton = 2;
            else
                nBoton = 3;

            // Función IntToStr convierte un entero a AnsiString, lo devuelve como resultado

            labelMsg.Text = "Pulsado el botón " + nBoton;

            // Convertirmos 'sender' al tipo Button
            Button btnPulsado = sender as Button;
            if (textoBoton != null)
            {
                // Restauramos el texto del antiguo botón
                ultBoton.Text = textoBoton;
            }
            textoBoton = btnPulsado.Text;
            btnPulsado.Text = "Pulsado";
            ultBoton = btnPulsado;
       }
    }
}