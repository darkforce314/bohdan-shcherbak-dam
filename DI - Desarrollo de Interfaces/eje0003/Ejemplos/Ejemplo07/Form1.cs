﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
/*
 * Ejemplo 7
 * Uso de los enventos "Enter", "Leave"
 * 
 * LABEL
 * - Propiedad Dock: para alinear un control con su contenedor
 * 
 * FORMULARIO
 * - FormBorderStyle: FixedDialog
 * - Como cancelar el cierre de un formulario. Evento "Closing"
 * 
 * TEXTBOX
 * - Todos los controles comparten el método para los eventos "Enter", "Leave"
 * - La propiedad TabIndex indica el orden en el que nos movemos por los
 *   controles cuando pulsamos la tecla tabulador
 * 
 **/
namespace Ejemplo07
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Color que tenía al botón antes de entrar
        /// </summary>
        Color antColor;
        /// <summary>
        /// Fuente que tenía el control antes de entrar
        /// </summary>
        Font antFont;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            // Cerramos el formulario, como es el principal finaliza la aplicación
            Close();

            
            // Un metodo más efectivo para finalizar la aplicación
            // desde cualquier lugar es invocar el método exit de la
            // clase Application
            
            // Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Preguntamos si desea salir
            
            // Preguntamos Si o No y en función de la respuesta permitimos 
            // o no cerrar la ventana
            e.Cancel = MessageBox.Show(
                "¿Desea salir de la apliación?", 
                "Pregunta",
                MessageBoxButtons.YesNo) == DialogResult.No;
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            // Obtenemos el cuadro de texto en el que entramos
            // sender apunta al objeto que ha disparado el evento
            TextBox tb = sender as TextBox;
            
            antColor = tb.BackColor;    // Guardamos el color

            tb.BackColor = Color.White; // Ponemos fondo blanco

            //  Ponemos fuente en negrita
            antFont = tb.Font;
            tb.Font = new Font(tb.Font, FontStyle.Bold);
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            // Obtenemos el cuadro de texto en el que entramos
            TextBox tb = sender as TextBox;

            tb.BackColor = antColor; // Restituimos color

            // Quitamos negrita
            // Restauramos fuente a valor anterior
            tb.Font = antFont;

        }
    }
}