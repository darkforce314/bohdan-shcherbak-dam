using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ejemplo12
{
    public partial class Form3 : Form
    {

        // Formulario que ha creado este formulario
        Form1 padre;
        int nForm;


        public Form3()
        {
            InitializeComponent();
        }

        // Devuelve el n�mero del formulario
        public int Num
        {
            get { return nForm; }
        }

        public void Inicializa(Form1 padre, int nForm)
        {
            // Inicializamos campos que identifican al formulario
            this.padre = padre;
            this.nForm = nForm;
            

            // Cambiamos el tit�lo de la ventana
            Text = String.Format("{0} - {1}", Text, nForm);
            labelId.Text = nForm.ToString();
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Al cerrar comunicamos al padre que nos cerramos
            padre.CerradoFormulario3(nForm);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            padre.BringToFront();
        }
    }
}