using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ejemplo12
{
    public partial class Form2 : Form
    {
        // Formulario que ha creado este formulario
        Form1 padre;

        public Form1 Padre
        {
            get { return padre; }
            set { padre = value; }
        }


        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Mostramos el formulario que ha creado este formulario
            padre.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = padre.TextoEnTextBox;
        }


        private void radioButtonRelojArena_Click(object sender, EventArgs e)
        {
            // Metodo asociado a los eventos Clic del RadioButton

            // La propiedad UseWaitCursor del la clase Application 
            // fija el tipo de cursor para todas las ventanas abiertas
            // de la aplicación
            Application.UseWaitCursor = radioButtonRelojArena.Checked;
        }
    }
}