﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/* Ejemplo 12 - Proyectos con más de un formulario
 * 
 * Creación de otros formularios y comunicación entre ellos
 * 
 * 
 * */
namespace ejemplo12
{
    public partial class Form1 : Form
    {
        //
        // Estrutura que me permitirá almacenar en la lista la referencia a los formularios
        // Tipo definido dentro de la clase, además es privado. No se podrá utilizar fuera de la clase
        //
        struct ElemListBox
        {
            Form3 frm;

            // Constructor
            public ElemListBox(Form3 frm)
            {
                this.frm = frm;
            }

            // Propiedad de solo lectura
            public Form3 Form
            {
                get { return frm; }
            }

            //
            // Sobreescribimos el método para que muestre información en la ListBox
            //
            public override string ToString()
            {
                return frm.Text;
            }
      
        }


        // Objeto Form2, solo tendremos un formulario de este tipo
        // Para hacer que dos formularios se comuniquen tendremos que tener referencias en ellos
        Form2 form2; 

        // Observerse que del Formulario 3 vamos a tener unos cuantos

        int n = 0;

        public Form1()
        {
            InitializeComponent();
        }

        // Propiedad que permitirá acceder al valor del text box
        public string TextoEnTextBox
        {
            get { return textBox.Text; }
            set { textBox.Text = value; }
        }

        // Metodo que utilizará el formulario 3 para comunicar que se ha cerrado 
        public void CerradoFormulario3(int n)
        {
            // Borramos de la lista, el formulario inidicado
            // Tendremos que buscarlo
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                // Convertimos elemento de Object a ElemListBox
                ElemListBox elem = (ElemListBox)listBox.Items[i];
                if (elem.Form.Num == n)
                {
                    listBox.Items.RemoveAt(i);
                    return;
                }
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (form2 == null)
            {
                // Creamos el formulario si aun no ha sido creado
                // solo se crea una vez
                form2 = new Form2();
                form2.Padre = this;
            }
            // Mostramos el formulario y lo traemos al frente
            form2.Show();
            form2.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 frm = new Form3();
            frm.Inicializa(this, ++n);
            listBox.Items.Add(new ElemListBox(frm));
            frm.Show();
            
        }

        private void buttonVer_Click(object sender, EventArgs e)
        {
            if (listBox.SelectedIndex > -1)
            {                
                // Convertimos elemento de Object a ElemListBox
                ElemListBox elem = (ElemListBox)listBox.Items[listBox.SelectedIndex];
                // Mostramos el formulario por si estaba oculto
                elem.Form.Show();
                // Traemos al frente la ventana
                elem.Form.BringToFront();

            
            }
        }

        private void buttonCerrar_Click(object sender, EventArgs e)
        {
            if (listBox.SelectedIndex > -1)
            {
                // Convertimos elemento de Object a ElemListBox
                ElemListBox elem = (ElemListBox)listBox.Items[listBox.SelectedIndex];
                // Cerramos la ventana
                // Observerse que la llamada al metodo close hace que se llame al método "CerradoFormulario3"
                // de este formulario
                elem.Form.Close();
            }
        }

        private void buttonOcultar_Click(object sender, EventArgs e)
        {
            if (listBox.SelectedIndex > -1)
            {
                // Convertimos elemento de Object a ElemListBox
                ElemListBox elem = (ElemListBox)listBox.Items[listBox.SelectedIndex];
                // Ocultamos el formulario
                elem.Form.Hide();

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Mostramos en la barra de estado la carpeta desde la que
            // se está ejecutando el programa
            toolStripStatusLabelRuta.Text = 
                "Ejecutandose en: "  + Application.StartupPath;
        }
    }
}