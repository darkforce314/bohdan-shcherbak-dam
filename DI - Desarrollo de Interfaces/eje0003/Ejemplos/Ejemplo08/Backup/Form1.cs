﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/* Ejemplo 8. Calculadora
 * 
 * PANEL:
 *  - Dock : Ajuste respecto al contenedor
 *  - Padding: Border interior
 *  - Margin: Borde exterior
 * 
 * Button:
 *  - Tag: Propiedad que sirve para almacenar datos propios, en este caso el número
 * 
 * FORMULARIO
 *  - KeyPreview: Hace que el formulario intercepte todas las pulsaciones de teclas
 *  - Evento KeyPress --> Como procesar una tecla y evitar que se envie a otro control
 * 
 * 
 **/
namespace Ejemplo08
{
    public partial class Form1 : Form
    {
        //
        // Tipos definidos dentro de la clase
        //
        enum Estado { calcEdicion, calcResultado };
        enum Operacion { opNada, opMas, opMenos, opPor };

        Estado estado; // Estado en el que se encuentra la calculadora, editando o mostrando un resultado
        Operacion ultop; // Ultima operacion realizada
        long resuIntermedio; // resultado intermedio

        public Form1()
        {
            // Constructor
            InitializeComponent();

            // Inicialización propia
            estado = Estado.calcEdicion;
            ultop = Operacion.opNada;
            resuIntermedio = 0;
        }


        //
        // METODOS de trabajo
        //
        // Realiza las operaciones aritmeticas
        void Opera()
        {
            int num = int.Parse(textBoxResu.Text);
            switch(ultop) {
              case Operacion.opNada:
                  resuIntermedio = num;
                  break;
              case Operacion.opMas:
                  resuIntermedio += num;
                  break;
              case Operacion.opMenos:
                  resuIntermedio -= num;
                  break;
              // Sin terminar
           };
           
           textBoxResu.Text = resuIntermedio.ToString();
           labelMemo.Text = textBoxResu.Text;
           estado = Estado.calcResultado;
        }

        // Inserta un número en el campo EditResu
        void InsertaNumero(char car)
        {
           if ( estado == Estado.calcEdicion ) 
           {
              // Añadimos al final salvo que solo tengamos el 0
               if (textBoxResu.Text.Length < textBoxResu.MaxLength)
               {
                   if (textBoxResu.Text == "0")
                   {
                       // Sobreescribimos cero a la izquierda
                       textBoxResu.Text = car.ToString();
                   }
                   else
                   {
                       // Añadimos a la derecha el número
                       textBoxResu.Text = textBoxResu.Text + car;
                   }
              }
           } else if ( estado == Estado.calcResultado ) {
               // Borramos resultado e introducimos número
              estado = Estado.calcEdicion;
              textBoxResu.Text = car.ToString();
           }
        }

        //
        // EVENTOS
        //
        private void miSalir_Click(object sender, EventArgs e)
        {
            // Evento de menú Archivo + Salir
            // Salimos de la aplicación
            Application.Exit();
        }

        // Pulsación de un botón de número
        private void buttonNumero_Click(object sender, EventArgs e)
        {
            // La propiedad Tag almacena el valor del botón
            Button boton = sender as Button;
            // Cogemos el primer caracter de la propiedad Tag
            char car = ((string)boton.Tag)[0];
            InsertaNumero(car);
        }

        private void buttonMas_Click(object sender, EventArgs e)
        {
            // Boton mas
            Opera();
            ultop = Operacion.opMas;
            labelOp.Text= "Más";
        }

        private void buttonMenos_Click(object sender, EventArgs e)
        {
            // Boton menos
            Opera();
            ultop = Operacion.opMenos;
            labelOp.Text = "Menos";
        }

        private void buttonIgual_Click(object sender, EventArgs e)
        {
            // Boton igual
            Opera();
            ultop = Operacion.opNada;
            labelOp.Text = "";
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // El formulario tiene activada la propiedad KeyPreview = true
            // cuando pulsen las teclas las interceptamos y realizamos la operacion
            // oportuna
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
                InsertaNumero(e.KeyChar);
            else
            {
                // Código de la tecla INTRO
                const char INTRO = '\r';
                const char BORRAR = '\b';

                // Otras Teclas
                switch (e.KeyChar)
                {
                    case INTRO:
                        buttonIgual_Click(null, null);
                        e.Handled=true; // Desactivamos la tecla para que no llame al botón
                        break;
                    case BORRAR:
                        if (textBoxResu.Text.Length > 0)
                        {
                            textBoxResu.Text = textBoxResu.Text.Substring(0, textBoxResu.Text.Length - 1);
                        }
                        e.Handled = true; // Desactivamos la tecla
                        break;

                    case '+':
                        buttonMas_Click(null, null);
                        e.Handled = true; // Desactivamos la tecla
                        break;
                    // Continuara ...
                }
            }
        }

        private void textBoxResu_TextChanged(object sender, EventArgs e)
        {
            // Opción Copiar del Menú edición

            // Copiamos resultado al portapapeles
            Clipboard.SetText(textBoxResu.Text);
        }
    }
}