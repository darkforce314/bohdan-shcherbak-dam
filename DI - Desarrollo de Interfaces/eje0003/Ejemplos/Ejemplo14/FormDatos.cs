﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/* Ejemplo 14. 
 *   Mostrar formularios como modales o no modales
 * 
 */ 
namespace Ejemplo14
{
    public partial class FormDatos : Form
    {
        FormPersona frmPersona = null;

        // Formulario Modal que mostraremos sin eliminar
        FormPersonaModal frmModal = new FormPersonaModal();

        public FormDatos()
        {
            InitializeComponent();

            // Mostramos algunos nombres en la lista al comenzar
            Inserta("Juan", "López Gutierrez");
            Inserta("Maria", "Pérez Martinez");
            Inserta("Tamara", "Garrido Horan");
        }

        //
        // Metodo que llamará el formulario Persona para indicar que se ha cerrado
        //
        public void CerrandoFormularioPersona()
        {
            // La referencia almacenada ya no es valida pues el formulario se ha eliminado
            frmPersona=null;
        }

        /// <summary>
        /// Inserta un elemento en la lista que muestra personas
        /// </summary>
        /// <param name="nombre">Nombre de la persona</param>
        /// <param name="apellidos">Apellidos de la persona</param>
        public void Inserta(string nombre, string apellidos)
        {
            dataGridView.Rows.Add(new String[] { nombre, apellidos });
        }

        private void buttonInsNoModal_Click(object sender, EventArgs e)
        {
            //
            // MOSTRAMOS DIALOGO COMO NO MODAL
            //
            if (frmPersona == null)
            {
                //
                // Aun no se ha creado el formulario, lo creamos
                // 
                frmPersona = new FormPersona();
                frmPersona.Padre = this; // Comunicamos al formulario quien lo ha creado
            }
            // Mostramos el formulario
            frmPersona.Show();
            frmPersona.BringToFront();
        }

        private void buttonInsertaModal_Click(object sender, EventArgs e)
        {
            //
            // MOSTRAMOS DIALOGO COMO MODAL
            //
            FormPersonaModal frm = new FormPersonaModal();

            // Con el método ShowDialog mostramos el formulario como modal
            if (frm.ShowDialog() == DialogResult.OK)
            {
                // Insertamos datos
                Inserta(frm.Nombre, frm.Apellidos);
            }
        }

        private void buttonModalSinBorrar_Click(object sender, EventArgs e)
        {
            // Al no crear un objeto cada vez que lo llamamos, conservamos los
            // valores del formulario

            // Con el método ShowDialog mostramos el formulario como modal
            if ( frmModal.ShowDialog() == DialogResult.OK)
            {
                // Insertamos datos
                Inserta(frmModal.Nombre, frmModal.Apellidos);
            }            
        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //
            // Doble clic sobre las celdas
            //
            
            //int col = e.ColumnIndex;    // La columna no nos importa pues vamos a copiar toda la celda
            int lin = e.RowIndex;
            if (frmPersona != null)
            {
                // Solo copiamo si el formulario está abierto
                frmPersona.Copia(dataGridView[0, lin].Value.ToString(), dataGridView[1, lin].Value.ToString());
            }
        }
    }
}