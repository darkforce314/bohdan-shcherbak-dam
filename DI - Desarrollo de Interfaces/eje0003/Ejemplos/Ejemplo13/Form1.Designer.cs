﻿namespace Ejemplo13
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miArchivoAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.miArchivoAbrirVarios = new System.Windows.Forms.ToolStripMenuItem();
            this.miArchivoGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miArchivoSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.miFuenteBorrar = new System.Windows.Forms.ToolStripMenuItem();
            this.miPropColor = new System.Windows.Forms.ToolStripMenuItem();
            this.miPropFuente = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listBoxAbrir = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.listBoxGuardar = new System.Windows.Forms.ListBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogVarios = new System.Windows.Forms.OpenFileDialog();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.labelTipoLetra = new System.Windows.Forms.Label();
            this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(545, 37);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(276, 36);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(272, 216);
            this.panel3.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Uso de los cuadros de Dialogo";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.miFuenteBorrar,
            this.miPropColor});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(545, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miArchivoAbrir,
            this.miArchivoAbrirVarios,
            this.miArchivoGuardar,
            this.toolStripMenuItem1,
            this.miArchivoSalir});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            // 
            // miArchivoAbrir
            // 
            this.miArchivoAbrir.Name = "miArchivoAbrir";
            this.miArchivoAbrir.Size = new System.Drawing.Size(140, 22);
            this.miArchivoAbrir.Text = "&Abrir";
            this.miArchivoAbrir.Click += new System.EventHandler(this.miArchivoAbrir_Click);
            // 
            // miArchivoAbrirVarios
            // 
            this.miArchivoAbrirVarios.Name = "miArchivoAbrirVarios";
            this.miArchivoAbrirVarios.Size = new System.Drawing.Size(140, 22);
            this.miArchivoAbrirVarios.Text = "Abrir &varios";
            this.miArchivoAbrirVarios.Click += new System.EventHandler(this.miArchivoAbrirVarios_Click);
            // 
            // miArchivoGuardar
            // 
            this.miArchivoGuardar.Name = "miArchivoGuardar";
            this.miArchivoGuardar.Size = new System.Drawing.Size(140, 22);
            this.miArchivoGuardar.Text = "&Guardar";
            this.miArchivoGuardar.Click += new System.EventHandler(this.miArchivoGuardar_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(137, 6);
            // 
            // miArchivoSalir
            // 
            this.miArchivoSalir.Name = "miArchivoSalir";
            this.miArchivoSalir.Size = new System.Drawing.Size(140, 22);
            this.miArchivoSalir.Text = "&Salir";
            this.miArchivoSalir.Click += new System.EventHandler(this.miArchivoSalir_Click);
            // 
            // miFuenteBorrar
            // 
            this.miFuenteBorrar.Name = "miFuenteBorrar";
            this.miFuenteBorrar.Size = new System.Drawing.Size(53, 20);
            this.miFuenteBorrar.Text = "&Fuente";
            // 
            // miPropColor
            // 
            this.miPropColor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miPropFuente,
            this.colorToolStripMenuItem});
            this.miPropColor.Name = "miPropColor";
            this.miPropColor.Size = new System.Drawing.Size(78, 20);
            this.miPropColor.Text = "&Propiedades";
            // 
            // miPropFuente
            // 
            this.miPropFuente.Name = "miPropFuente";
            this.miPropFuente.Size = new System.Drawing.Size(152, 22);
            this.miPropFuente.Text = "&Fuente";
            this.miPropFuente.Click += new System.EventHandler(this.miPropFuente_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.listBoxAbrir);
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5);
            this.panel2.Size = new System.Drawing.Size(270, 216);
            this.panel2.TabIndex = 2;
            // 
            // listBoxAbrir
            // 
            this.listBoxAbrir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxAbrir.FormattingEnabled = true;
            this.listBoxAbrir.Location = new System.Drawing.Point(5, 5);
            this.listBoxAbrir.Name = "listBoxAbrir";
            this.listBoxAbrir.ScrollAlwaysVisible = true;
            this.listBoxAbrir.Size = new System.Drawing.Size(256, 199);
            this.listBoxAbrir.TabIndex = 0;
            this.listBoxAbrir.SelectedIndexChanged += new System.EventHandler(this.listBoxAbrir_SelectedIndexChanged);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.listBoxGuardar);
            this.panel4.Location = new System.Drawing.Point(275, 60);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(5);
            this.panel4.Size = new System.Drawing.Size(269, 216);
            this.panel4.TabIndex = 3;
            // 
            // listBoxGuardar
            // 
            this.listBoxGuardar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxGuardar.FormattingEnabled = true;
            this.listBoxGuardar.Location = new System.Drawing.Point(5, 5);
            this.listBoxGuardar.Name = "listBoxGuardar";
            this.listBoxGuardar.ScrollAlwaysVisible = true;
            this.listBoxGuardar.Size = new System.Drawing.Size(255, 199);
            this.listBoxGuardar.TabIndex = 0;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Text files (*.txt)|*.txt|Ficheros C#|*.cs|Todos los ficheros (*.*)|*.*";
            // 
            // openFileDialogVarios
            // 
            this.openFileDialogVarios.FileName = "openFileDialog1";
            this.openFileDialogVarios.Filter = "Text files (*.txt)|*.txt|Ficheros C#|*.cs|Todos los ficheros (*.*)|*.*";
            this.openFileDialogVarios.FilterIndex = 3;
            this.openFileDialogVarios.Multiselect = true;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.textBoxNombre);
            this.panel5.Location = new System.Drawing.Point(0, 272);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(548, 30);
            this.panel5.TabIndex = 4;
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Location = new System.Drawing.Point(3, 5);
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(533, 20);
            this.textBoxNombre.TabIndex = 0;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "*.tmp";
            this.saveFileDialog.Filter = "Text files (*.txt)|*.txt|Ficheros C#|*.cs|Todos los ficheros (*.*)|*.*";
            // 
            // labelTipoLetra
            // 
            this.labelTipoLetra.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelTipoLetra.Location = new System.Drawing.Point(0, 305);
            this.labelTipoLetra.Name = "labelTipoLetra";
            this.labelTipoLetra.Size = new System.Drawing.Size(545, 28);
            this.labelTipoLetra.TabIndex = 5;
            this.labelTipoLetra.Text = "Tipo de Letra";
            this.labelTipoLetra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // colorToolStripMenuItem
            // 
            this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
            this.colorToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.colorToolStripMenuItem.Text = "&Color";
            this.colorToolStripMenuItem.Click += new System.EventHandler(this.colorToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 333);
            this.Controls.Add(this.labelTipoLetra);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Cuadros de dialogo";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miArchivoAbrir;
        private System.Windows.Forms.ToolStripMenuItem miArchivoGuardar;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miArchivoSalir;
        private System.Windows.Forms.ToolStripMenuItem miFuenteBorrar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox listBoxAbrir;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ListBox listBoxGuardar;
        private System.Windows.Forms.ToolStripMenuItem miArchivoAbrirVarios;
        private System.Windows.Forms.OpenFileDialog openFileDialogVarios;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Label labelTipoLetra;
        private System.Windows.Forms.ToolStripMenuItem miPropColor;
        private System.Windows.Forms.ToolStripMenuItem miPropFuente;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog;
    }
}

