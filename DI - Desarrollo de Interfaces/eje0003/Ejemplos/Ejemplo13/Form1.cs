﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


/*
 * Ejemplo 13. Uso de Dialogos comunes
 * 
 * OpenDialog
 *   - FileName
 *   - FileNames
 *   - MultiSelect
 *   - Filter
 * 
 * SaveDialog
 *   - FileName
 *   - CheckFileExist
 *   - DefaultExt
 *   - Filter
 * 
 * 
 **/
namespace Ejemplo13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            listBoxAbrir.Items.Add("Elemento 1");
            listBoxGuardar.Items.Add("Elementos 2");
        }

        private void miArchivoAbrir_Click(object sender, EventArgs e)
        {
            // Opción de menu Abrir

            // Observar la propiedad Filter. Ahí indicamos los archivos que se mostrarán

            // Abrimos el formulario
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                // Han pulsado el botón "Abrir" en el Dialogo
                listBoxAbrir.Items.Add(openFileDialog.FileName);
            }
            // else 
            // {
            //  Si no pulsan OK no hacemos nada
            // }
        }

        private void miArchivoAbrirVarios_Click(object sender, EventArgs e)
        {
            // Opción menu Abrir varios

            // El componente openFileDialogVarios tiene activada la propiedad "multiselect"

            // Observar la forma compacta de leer el formulario y mirar su valor devuelto
            if (openFileDialogVarios.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in openFileDialogVarios.FileNames)
                {
                    listBoxAbrir.Items.Add(file);
                }
            }

        }


        private void miArchivoGuardar_Click(object sender, EventArgs e)
        {
            // Opción Guardar del menú

            // Como nombre por defecto seleccionamos el que hay en el cuadro de texto
            saveFileDialog.FileName = textBoxNombre.Text;

            // Observese que si al fichero no le indicamos la extensión, automáticamente se le incluirá
            // la seleccionada en DefaultExt

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // En una aplicación real comprobaríamos que el fichero que estamos guardando 
                // no existiese ya en el disco
                if (System.IO.File.Exists(saveFileDialog.FileName))
                {
                    if (MessageBox.Show(
                            "El fichero ya existe\r\n¿Desea sobreescrirlo?\n\r"+
                            "OJO esta pregunta no es la que hace el cuadro de dialogo por tener activada la propiedad 'OverwritePrompt'",
                            "Pregunta",
                            MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        // El usuario no desea sobreescribir el fichero
                        // salimos de la función y no hacemos nada más
                        return;
                    }
                }
                listBoxGuardar.Items.Add(saveFileDialog.FileName);
            }
        }

        private void listBoxAbrir_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Cambia el elemento seleccionado en la listBox de ficheros abiertos

            // Copiamos al cuadro de texto el nombre de fichero seleccionado, si hay algún elmento seleccionado
            if (listBoxAbrir.SelectedIndex != -1)
            {
                textBoxNombre.Text = listBoxAbrir.Items[listBoxAbrir.SelectedIndex].ToString();
            }
        }


        private void miArchivoSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void miPropFuente_Click(object sender, EventArgs e)
        {
            // Menú fuente

            // Abrimos cuadro de dialogo para mostrar la fuente
            //
            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                Font fuente = fontDialog.Font;

                listBoxAbrir.Font = fuente;
                listBoxGuardar.Font = fuente;
                textBoxNombre.Font = fuente;
                labelTipoLetra.Font = fuente;

                labelTipoLetra.Text = String.Format("{0} ({1})", fuente.Name, fuente.Size);

            }
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Menu Color

            // Abrimos cuadro de dialogo para seleccionar el color
            //
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                Color color = colorDialog.Color;

                listBoxAbrir.ForeColor = color;
                listBoxGuardar.ForeColor = color;
                textBoxNombre.ForeColor = color;
                labelTipoLetra.ForeColor = color;

            }
        }
    }
}