﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ej_comunicacion
{
    public partial class Form2 : Form
    {
        public Form1 padre;
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            padre.TextoEnTextBox = textBox1.Text;
        }

        public string textoentexbox
        {
            get
            {

                return textBox1.Text;
            }
            set
            {

                textBox1.Text = value;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            //padre.BringToFront();
            //SendToBack();
            
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.asignar_null();
        }
    }
}
