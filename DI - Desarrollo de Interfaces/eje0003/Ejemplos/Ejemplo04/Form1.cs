﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            // Disminuimos tamaño de la ventana
            // Propiedades del formulario
            Width -= 10;
            Height -= 10;
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            // Aumentamos tamaño de la ventana
            // Propiedades del formulario
            Width += 10;
            Height += 10;

        }
    }
}