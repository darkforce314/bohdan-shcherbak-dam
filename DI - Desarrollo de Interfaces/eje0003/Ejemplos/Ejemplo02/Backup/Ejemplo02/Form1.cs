﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
/*  EJEMPLO 2
   Programa que muestra una ficha con dos botones y en un componente Label
   nos muestra un mensaje indicando que botón se ha pulsado la última vez.

*/
namespace Ejemplo02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            // En el constructor inicializamos las propiedades 
            // de todos los componentes, este método está definido en 
            // el fichero "Form1.Designer.cs"
            InitializeComponent();
        }

        private void boton1_Click(object sender, EventArgs e)
        {
            // Esta función será llamada cuando pulsemos el botón 1

            // LabelMsg es el control de Tipo Label. Está definido en el Form1.Designer.cs
            // Text es la propiedad que almacena el texto de Label y es de tipo string

            // Modificamos la propiedad en tiempo de ejecución

            labelMsg.Text = "Pulsado el botón 1";
        }

        private void boton2_Click(object sender, EventArgs e)
        {
            labelMsg.Text = "Pulsado el botón 2";
        }

        private void boton3_Click(object sender, EventArgs e)
        {
            labelMsg.Text = "Pulsado el botón 3";
        }
    }
}