﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
/*
 * Ejemplo 17. Utilización del control Timer para crear un Reloj
 * 
 * Objeto Timer
 *   - Enabled -> Activa o desactiva el temporizador
 *   - Interval --> Tiempo en milisegundos, fija el intervalo
 *   - Evento Tick --> Se dispara cuando se agota el temporizador
 * 
 * Objeto Form
 *   - Opacity --> Para hacer fijar opacidad
 *   - TransparencyKey --> Para hacer un color transparente
 * 
 **/
namespace Ejemplo17
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripButtonArrancar_Click(object sender, EventArgs e)
        {
            // Botón Arrancar. Activamos temporizador
            timer.Enabled = true;
            toolStripButtonArrancar.Visible = false;
            toolStripButtonParar.Visible = true;
        }

        private void toolStripButtonParar_Click(object sender, EventArgs e)
        {
            // Botón Parar. Desactivamos el temporizador
            timer.Enabled = false;
            toolStripButtonArrancar.Visible = true;
            toolStripButtonParar.Visible = false;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            // Botón salir
            Application.Exit();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // Evento lanzado por el temporizador

            // Copiamos la hora actual con el formato deseado en la etiqueta
            labelHora.Text = DateTime.Now.ToString("H:mm:ss");
        }
    }
}