﻿namespace Ejemplo17
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonArrancar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonParar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.labelHora = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButtonArrancar,
            this.toolStripButtonParar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(189, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonArrancar
            // 
            this.toolStripButtonArrancar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonArrancar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonArrancar.Image")));
            this.toolStripButtonArrancar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonArrancar.Name = "toolStripButtonArrancar";
            this.toolStripButtonArrancar.Size = new System.Drawing.Size(53, 22);
            this.toolStripButtonArrancar.Text = "Arrancar";
            this.toolStripButtonArrancar.ToolTipText = "Arrancar";
            this.toolStripButtonArrancar.Visible = false;
            this.toolStripButtonArrancar.Click += new System.EventHandler(this.toolStripButtonArrancar_Click);
            // 
            // toolStripButtonParar
            // 
            this.toolStripButtonParar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonParar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonParar.Image")));
            this.toolStripButtonParar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonParar.Name = "toolStripButtonParar";
            this.toolStripButtonParar.Size = new System.Drawing.Size(37, 22);
            this.toolStripButtonParar.Text = "Parar";
            this.toolStripButtonParar.Click += new System.EventHandler(this.toolStripButtonParar_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(31, 22);
            this.toolStripButton1.Text = "Salir";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // labelHora
            // 
            this.labelHora.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHora.Font = new System.Drawing.Font("Arial Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHora.Location = new System.Drawing.Point(0, 25);
            this.labelHora.Name = "labelHora";
            this.labelHora.Size = new System.Drawing.Size(189, 45);
            this.labelHora.TabIndex = 1;
            this.labelHora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(189, 68);
            this.ControlBox = false;
            this.Controls.Add(this.labelHora);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Opacity = 0.65;
            this.Text = "Reloj";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.White;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonArrancar;
        private System.Windows.Forms.ToolStripButton toolStripButtonParar;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Label labelHora;
    }
}

