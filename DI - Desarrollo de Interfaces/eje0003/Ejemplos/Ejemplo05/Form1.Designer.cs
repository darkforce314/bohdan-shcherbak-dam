﻿namespace Ejemplo05
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelFormWidth = new System.Windows.Forms.Label();
            this.labelFormHeight = new System.Windows.Forms.Label();
            this.labelMY = new System.Windows.Forms.Label();
            this.labelMX = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.botonCerrar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.labelMY);
            this.panel1.Controls.Add(this.labelMX);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.labelFormHeight);
            this.panel1.Controls.Add(this.labelFormWidth);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(193, 126);
            this.panel1.TabIndex = 0;
            this.panel1.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(53, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "TAMAÑO";
            // 
            // labelFormWidth
            // 
            this.labelFormWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelFormWidth.Location = new System.Drawing.Point(8, 34);
            this.labelFormWidth.Name = "labelFormWidth";
            this.labelFormWidth.Size = new System.Drawing.Size(61, 21);
            this.labelFormWidth.TabIndex = 1;
            this.labelFormWidth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFormHeight
            // 
            this.labelFormHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelFormHeight.Location = new System.Drawing.Point(110, 34);
            this.labelFormHeight.Name = "labelFormHeight";
            this.labelFormHeight.Size = new System.Drawing.Size(61, 21);
            this.labelFormHeight.TabIndex = 2;
            this.labelFormHeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMY
            // 
            this.labelMY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMY.Location = new System.Drawing.Point(115, 94);
            this.labelMY.Name = "labelMY";
            this.labelMY.Size = new System.Drawing.Size(61, 21);
            this.labelMY.TabIndex = 5;
            this.labelMY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMX
            // 
            this.labelMX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMX.Location = new System.Drawing.Point(13, 94);
            this.labelMX.Name = "labelMX";
            this.labelMX.Size = new System.Drawing.Size(61, 21);
            this.labelMX.TabIndex = 4;
            this.labelMX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(58, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "RATÓN";
            // 
            // botonCerrar
            // 
            this.botonCerrar.Location = new System.Drawing.Point(74, 140);
            this.botonCerrar.Name = "botonCerrar";
            this.botonCerrar.Size = new System.Drawing.Size(75, 23);
            this.botonCerrar.TabIndex = 1;
            this.botonCerrar.Text = "Cerrar";
            this.botonCerrar.UseVisualStyleBackColor = true;
            this.botonCerrar.Click += new System.EventHandler(this.botonCerrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.botonCerrar);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(600, 600);
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ClientSizeChanged += new System.EventHandler(this.Form1_ClientSizeChanged);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelFormWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelMY;
        private System.Windows.Forms.Label labelMX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelFormHeight;
        private System.Windows.Forms.Button botonCerrar;
    }
}

