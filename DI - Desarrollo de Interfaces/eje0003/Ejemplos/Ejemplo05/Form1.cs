﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            // Evento que se dispara con el movimiento del ratón
            // Registramos la posición del ratón en los paneles

            // Copiamos las coordenadas que vienen en el parámetro 'e'
            labelMX.Text = e.X.ToString();
            labelMY.Text = e.Y.ToString();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            // Evento que se dispara al cambiar el tamaño del la ventana
            
            labelFormHeight.Text = Height.ToString();
            labelFormWidth.Text = Width.ToString();

            // Con la propiedad MaximunSize establecemos el tamaño máximo de la ventana
            // Con la propiedad MinimunSize establecemos el tamaño mínimo de la ventana
        }

        private void Form1_ClientSizeChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {

        }

        private void botonCerrar_Click(object sender, EventArgs e)
        {
            // Este método cierra la ficha y finaliza la aplicación
            // si es la única
            Close();
        }
    }
}