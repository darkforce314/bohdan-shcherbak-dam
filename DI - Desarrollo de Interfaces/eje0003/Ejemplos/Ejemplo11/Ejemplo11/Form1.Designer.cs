﻿namespace Ejemplo11
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miOpc1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc1_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc1_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miOpc1_3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.miSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc2_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc2_1_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc2_1_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc2_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc3 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc3_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc3_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpc3_3 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxActOpc2 = new System.Windows.Forms.CheckBox();
            this.checkBoxActOpc1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxOpc2_2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOpc2_1 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxMarcar1_3 = new System.Windows.Forms.CheckBox();
            this.checkBoxMarcar1_2 = new System.Windows.Forms.CheckBox();
            this.checkBoxMarcar1_1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextual1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mensajeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButtonPeras = new System.Windows.Forms.RadioButton();
            this.radioButtonNaranjas = new System.Windows.Forms.RadioButton();
            this.radioButtonMelocotones = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpc1,
            this.miOpc2,
            this.miOpc3});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miOpc1
            // 
            this.miOpc1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpc1_1,
            this.miOpc1_2,
            this.toolStripMenuItem1,
            this.miOpc1_3,
            this.toolStripMenuItem2,
            this.miSalir});
            this.miOpc1.Name = "miOpc1";
            this.miOpc1.Size = new System.Drawing.Size(61, 20);
            this.miOpc1.Text = "Opción 1";
            // 
            // miOpc1_1
            // 
            this.miOpc1_1.Name = "miOpc1_1";
            this.miOpc1_1.Size = new System.Drawing.Size(137, 22);
            this.miOpc1_1.Text = "Opción 1-1";
            this.miOpc1_1.Click += new System.EventHandler(this.miOpc1_1_Click);
            // 
            // miOpc1_2
            // 
            this.miOpc1_2.Name = "miOpc1_2";
            this.miOpc1_2.Size = new System.Drawing.Size(137, 22);
            this.miOpc1_2.Text = "Opción 1-2";
            this.miOpc1_2.Click += new System.EventHandler(this.miOpc1_2_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(134, 6);
            // 
            // miOpc1_3
            // 
            this.miOpc1_3.Name = "miOpc1_3";
            this.miOpc1_3.Size = new System.Drawing.Size(137, 22);
            this.miOpc1_3.Text = "Opción 1-3";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(134, 6);
            // 
            // miSalir
            // 
            this.miSalir.Name = "miSalir";
            this.miSalir.Size = new System.Drawing.Size(137, 22);
            this.miSalir.Text = "&Salir";
            this.miSalir.Click += new System.EventHandler(this.miSalir_Click);
            // 
            // miOpc2
            // 
            this.miOpc2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpc2_1,
            this.miOpc2_2});
            this.miOpc2.Image = global::Ejemplo11.Properties.Resources.ac0036_48;
            this.miOpc2.Name = "miOpc2";
            this.miOpc2.Size = new System.Drawing.Size(77, 20);
            this.miOpc2.Text = "Opción 2";
            // 
            // miOpc2_1
            // 
            this.miOpc2_1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpc2_1_1,
            this.miOpc2_1_2});
            this.miOpc2_1.Name = "miOpc2_1";
            this.miOpc2_1.Size = new System.Drawing.Size(137, 22);
            this.miOpc2_1.Text = "Opcion 2-1";
            // 
            // miOpc2_1_1
            // 
            this.miOpc2_1_1.Name = "miOpc2_1_1";
            this.miOpc2_1_1.Size = new System.Drawing.Size(147, 22);
            this.miOpc2_1_1.Text = "Opción 2-1-1";
            // 
            // miOpc2_1_2
            // 
            this.miOpc2_1_2.Name = "miOpc2_1_2";
            this.miOpc2_1_2.Size = new System.Drawing.Size(147, 22);
            this.miOpc2_1_2.Text = "Opción 2-1-2";
            // 
            // miOpc2_2
            // 
            this.miOpc2_2.Name = "miOpc2_2";
            this.miOpc2_2.Size = new System.Drawing.Size(137, 22);
            this.miOpc2_2.Text = "Opcion 2-2";
            // 
            // miOpc3
            // 
            this.miOpc3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpc3_1,
            this.miOpc3_2,
            this.miOpc3_3});
            this.miOpc3.Image = global::Ejemplo11.Properties.Resources.ei0021_48;
            this.miOpc3.Name = "miOpc3";
            this.miOpc3.Size = new System.Drawing.Size(66, 20);
            this.miOpc3.Text = "Frutas";
            this.miOpc3.Click += new System.EventHandler(this.miOpc3_Click);
            // 
            // miOpc3_1
            // 
            this.miOpc3_1.CheckOnClick = true;
            this.miOpc3_1.Name = "miOpc3_1";
            this.miOpc3_1.Size = new System.Drawing.Size(152, 22);
            this.miOpc3_1.Text = "Peras";
            this.miOpc3_1.ToolTipText = "Pulsa para activar elemento. La propiedad CheckOnClick lo activa automáticamente";
            // 
            // miOpc3_2
            // 
            this.miOpc3_2.Image = global::Ejemplo11.Properties.Resources.wi0054_48;
            this.miOpc3_2.Name = "miOpc3_2";
            this.miOpc3_2.Size = new System.Drawing.Size(152, 22);
            this.miOpc3_2.Text = "Narajas";
            // 
            // miOpc3_3
            // 
            this.miOpc3_3.Image = global::Ejemplo11.Properties.Resources.wi0063_48;
            this.miOpc3_3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.miOpc3_3.Name = "miOpc3_3";
            this.miOpc3_3.Size = new System.Drawing.Size(152, 22);
            this.miOpc3_3.Text = "Melocotones";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxActOpc2);
            this.groupBox1.Controls.Add(this.checkBoxActOpc1);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(122, 78);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Activar";
            // 
            // checkBoxActOpc2
            // 
            this.checkBoxActOpc2.AutoSize = true;
            this.checkBoxActOpc2.Checked = true;
            this.checkBoxActOpc2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxActOpc2.Location = new System.Drawing.Point(12, 49);
            this.checkBoxActOpc2.Name = "checkBoxActOpc2";
            this.checkBoxActOpc2.Size = new System.Drawing.Size(69, 17);
            this.checkBoxActOpc2.TabIndex = 1;
            this.checkBoxActOpc2.Text = "Opción 2";
            this.checkBoxActOpc2.UseVisualStyleBackColor = true;
            this.checkBoxActOpc2.CheckedChanged += new System.EventHandler(this.checkBoxActOpc2_CheckedChanged);
            // 
            // checkBoxActOpc1
            // 
            this.checkBoxActOpc1.AutoSize = true;
            this.checkBoxActOpc1.Checked = true;
            this.checkBoxActOpc1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxActOpc1.Location = new System.Drawing.Point(12, 26);
            this.checkBoxActOpc1.Name = "checkBoxActOpc1";
            this.checkBoxActOpc1.Size = new System.Drawing.Size(69, 17);
            this.checkBoxActOpc1.TabIndex = 0;
            this.checkBoxActOpc1.Text = "Opción 1";
            this.checkBoxActOpc1.UseVisualStyleBackColor = true;
            this.checkBoxActOpc1.CheckedChanged += new System.EventHandler(this.checkBoxActOpc1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxOpc2_2);
            this.groupBox2.Controls.Add(this.checkBoxOpc2_1);
            this.groupBox2.Location = new System.Drawing.Point(140, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(140, 78);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Visible";
            // 
            // checkBoxOpc2_2
            // 
            this.checkBoxOpc2_2.AutoSize = true;
            this.checkBoxOpc2_2.Checked = true;
            this.checkBoxOpc2_2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOpc2_2.Location = new System.Drawing.Point(6, 49);
            this.checkBoxOpc2_2.Name = "checkBoxOpc2_2";
            this.checkBoxOpc2_2.Size = new System.Drawing.Size(78, 17);
            this.checkBoxOpc2_2.TabIndex = 1;
            this.checkBoxOpc2_2.Text = "Opcion 2.2";
            this.checkBoxOpc2_2.UseVisualStyleBackColor = true;
            this.checkBoxOpc2_2.CheckedChanged += new System.EventHandler(this.checkBoxOpc2_2_CheckedChanged);
            // 
            // checkBoxOpc2_1
            // 
            this.checkBoxOpc2_1.AutoSize = true;
            this.checkBoxOpc2_1.Checked = true;
            this.checkBoxOpc2_1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOpc2_1.Location = new System.Drawing.Point(8, 26);
            this.checkBoxOpc2_1.Name = "checkBoxOpc2_1";
            this.checkBoxOpc2_1.Size = new System.Drawing.Size(78, 17);
            this.checkBoxOpc2_1.TabIndex = 0;
            this.checkBoxOpc2_1.Text = "Opción 2.1";
            this.checkBoxOpc2_1.UseVisualStyleBackColor = true;
            this.checkBoxOpc2_1.CheckedChanged += new System.EventHandler(this.checkBoxOpc2_1_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxMarcar1_3);
            this.groupBox3.Controls.Add(this.checkBoxMarcar1_2);
            this.groupBox3.Controls.Add(this.checkBoxMarcar1_1);
            this.groupBox3.Location = new System.Drawing.Point(12, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(122, 102);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Marcado";
            // 
            // checkBoxMarcar1_3
            // 
            this.checkBoxMarcar1_3.AutoSize = true;
            this.checkBoxMarcar1_3.Location = new System.Drawing.Point(10, 66);
            this.checkBoxMarcar1_3.Name = "checkBoxMarcar1_3";
            this.checkBoxMarcar1_3.Size = new System.Drawing.Size(64, 17);
            this.checkBoxMarcar1_3.TabIndex = 2;
            this.checkBoxMarcar1_3.Text = "Opc 1.3";
            this.checkBoxMarcar1_3.UseVisualStyleBackColor = true;
            this.checkBoxMarcar1_3.CheckedChanged += new System.EventHandler(this.checkBoxMarcar1_3_CheckedChanged);
            // 
            // checkBoxMarcar1_2
            // 
            this.checkBoxMarcar1_2.AutoSize = true;
            this.checkBoxMarcar1_2.Location = new System.Drawing.Point(10, 43);
            this.checkBoxMarcar1_2.Name = "checkBoxMarcar1_2";
            this.checkBoxMarcar1_2.Size = new System.Drawing.Size(64, 17);
            this.checkBoxMarcar1_2.TabIndex = 1;
            this.checkBoxMarcar1_2.Text = "Opc 1.2";
            this.checkBoxMarcar1_2.UseVisualStyleBackColor = true;
            this.checkBoxMarcar1_2.CheckedChanged += new System.EventHandler(this.checkBoxMarcar1_2_CheckedChanged);
            // 
            // checkBoxMarcar1_1
            // 
            this.checkBoxMarcar1_1.AutoSize = true;
            this.checkBoxMarcar1_1.Location = new System.Drawing.Point(10, 20);
            this.checkBoxMarcar1_1.Name = "checkBoxMarcar1_1";
            this.checkBoxMarcar1_1.Size = new System.Drawing.Size(64, 17);
            this.checkBoxMarcar1_1.TabIndex = 0;
            this.checkBoxMarcar1_1.Text = "Opc 1.1";
            this.checkBoxMarcar1_1.UseVisualStyleBackColor = true;
            this.checkBoxMarcar1_1.CheckedChanged += new System.EventHandler(this.checkBoxMarcar1_1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.label1.ContextMenuStrip = this.contextMenuStrip1;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 41);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pulsa el botón derecho";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.label1, "Botón derecho muestra menú contextual asociado a control");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextual1ToolStripMenuItem,
            this.mensajeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(148, 48);
            // 
            // contextual1ToolStripMenuItem
            // 
            this.contextual1ToolStripMenuItem.Name = "contextual1ToolStripMenuItem";
            this.contextual1ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.contextual1ToolStripMenuItem.Text = "Contextual 1";
            // 
            // mensajeToolStripMenuItem
            // 
            this.mensajeToolStripMenuItem.Name = "mensajeToolStripMenuItem";
            this.mensajeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.mensajeToolStripMenuItem.Text = "Mensaje";
            this.mensajeToolStripMenuItem.Click += new System.EventHandler(this.mensajeToolStripMenuItem_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButtonMelocotones);
            this.groupBox4.Controls.Add(this.radioButtonNaranjas);
            this.groupBox4.Controls.Add(this.radioButtonPeras);
            this.groupBox4.Location = new System.Drawing.Point(146, 120);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(134, 102);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Frutas (Radio Buttons)";
            // 
            // radioButtonPeras
            // 
            this.radioButtonPeras.AutoSize = true;
            this.radioButtonPeras.Checked = true;
            this.radioButtonPeras.Location = new System.Drawing.Point(6, 29);
            this.radioButtonPeras.Name = "radioButtonPeras";
            this.radioButtonPeras.Size = new System.Drawing.Size(52, 17);
            this.radioButtonPeras.TabIndex = 0;
            this.radioButtonPeras.TabStop = true;
            this.radioButtonPeras.Text = "Peras";
            this.radioButtonPeras.UseVisualStyleBackColor = true;
            // 
            // radioButtonNaranjas
            // 
            this.radioButtonNaranjas.AutoSize = true;
            this.radioButtonNaranjas.Location = new System.Drawing.Point(6, 52);
            this.radioButtonNaranjas.Name = "radioButtonNaranjas";
            this.radioButtonNaranjas.Size = new System.Drawing.Size(67, 17);
            this.radioButtonNaranjas.TabIndex = 1;
            this.radioButtonNaranjas.Text = "Naranjas";
            this.radioButtonNaranjas.UseVisualStyleBackColor = true;
            // 
            // radioButtonMelocotones
            // 
            this.radioButtonMelocotones.AutoSize = true;
            this.radioButtonMelocotones.Location = new System.Drawing.Point(6, 75);
            this.radioButtonMelocotones.Name = "radioButtonMelocotones";
            this.radioButtonMelocotones.Size = new System.Drawing.Size(86, 17);
            this.radioButtonMelocotones.TabIndex = 2;
            this.radioButtonMelocotones.Text = "Melocotones";
            this.radioButtonMelocotones.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miOpc1;
        private System.Windows.Forms.ToolStripMenuItem miOpc1_1;
        private System.Windows.Forms.ToolStripMenuItem miOpc1_2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miOpc1_3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem miSalir;
        private System.Windows.Forms.ToolStripMenuItem miOpc2;
        private System.Windows.Forms.ToolStripMenuItem miOpc2_1;
        private System.Windows.Forms.ToolStripMenuItem miOpc2_1_1;
        private System.Windows.Forms.ToolStripMenuItem miOpc2_1_2;
        private System.Windows.Forms.ToolStripMenuItem miOpc2_2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxActOpc1;
        private System.Windows.Forms.CheckBox checkBoxActOpc2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxOpc2_2;
        private System.Windows.Forms.CheckBox checkBoxOpc2_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxMarcar1_1;
        private System.Windows.Forms.CheckBox checkBoxMarcar1_3;
        private System.Windows.Forms.CheckBox checkBoxMarcar1_2;
        private System.Windows.Forms.ToolStripMenuItem miOpc3;
        private System.Windows.Forms.ToolStripMenuItem miOpc3_1;
        private System.Windows.Forms.ToolStripMenuItem miOpc3_2;
        private System.Windows.Forms.ToolStripMenuItem miOpc3_3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem contextual1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mensajeToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButtonMelocotones;
        private System.Windows.Forms.RadioButton radioButtonNaranjas;
        private System.Windows.Forms.RadioButton radioButtonPeras;
    }
}

