﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void miSalir_Click(object sender, EventArgs e)
        {
            // Finalizamos la aplicación
            Application.Exit();
        }

        private void checkBoxActOpc1_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Activar opción 1 del menú?
            miOpc1.Enabled = checkBoxActOpc1.Checked;
        }

        private void checkBoxActOpc2_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Activar opción 2 del menú?
            miOpc2.Enabled = checkBoxActOpc2.Checked;
        }

        private void checkBoxOpc2_1_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Mostrar Opción 2.1?
            miOpc2_1.Visible = checkBoxOpc2_1.Checked;
        }

        private void checkBoxOpc2_2_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Mostrar Opción 2.2?
            miOpc2_2.Visible = checkBoxOpc2_2.Checked;

        }

        private void checkBoxMarcar1_1_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Marcarmos elemento del menú?
            miOpc1_1.Checked = checkBoxMarcar1_1.Checked;
        }

        private void checkBoxMarcar1_2_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Marcarmos elemento del menú?
            miOpc1_2.Checked = checkBoxMarcar1_2.Checked;
        }

        private void checkBoxMarcar1_3_CheckedChanged(object sender, EventArgs e)
        {
            // ¿Marcarmos elemento del menú?
            miOpc1_3.Checked = checkBoxMarcar1_3.Checked;
        }

        private void miOpc1_1_Click(object sender, EventArgs e)
        {
            // Cambiamos estado de marca de elemento del menú
            miOpc1_1.Checked = !miOpc1_1.Checked;
            checkBoxMarcar1_1.Checked = miOpc1_1.Checked;
        }

        private void miOpc1_2_Click(object sender, EventArgs e)
        {
            // Cambiamos estado de marca de elemento del menú
            miOpc1_2.Checked = !miOpc1_2.Checked;
            checkBoxMarcar1_2.Checked = miOpc1_2.Checked;
        }

        private void mensajeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Seleccinada opción de Menú Contextual");
        }

        private void miOpc3_Click(object sender, EventArgs e)
        {
            // Pulsación de la opción de menu "Frutas"
            // Utilizamos este evento para activar / desactivar 
            // las opciónes de menú que incluye
            miOpc3_1.Enabled = radioButtonPeras.Checked;
            miOpc3_2.Enabled = radioButtonNaranjas.Checked;
            miOpc3_3.Enabled = radioButtonMelocotones.Checked;
        }


    }
}