using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/*  Ejercicio 15 - Ejemplo de ventanas con MDI
 * 
 *  Pasos para crear MDI
 * 
 *  - Seleccionar formulario contenedor:
 *      IsMDIContainer = true
 * 
 *  - Crear ventanas Hijas
 *      Crear ventana
 *      Indicarle que su padre es la ventana contenedora (MDIParent)
 * 
 * 
 * */
namespace Ejemplo15
{
    public partial class FormMDIParent : Form
    {
        FormDatos frmDatos;

        public FormMDIParent()
        {
            InitializeComponent();
        }

        // Metodo que inserta datos en el formulario frmDatos
        public void Inserta(string nombre, string apellidos)
        {
            frmDatos.Inserta(nombre, apellidos);
        }

        private void FormMDIParent_Load(object sender, EventArgs e)
        {
            // Creamos el formulario de Datos al arrancar el formulario
            // MDI
            frmDatos = new FormDatos();

            // Le indicamos dentro de que ventana estar� contenida
            frmDatos.MdiParent = this;
            frmDatos.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Salir en el men�
            Application.Exit();
        }

        private void nuevaVentanaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Nueva ventana
            FormPersona frm = new FormPersona();
            frm.MdiParent = this;
            frm.Padre = this;
            frm.Show();
        }

        private void toolStripButtonNueva_Click(object sender, EventArgs e)
        {
            // Pulsaci�n de bot�n de barra de herramientas
            // Invoca el m�todo del men�
            nuevaVentanaToolStripMenuItem_Click(sender, e);
        }

        private void cascadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Cascada
            // LayoutMDI es un metodo heredado de Form
            LayoutMdi(MdiLayout.Cascade);
        }

        private void mosaicoVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Mosaico Vertical
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void mosaicoHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Mosaico horizontal
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void cerrarTodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Cerrar todo

            // Cerramos todas las ventanas menos la de datos
            // MdiChildren contiene la lista de ventanas hijas
            foreach (Form childForm in MdiChildren)
            {
                // No cerramos la ventana de datos
                if (childForm != frmDatos)
                {
                    childForm.Close();
                }
            }
        }

    }
}