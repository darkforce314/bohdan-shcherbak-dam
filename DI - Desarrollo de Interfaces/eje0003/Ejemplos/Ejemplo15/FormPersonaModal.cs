using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/*
 *  Formulario que se mostrar� como MODAL. 
 * 
 *  Un formulario se puede mostrar como MODAL o como NO MODAL, indistintamente de su dise�o.
 * 
 *  Un formulario MODAL no deja seguir iteractuando con el resto de la aplicaci�n hasta que no
 *  se cierra. Para salir de un formulario modal existir� un Boton de Aceptar y otro de Cancelar.
 *  Dependiendo del valor pulsado, ese sera el valor devuelto por el formulario.
 * 
 *  Para establecer los botones de aceptar y cancelar mirar las propiedades:
 *     FORMULARIO
 *      - AcceptButton
 *      - CancelButton
 * 
 *     BOTON
 *      - DialogResult
 * 
 * M�todo ShowDialog
 * Este m�todo se puede usar para mostrar un cuadro de di�logo modal en la aplicaci�n.
 * Cuando se llama a este m�todo, el c�digo que lo sigue no se ejecuta hasta que se ha 
 * cerrado el cuadro de di�logo. Al cuadro de di�logo se le puede asignar uno de los 
 * valores de la enumeraci�n DialogResult asign�ndolo a la propiedad DialogResult de un 
 * Button del formulario o estableciendo la propiedad DialogResult del formulario en c�digo. 
 * As�, este m�todo devuelve este valor. El valor devuelto se puede usar para determinar el 
 * modo de procesar las acciones que se produjeron en el cuadro de di�logo. 
 * Por ejemplo, si el cuadro de di�logo se cerr� y devolvi� el valor DialogResult.Cancel mediante 
 * este m�todo, se puede impedir la ejecuci�n del c�digo que sigue a la llamada a ShowDialog.
 *    
 * 
 **/
namespace Ejemplo15
{
    public partial class FormPersonaModal : Form
    {
        public FormPersonaModal()
        {
            InitializeComponent();
        }

        // Propiedad para acceder al nombre
        public string Nombre
        {
            get { return textBoxNombre.Text; }
        }
        // Propiedad para acceder a los apellidos
        public string Apellidos
        {
            get { return textBoxApellidos.Text; }
        }

    }
}