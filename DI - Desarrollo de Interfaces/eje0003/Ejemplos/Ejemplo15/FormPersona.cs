using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/*  Formulario que se mostrar� como NO modal y que permitir� insertar datos de personas
 * 
 *  Mirar propiedad del formulario TopMost que hace que el formulario siempre aparezca encima
 * 
 **/
namespace Ejemplo15
{
    public partial class FormPersona : Form
    {
        FormMDIParent padre;

        // Guarda el formulario que lo controla
        public FormMDIParent Padre
        {
            get { return padre; }
            set { padre = value; }
        }

        // Copia datos a los textBox
        //
        public void Copia(string nombre, string apellidos)
        {
            textBoxNombre.Text = nombre;
            textBoxApellidos.Text = apellidos;
        }

        public FormPersona()
        {
            InitializeComponent();
        }

        private void buttonCerrar_Click(object sender, EventArgs e)
        {
            // Bot�n Cerrar Formulario
            Close();
        }


        private void buttonInsertar_Click(object sender, EventArgs e)
        {
            //
            // Le comunicamos al formulario padre los datos que debe insertar
            //
            padre.Inserta(textBoxNombre.Text, textBoxApellidos.Text);
        }
    }
}