﻿namespace Ejemplo15
{
    partial class FormDatos
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonModalSinBorrar = new System.Windows.Forms.Button();
            this.buttonInsertaModal = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonModalSinBorrar);
            this.panel1.Controls.Add(this.buttonInsertaModal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 33);
            this.panel1.TabIndex = 0;
            // 
            // buttonModalSinBorrar
            // 
            this.buttonModalSinBorrar.Location = new System.Drawing.Point(131, 3);
            this.buttonModalSinBorrar.Name = "buttonModalSinBorrar";
            this.buttonModalSinBorrar.Size = new System.Drawing.Size(131, 23);
            this.buttonModalSinBorrar.TabIndex = 2;
            this.buttonModalSinBorrar.Text = "Ins. Modal sin borrar";
            this.buttonModalSinBorrar.UseVisualStyleBackColor = true;
            this.buttonModalSinBorrar.Click += new System.EventHandler(this.buttonModalSinBorrar_Click);
            // 
            // buttonInsertaModal
            // 
            this.buttonInsertaModal.Location = new System.Drawing.Point(12, 3);
            this.buttonInsertaModal.Name = "buttonInsertaModal";
            this.buttonInsertaModal.Size = new System.Drawing.Size(113, 23);
            this.buttonInsertaModal.TabIndex = 1;
            this.buttonInsertaModal.Text = "Insertar Modal";
            this.buttonInsertaModal.UseVisualStyleBackColor = true;
            this.buttonInsertaModal.Click += new System.EventHandler(this.buttonInsertaModal_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nombre,
            this.Apellidos});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 33);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(549, 183);
            this.dataGridView.TabIndex = 1;
            this.toolTip1.SetToolTip(this.dataGridView, "Doble clic copia datos a formulario");
            this.dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellDoubleClick);
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            // 
            // Apellidos
            // 
            this.Apellidos.HeaderText = "Apellidos";
            this.Apellidos.Name = "Apellidos";
            this.Apellidos.ReadOnly = true;
            // 
            // FormDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 216);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.panel1);
            this.Name = "FormDatos";
            this.Text = "Datos";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellidos;
        private System.Windows.Forms.Button buttonInsertaModal;
        private System.Windows.Forms.Button buttonModalSinBorrar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

