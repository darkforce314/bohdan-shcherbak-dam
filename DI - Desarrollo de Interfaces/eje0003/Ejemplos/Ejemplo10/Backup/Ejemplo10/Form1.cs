﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/*
 * Ejemplo 10
 * 
 * Utilización de ListBox y ComboBox
 * 
 * 
 * LISTBOX
 *   - Items
 *   - SelectedIndex
 *   - SelectionMode
 *   - Sorted
 *  
 * COMBOBOX
 *   - DropDownStyle
 *   - Text
 * 
 * TOOLTIP
 *   - Como mostrar ayuda en los controles
 *   - Despues de introducir este control, ver nueva propiedad ToolTip de los controles.
 */
namespace Ejemplo10
{
    public partial class Form1 : Form
    {
        int n=0; // Contador utilizado para insertar elementos en el edit

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonAnyadir_Click(object sender, EventArgs e)
        {
            // Copiamos el texto a la listbox y los combobox
            //
            // Tanto ListBox como ComboBox tienen la propiedad Items, que es una lista de cadenas
            // que contiene los elementos que se deben mostrar en la lista
            //
            // El método Add añade un elemento a lista
            string texto = textBoxValor.Text;
            listBox.Items.Add(texto);
            comboBoxCopiar.Items.Add(texto);
            comboBoxSeleccionar.Items.Add(texto);

            //
            // Añadimos número al texto, quitamos el anterior si lo tiene
            //
            int pos = texto.IndexOf("-");
            // Si pos=-1 entonces no existe guion
            if (pos >= 0)
            {
                // truncamos la cadena en el guion
                texto = texto.Substring(0, pos);
            }
            texto = String.Format("{0}-{1}", texto,++n);

            textBoxValor.Text = texto;
        }

        private void checkBoxMultiple_CheckedChanged(object sender, EventArgs e)
        {
            // Fijamos selección múltiple de la listbox
            if (checkBoxMultiple.Checked)
            {
                // Seleccionado múltiple
                listBox.SelectionMode = SelectionMode.MultiExtended;
            }
            else
            {
                // Solamente se puede seleccionar un elemento
                listBox.SelectionMode = SelectionMode.One;
            }
            
        }

        private void checkBoxOrdenada_CheckedChanged(object sender, EventArgs e)
        {
            // Lista ordenada, ordenamos el contenido de la lista en función del valor del checkbox
            listBox.Sorted = checkBoxOrdenada.Checked;
            comboBoxSeleccionar.Sorted = checkBoxOrdenada.Checked;
        }

        private void buttonDer_Click(object sender, EventArgs e)
        {
            // Copiamos elementos a la otra lista
            if (listBox.SelectionMode == SelectionMode.MultiExtended)
            {
                // Sel contiene la lista con los indices de los elementos seleccionados
                ListBox.SelectedIndexCollection sel = listBox.SelectedIndices;

                // Seleccion múltiple, recorremos la lista buscando elementos seleccionados
                // y los copiamos en la otra lista
                for (int i = 0; i < sel.Count; i++)
                {
                    // Recorremos la lista copiando los elementos seleccionados
                    listBoxDest.Items.Add(listBox.Items[sel[i]]);
                }

            }
            else
            {
                // Selección simple
                int sel = listBox.SelectedIndex;
                listBoxDest.Items.Add(listBox.Items[sel]);
            }
        }

        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            // Borramos los elementos seleccionados
            if (listBox.SelectionMode == SelectionMode.MultiExtended)
            {
                // Sel contiene la lista con los indices de los elementos seleccionados
                ListBox.SelectedIndexCollection sel = listBox.SelectedIndices;

                // Borramos los elementos seleccionados
                for (int i = sel.Count - 1; i >= 0; i--)
                {
                    // Recorremos la lista y borramos los elementos seleccionados
                    // Recorremos la lista del final al principio, en caso contrario el indice de los
                    // elementos cambiaría cada vez que borrasemos un elemento
                    listBox.Items.RemoveAt(sel[i]);
                    // La lista del comboBox tiene lo mismo que la listBox
                    comboBoxSeleccionar.Items.RemoveAt(sel[i]);
                }
            }
            else
            {
                // Selección simple
                int sel = listBox.SelectedIndex;
                listBox.Items.RemoveAt(listBox.SelectedIndex);
                // La lista del comboBox tiene lo mismo que la listBox
                comboBoxSeleccionar.Items.RemoveAt(listBox.SelectedIndex);
            }
        }

        private void buttonCopiaValor_Click(object sender, EventArgs e)
        {
            // Usamos combo box como cuadro de texto
            textBoxValor.Text = comboBoxCopiar.Text;
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {   
            // Activamos desactivamos los botones de borrar o copiar
            if (listBoxDest.SelectionMode == SelectionMode.MultiExtended)
            {
                // Selección multiple
                buttonDer.Enabled = listBox.SelectedIndices.Count > 0;
            }
            else
            {
                // Selección simple
                buttonDer.Enabled = listBox.SelectedIndex != -1;
            }
            // Un botón igual al otro
            buttonBorrar.Enabled = buttonDer.Enabled;

        }

        private void buttonMover_Click(object sender, EventArgs e)
        {
            if ( listBoxDest.SelectedIndex == -1)
            {
                MessageBox.Show("Nada que mover");
                return;
            }
            else
            {
                // Movemos elemento seleccionado
                int sel = listBoxDest.SelectedIndex;
                // Copiamos
                listBox.Items.Add(listBoxDest.Items[sel]);
                comboBoxSeleccionar.Items.Add(listBoxDest.Items[sel]);
                // Borramos de la lista el elemento
                listBoxDest.Items.RemoveAt(sel);
            }
        }

        private void buttonSeleccionar_Click(object sender, EventArgs e)
        {
            // Seleecionamos el la lista el elemento indicado
            if (comboBoxSeleccionar.SelectedIndex != -1)
            {
                if (listBox.SelectionMode == SelectionMode.MultiExtended)
                {
                    // Seleccion múltiple
                    listBox.SelectedIndices.Add(comboBoxSeleccionar.SelectedIndex);
                }
                else
                {
                    // Selección simple
                    listBox.SelectedIndex = comboBoxSeleccionar.SelectedIndex;
                }
                // Miramos el estado de los botones
                listBox_SelectedIndexChanged(null, null);
            }
        }
    }
}