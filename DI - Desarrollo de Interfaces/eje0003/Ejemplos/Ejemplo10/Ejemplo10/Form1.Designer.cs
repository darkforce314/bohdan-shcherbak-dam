﻿namespace Ejemplo10
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonAnyadir = new System.Windows.Forms.Button();
            this.textBoxValor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox = new System.Windows.Forms.ListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.listBoxDest = new System.Windows.Forms.ListBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonMover = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.checkBoxOrdenada = new System.Windows.Forms.CheckBox();
            this.checkBoxMultiple = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonSeleccionar = new System.Windows.Forms.Button();
            this.comboBoxSeleccionar = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCopiaValor = new System.Windows.Forms.Button();
            this.comboBoxCopiar = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonBorrar = new System.Windows.Forms.Button();
            this.buttonDer = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.buttonAnyadir);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxValor);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.listBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(525, 330);
            this.splitContainer1.SplitterDistance = 203;
            this.splitContainer1.TabIndex = 0;
            // 
            // buttonAnyadir
            // 
            this.buttonAnyadir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnyadir.Location = new System.Drawing.Point(132, 288);
            this.buttonAnyadir.Name = "buttonAnyadir";
            this.buttonAnyadir.Size = new System.Drawing.Size(28, 29);
            this.buttonAnyadir.TabIndex = 3;
            this.buttonAnyadir.Text = "+";
            this.buttonAnyadir.UseVisualStyleBackColor = true;
            this.buttonAnyadir.Click += new System.EventHandler(this.buttonAnyadir_Click);
            // 
            // textBoxValor
            // 
            this.textBoxValor.Location = new System.Drawing.Point(17, 294);
            this.textBoxValor.Name = "textBoxValor";
            this.textBoxValor.Size = new System.Drawing.Size(100, 20);
            this.textBoxValor.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Valor";
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(12, 12);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(178, 251);
            this.listBox.TabIndex = 0;
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.listBoxDest);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(144, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(174, 330);
            this.panel5.TabIndex = 6;
            // 
            // listBoxDest
            // 
            this.listBoxDest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxDest.FormattingEnabled = true;
            this.listBoxDest.Location = new System.Drawing.Point(0, 0);
            this.listBoxDest.Name = "listBoxDest";
            this.listBoxDest.Size = new System.Drawing.Size(170, 277);
            this.listBoxDest.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.buttonMover);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 286);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(170, 40);
            this.panel6.TabIndex = 0;
            // 
            // buttonMover
            // 
            this.buttonMover.Location = new System.Drawing.Point(48, 9);
            this.buttonMover.Name = "buttonMover";
            this.buttonMover.Size = new System.Drawing.Size(75, 23);
            this.buttonMover.TabIndex = 0;
            this.buttonMover.Text = "<-- Mover";
            this.buttonMover.UseVisualStyleBackColor = true;
            this.buttonMover.Click += new System.EventHandler(this.buttonMover_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.checkBoxOrdenada);
            this.panel4.Controls.Add(this.checkBoxMultiple);
            this.panel4.Location = new System.Drawing.Point(4, 262);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(136, 67);
            this.panel4.TabIndex = 5;
            // 
            // checkBoxOrdenada
            // 
            this.checkBoxOrdenada.AutoSize = true;
            this.checkBoxOrdenada.Location = new System.Drawing.Point(10, 42);
            this.checkBoxOrdenada.Name = "checkBoxOrdenada";
            this.checkBoxOrdenada.Size = new System.Drawing.Size(73, 17);
            this.checkBoxOrdenada.TabIndex = 1;
            this.checkBoxOrdenada.Text = "Ordenada";
            this.checkBoxOrdenada.UseVisualStyleBackColor = true;
            this.checkBoxOrdenada.CheckedChanged += new System.EventHandler(this.checkBoxOrdenada_CheckedChanged);
            // 
            // checkBoxMultiple
            // 
            this.checkBoxMultiple.AutoSize = true;
            this.checkBoxMultiple.Location = new System.Drawing.Point(11, 19);
            this.checkBoxMultiple.Name = "checkBoxMultiple";
            this.checkBoxMultiple.Size = new System.Drawing.Size(112, 17);
            this.checkBoxMultiple.TabIndex = 0;
            this.checkBoxMultiple.Text = "Selección Múltiple";
            this.checkBoxMultiple.UseVisualStyleBackColor = true;
            this.checkBoxMultiple.CheckedChanged += new System.EventHandler(this.checkBoxMultiple_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonSeleccionar);
            this.panel3.Controls.Add(this.comboBoxSeleccionar);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(3, 156);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(138, 107);
            this.panel3.TabIndex = 4;
            // 
            // buttonSeleccionar
            // 
            this.buttonSeleccionar.Location = new System.Drawing.Point(33, 63);
            this.buttonSeleccionar.Name = "buttonSeleccionar";
            this.buttonSeleccionar.Size = new System.Drawing.Size(75, 23);
            this.buttonSeleccionar.TabIndex = 2;
            this.buttonSeleccionar.Text = "Seleccionar";
            this.toolTip1.SetToolTip(this.buttonSeleccionar, "Selecciona en la lista el elemento seleccionado");
            this.buttonSeleccionar.UseVisualStyleBackColor = true;
            this.buttonSeleccionar.Click += new System.EventHandler(this.buttonSeleccionar_Click);
            // 
            // comboBoxSeleccionar
            // 
            this.comboBoxSeleccionar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeleccionar.FormattingEnabled = true;
            this.comboBoxSeleccionar.Location = new System.Drawing.Point(11, 28);
            this.comboBoxSeleccionar.Name = "comboBoxSeleccionar";
            this.comboBoxSeleccionar.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSeleccionar.TabIndex = 1;
            this.toolTip1.SetToolTip(this.comboBoxSeleccionar, "DropDownStyle = DropDownList (no se puede escribir)");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Seleccionar";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.buttonCopiaValor);
            this.panel2.Controls.Add(this.comboBoxCopiar);
            this.panel2.Location = new System.Drawing.Point(3, 86);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(138, 74);
            this.panel2.TabIndex = 3;
            // 
            // buttonCopiaValor
            // 
            this.buttonCopiaValor.Location = new System.Drawing.Point(16, 39);
            this.buttonCopiaValor.Name = "buttonCopiaValor";
            this.buttonCopiaValor.Size = new System.Drawing.Size(107, 23);
            this.buttonCopiaValor.TabIndex = 1;
            this.buttonCopiaValor.Text = "Copiar a valor";
            this.buttonCopiaValor.UseVisualStyleBackColor = true;
            this.buttonCopiaValor.Click += new System.EventHandler(this.buttonCopiaValor_Click);
            // 
            // comboBoxCopiar
            // 
            this.comboBoxCopiar.FormattingEnabled = true;
            this.comboBoxCopiar.Location = new System.Drawing.Point(9, 12);
            this.comboBoxCopiar.Name = "comboBoxCopiar";
            this.comboBoxCopiar.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCopiar.TabIndex = 0;
            this.toolTip1.SetToolTip(this.comboBoxCopiar, "DropDownStyle = DropDown (se puede escribir)");
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buttonBorrar);
            this.panel1.Controls.Add(this.buttonDer);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(138, 77);
            this.panel1.TabIndex = 2;
            // 
            // buttonBorrar
            // 
            this.buttonBorrar.Enabled = false;
            this.buttonBorrar.Location = new System.Drawing.Point(17, 41);
            this.buttonBorrar.Name = "buttonBorrar";
            this.buttonBorrar.Size = new System.Drawing.Size(105, 23);
            this.buttonBorrar.TabIndex = 1;
            this.buttonBorrar.Text = "Borrar";
            this.buttonBorrar.UseVisualStyleBackColor = true;
            this.buttonBorrar.Click += new System.EventHandler(this.buttonBorrar_Click);
            // 
            // buttonDer
            // 
            this.buttonDer.Enabled = false;
            this.buttonDer.Location = new System.Drawing.Point(17, 12);
            this.buttonDer.Name = "buttonDer";
            this.buttonDer.Size = new System.Drawing.Size(105, 23);
            this.buttonDer.TabIndex = 0;
            this.buttonDer.Text = "Copiar -->";
            this.buttonDer.UseVisualStyleBackColor = true;
            this.buttonDer.Click += new System.EventHandler(this.buttonDer_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 330);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button buttonAnyadir;
        private System.Windows.Forms.TextBox textBoxValor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Button buttonBorrar;
        private System.Windows.Forms.Button buttonDer;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCopiaValor;
        private System.Windows.Forms.ComboBox comboBoxCopiar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListBox listBoxDest;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonMover;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox checkBoxOrdenada;
        private System.Windows.Forms.CheckBox checkBoxMultiple;
        private System.Windows.Forms.Button buttonSeleccionar;
        private System.Windows.Forms.ComboBox comboBoxSeleccionar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

