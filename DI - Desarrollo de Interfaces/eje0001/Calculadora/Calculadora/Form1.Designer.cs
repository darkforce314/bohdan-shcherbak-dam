﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.texto = new System.Windows.Forms.TextBox();
            this.bpor = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.bpartido = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.bmas = new System.Windows.Forms.Button();
            this.bpunto = new System.Windows.Forms.Button();
            this.b0 = new System.Windows.Forms.Button();
            this.bigual = new System.Windows.Forms.Button();
            this.bmenos = new System.Windows.Forms.Button();
            this.bborraruno = new System.Windows.Forms.Button();
            this.bborrartodo = new System.Windows.Forms.Button();
            this.bsqrt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // texto
            // 
            this.texto.Location = new System.Drawing.Point(12, 12);
            this.texto.Name = "texto";
            this.texto.ReadOnly = true;
            this.texto.Size = new System.Drawing.Size(182, 20);
            this.texto.TabIndex = 0;
            // 
            // bpor
            // 
            this.bpor.Location = new System.Drawing.Point(153, 77);
            this.bpor.Name = "bpor";
            this.bpor.Size = new System.Drawing.Size(41, 33);
            this.bpor.TabIndex = 3;
            this.bpor.Text = "*";
            this.bpor.UseVisualStyleBackColor = true;
            this.bpor.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b1
            // 
            this.b1.Location = new System.Drawing.Point(12, 77);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(41, 33);
            this.b1.TabIndex = 4;
            this.b1.Text = "1";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b2
            // 
            this.b2.Location = new System.Drawing.Point(59, 77);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(41, 33);
            this.b2.TabIndex = 5;
            this.b2.Text = "2";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b3
            // 
            this.b3.Location = new System.Drawing.Point(106, 77);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(41, 33);
            this.b3.TabIndex = 6;
            this.b3.Text = "3";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b4
            // 
            this.b4.Location = new System.Drawing.Point(12, 116);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(41, 33);
            this.b4.TabIndex = 7;
            this.b4.Text = "4";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b5
            // 
            this.b5.Location = new System.Drawing.Point(59, 116);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(41, 33);
            this.b5.TabIndex = 8;
            this.b5.Text = "5";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b6
            // 
            this.b6.Location = new System.Drawing.Point(106, 116);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(41, 33);
            this.b6.TabIndex = 9;
            this.b6.Text = "6";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bpartido
            // 
            this.bpartido.Location = new System.Drawing.Point(153, 116);
            this.bpartido.Name = "bpartido";
            this.bpartido.Size = new System.Drawing.Size(41, 33);
            this.bpartido.TabIndex = 10;
            this.bpartido.Text = "/";
            this.bpartido.UseVisualStyleBackColor = true;
            this.bpartido.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b7
            // 
            this.b7.Location = new System.Drawing.Point(12, 155);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(41, 33);
            this.b7.TabIndex = 11;
            this.b7.Text = "7";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b8
            // 
            this.b8.Location = new System.Drawing.Point(59, 155);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(41, 33);
            this.b8.TabIndex = 12;
            this.b8.Text = "8";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b9
            // 
            this.b9.Location = new System.Drawing.Point(106, 155);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(41, 33);
            this.b9.TabIndex = 13;
            this.b9.Text = "9";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bmas
            // 
            this.bmas.Location = new System.Drawing.Point(153, 155);
            this.bmas.Name = "bmas";
            this.bmas.Size = new System.Drawing.Size(41, 33);
            this.bmas.TabIndex = 14;
            this.bmas.Text = "+";
            this.bmas.UseVisualStyleBackColor = true;
            this.bmas.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bpunto
            // 
            this.bpunto.Location = new System.Drawing.Point(12, 194);
            this.bpunto.Name = "bpunto";
            this.bpunto.Size = new System.Drawing.Size(41, 33);
            this.bpunto.TabIndex = 15;
            this.bpunto.Text = ".";
            this.bpunto.UseVisualStyleBackColor = true;
            this.bpunto.Click += new System.EventHandler(this.obtener_dato);
            // 
            // b0
            // 
            this.b0.Location = new System.Drawing.Point(59, 194);
            this.b0.Name = "b0";
            this.b0.Size = new System.Drawing.Size(41, 33);
            this.b0.TabIndex = 16;
            this.b0.Text = "0";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bigual
            // 
            this.bigual.Location = new System.Drawing.Point(106, 194);
            this.bigual.Name = "bigual";
            this.bigual.Size = new System.Drawing.Size(41, 33);
            this.bigual.TabIndex = 17;
            this.bigual.Text = "=";
            this.bigual.UseVisualStyleBackColor = true;
            this.bigual.Click += new System.EventHandler(this.evaluar);
            // 
            // bmenos
            // 
            this.bmenos.Location = new System.Drawing.Point(153, 194);
            this.bmenos.Name = "bmenos";
            this.bmenos.Size = new System.Drawing.Size(41, 33);
            this.bmenos.TabIndex = 18;
            this.bmenos.Text = "-";
            this.bmenos.UseVisualStyleBackColor = true;
            this.bmenos.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bborraruno
            // 
            this.bborraruno.Location = new System.Drawing.Point(12, 38);
            this.bborraruno.Name = "bborraruno";
            this.bborraruno.Size = new System.Drawing.Size(64, 33);
            this.bborraruno.TabIndex = 19;
            this.bborraruno.Text = "Delete";
            this.bborraruno.UseVisualStyleBackColor = true;
            this.bborraruno.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bborrartodo
            // 
            this.bborrartodo.Location = new System.Drawing.Point(82, 38);
            this.bborrartodo.Name = "bborrartodo";
            this.bborrartodo.Size = new System.Drawing.Size(65, 33);
            this.bborrartodo.TabIndex = 21;
            this.bborrartodo.Text = "Clear";
            this.bborrartodo.UseVisualStyleBackColor = true;
            this.bborrartodo.Click += new System.EventHandler(this.obtener_dato);
            // 
            // bsqrt
            // 
            this.bsqrt.Location = new System.Drawing.Point(153, 38);
            this.bsqrt.Name = "bsqrt";
            this.bsqrt.Size = new System.Drawing.Size(41, 33);
            this.bsqrt.TabIndex = 22;
            this.bsqrt.Text = "√x";
            this.bsqrt.UseVisualStyleBackColor = true;
            this.bsqrt.Click += new System.EventHandler(this.raiz);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 233);
            this.Controls.Add(this.bsqrt);
            this.Controls.Add(this.bborrartodo);
            this.Controls.Add(this.bborraruno);
            this.Controls.Add(this.bmenos);
            this.Controls.Add(this.bigual);
            this.Controls.Add(this.b0);
            this.Controls.Add(this.bpunto);
            this.Controls.Add(this.bmas);
            this.Controls.Add(this.b9);
            this.Controls.Add(this.b8);
            this.Controls.Add(this.b7);
            this.Controls.Add(this.bpartido);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.bpor);
            this.Controls.Add(this.texto);
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox texto;
        private System.Windows.Forms.Button bpor;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button bpartido;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button bmas;
        private System.Windows.Forms.Button bpunto;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.Button bigual;
        private System.Windows.Forms.Button bmenos;
        private System.Windows.Forms.Button bborraruno;
        private System.Windows.Forms.Button bborrartodo;
        private System.Windows.Forms.Button bsqrt;
    }
}

