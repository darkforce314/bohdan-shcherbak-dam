﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        String expression="";
        String lastBtn = "";
        String evaluated="";
        Boolean point = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void obtener_dato(object sender, EventArgs e)
        {
            Button button = sender as Button;
            String aux = button.Text;
            switch (aux)
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    if (evaluated == "=")
                    {
                        expression = aux;
                        lastBtn = aux;
                    }
                    else if (evaluated == "")
                    {
                        expression = expression + aux;
                        lastBtn = aux;
                    }
                    evaluated = "";
                    texto.Text = expression;
                    break;
                case ".": if (lastBtn != "." && lastBtn != "*" && lastBtn != "/" && lastBtn != "+" && lastBtn != "-" && lastBtn != "" && point == false)
                    {
                        expression = expression + aux;
                        lastBtn = aux;
                        evaluated = "";
                        point = true;
                    }
                    texto.Text = expression;
                    break;
                case "*": case "/": case "+": case "-":
                    if (lastBtn != "." && lastBtn != "*" && lastBtn != "/" && lastBtn != "+" && lastBtn != "-" && lastBtn != "")
                    {
                        expression = expression + aux;
                        lastBtn = aux;
                        evaluated = "";
                        point = false;
                    }
                    texto.Text = expression;
                    break;
                case "C":
                    if (expression.Length > 1)
                    {
                        expression = expression.Remove(expression.Length - 1);
                        String aux1 = expression.Substring(expression.Length -1, 1);
                        if (aux1 != "." && aux1 != "*" && aux1 != "/" && aux1 != "+" && aux1 != "-" && aux1 != "")
                        {
                            lastBtn = aux1;
                        }
                        else
                        {
                            expression = expression.Remove(expression.Length - 1);
                            lastBtn = expression.Substring(expression.Length - 1, 1); ;
                        }

                    }
                    else if (expression.Length==1)
                    {
                        expression = "";
                        lastBtn = "";
                    }
                    evaluated = "";
                    texto.Text = expression;
                    break;
                case "CE":
                    point = false;
                    expression = "";
                    lastBtn = "";
                    evaluated = "";
                    texto.Text = expression;
                    break;
            }
        }

        public static double Evaluate(string expression)
        {
            DataTable table = new DataTable();
            table.Columns.Add("expression", typeof(string), expression);
            DataRow row = table.NewRow();
            table.Rows.Add(row);
            return double.Parse((string)row["expression"]);
        }

        private void evaluar(object sender, EventArgs e)
        {
            if (lastBtn != "." && lastBtn != "*" && lastBtn != "/" && lastBtn != "+" && lastBtn != "-" && lastBtn != "")
            {
                String salidaAux = (Evaluate(expression)).ToString();
                texto.Text = salidaAux.Replace(',', '.');
                evaluated = "=";
            }

        }

        private void raiz(object sender, EventArgs e)
        {
            if (lastBtn != "." && lastBtn != "*" && lastBtn != "/" && lastBtn != "+" && lastBtn != "-" && lastBtn != "")
            {
                double totalAux = (Evaluate(expression.Replace(',', '.')));
                expression = Math.Round(Math.Sqrt(totalAux), 2).ToString();
                expression = expression.Replace(',', '.');
                texto.Text = expression;
                evaluated = "=";
            }

        }
    }
}
