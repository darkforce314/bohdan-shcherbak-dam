4)

SQL>

CREATE OR REPLACE FUNCTION producto_ventas(
cProd IN NUMBER)
RETURN REAL
AS
cantidad NUMBER(3);
BEGIN
	SELECT SUM(UNIDADES) INTO cantidad FROM VENTAS
	WHERE COD_PRODUCTO = cProd;

	IF cantidad = 0 THEN
		RETURN 0;
	ELSE
		RETURN cantidad;
	END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN RETURN -1;
END;

OUTPUT>

Funci�n creada.

***************************************************************************
5)

SQL>

CREATE OR REPLACE PROCEDURE producto_ventas_total(
cProd IN NUMBER,
ventas OUT NUMBER
)
AS
BEGIN
	SELECT SUM(UNIDADES) INTO ventas FROM VENTAS
	WHERE COD_PRODUCTO = cProd;
EXCEPTION
  WHEN NO_DATA_FOUND THEN ventas := -1;
END;

OUTPUT>

Procedimiento creado.

***************************************************************************
6)

SQL>

CREATE OR REPLACE PROCEDURE userinfoVentas(
cliente VARCHAR2,
producto NUMBER
)
AS
name CLIENTES.NOMBRE%TYPE;
descr PRODUCTOS.DESCRIPCION%TYPE; 
units VENTAS.UNIDADES%TYPE;
dateTime VENTAS.FECHA%TYPE;
BEGIN
	SELECT NOMBRE, DESCRIPCION, UNIDADES, FECHA 
	INTO name, descr, units, dateTime 
	FROM CLIENTES, VENTAS, PRODUCTOS
	WHERE CLIENTES.NIF = VENTAS.NIF AND
	VENTAS.COD_PRODUCTO = PRODUCTOS.COD_PRODUCTO AND
	CLIENTES.NIF = cliente AND
	VENTAS.COD_PRODUCTO = producto;

	DBMS_OUTPUT.PUT_LINE('nombre > '||name);
	DBMS_OUTPUT.PUT_LINE('desc > '||descr);
	DBMS_OUTPUT.PUT_LINE('unidades > '||units);
	DBMS_OUTPUT.PUT_LINE('fecha > '||dateTime);

	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	RAISE_APPLICATION_ERROR(-20000, 'Error! No se han encontrado datos.');
END;

OUTPUT>

Procedimiento creado.

***************************************************************************
7)

SQL>

CREATE OR REPLACE FUNCTION ventasCount(
cliente VARCHAR2
)
RETURN REAL
AS
cantidad NUMBER;
BEGIN
	SELECT COUNT(*) 
	INTO cantidad 
	FROM VENTAS
	WHERE NIF = cliente;
	RETURN cantidad;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
	RETURN -1;
END;

OUTPUT>

Funci�n creada.

***************************************************************************
8)

SQL>

CREATE OR REPLACE FUNCTION ventasNombre(
nombre VARCHAR2
)
RETURN REAL
AS
nif CLIENTES.NIF%TYPE;
BEGIN
	SELECT NIF INTO COD_CLI FROM CLIENTES
	WHERE NOMBRE LIKE '%nombre%';

  EXECUTE ventasCount(nif);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
	RETURN -1;
END;

OUTPUT>

Funci�n creada.

***************************************************************************
9)

SQL>

CREATE OR REPLACE FUNCTION priceDiff
RETURN REAL
AS
  greatest PRODUCTOS.PRECIO_UNI%TYPE;
  lowest PRODUCTOS.PRECIO_UNI%TYPE;
BEGIN
	SELECT MAX(PRECIO_UNI), MIN(PRECIO_UNI) INTO greatest, lowest FROM PRODUCTOS;
  RETURN greatest - lowest;
END;

OUTPUT>

Funci�n creada.

***************************************************************************