1) Actividad Propuesta 1

SQL>

DECLARE
  Num1 NUMBER(8,2) := 0;
  Num2 NUMBER(8,2) NOT NULL := 0;
  Num3 NUMBER(8,2) NOT NULL := 0;
  Cantidad INTEGER(3);
  Precio NUMBER(6);
  Descuento NUMBER(6);
  Num4 Num1%TYPE;
  Dto CONSTANT INTEGER := 40;
  BEGIN
    DBMS_OUTPUT.PUT_LINE(Num4);
  END;


OUTPUT>

Linea 2 => se debe de cerrar la declaraci�n con ";"
Linea 4 => si una variable es "NOT NULL" se le debe de asignar un valor.
Linea 6 => declaraci�nes de las variables no deben anidarse.
Linea 7 => se debe de utilizar %TYPE en vez de %ROWTYPE porque queremos declarar una variable del mismo tipo que Num1
Linea 8 => una constante debe ser inicializada.

****************************************************************************************

2) Caso practico 1

SQL>

DECLARE 
  v_empleado_no NUMBER(4,0);
  v_c_empleados NUMBER(2);
  v_aumento NUMBER(7) DEFAULT 0;
  v_oficio VARCHAR2(10);
BEGIN
  v_empleado_no := &vt_empno;
  
  SELECT oficio INTO v_oficio FROM emple
  WHERE emp_no = v_empleado_no;
  
  IF v_oficio = 'PRESIDENTE' THEN
    v_aumento := 30;
  END IF;
  
  SELECT COUNT(*) INTO v_c_empleados FROM emple
  WHERE dir = v_empleado_no;
  
  IF v_c_empleados = 0 THEN
    v_aumento := v_aumento + 50;
  ELSIF v_c_empleados = 1 THEN
    v_aumento := v_aumento + 50;
  ELSIF v_c_empleados = 2 THEN
    v_aumento := v_aumento + 100;
  ELSE
    v_aumento := v_aumento + 110;
  END IF;
  
  UPDATE emple SET salario = salario + v_aumento WHERE emp_no = v_empleado_no;
  DBMS_OUTPUT.PUT_LINE(v_aumento);
END;

OUTPUT>

Introduzca un valor para vt_empno: antiguo   7:   v_empleado_no := &vt_empno;
nuevo   7:   v_empleado_no := 7839;
140

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

3) Caso pr�ctico 2

SQL>

DECLARE
  v_apellidos VARCHAR2(25);
  v_1apel VARCHAR(25);
  v_caracter CHAR;
  v_posicion INTEGER := 1;
BEGIN
  v_apellidos := '&vs_apellidos';
  
  v_caracter := SUBSTR(v_apellidos,v_posicion,1);
  WHILE v_caracter BETWEEN 'A' AND 'Z' LOOP
    v_1apel := v_1apel || v_caracter;
    v_posicion := v_posicion + 1;
    v_caracter := SUBSTR(v_apellidos,v_posicion,1);
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('1er Apellido: ' || v_1apel || '*');
END;

OUTPUT>

Introduzca un valor para vs_apellidos: antiguo   7:   v_apellidos := '&vs_apellidos';
nuevo   7:   v_apellidos := 'GIL MORENO';
1er Apellido: GIL*

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

4) Caso practico 3

SQL>

DECLARE
  r_cadena VARCHAR2(10);
BEGIN
  FOR i IN REVERSE 1..LENGTH('HOLA') LOOP
    r_cadena := r_cadena || SUBSTR('HOLA',i,1);
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(r_cadena);
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

DECLARE
  r_cadena VARCHAR2(10);
  i BINARY_INTEGER;
BEGIN
  i := LENGTH('HOLA');
  WHILE i >= 1 LOOP
    r_cadena := r_cadena || SUBSTR('HOLA',i,1);
    i := i - 1;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(r_cadena);
END;

OUTPUT>

ALOH

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

5) Actividad Propuesta 2

SQL>

DECLARE
  r_cadena VARCHAR2(10);
  i BINARY_INTEGER;
BEGIN
  i := LENGTH('HOLA');
  LOOP
    r_cadena := r_cadena || SUBSTR('HOLA',i,1);
    i := i - 1;
  EXIT WHEN i = 0;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(r_cadena);
END;

OUTPUT>

ALOH

Procedimiento PL/SQL terminado correctamente.


****************************************************************************************

6) Caso practico 4

SQL>

CREATE OR REPLACE PROCEDURE cambiar_oficio (
  num_empleado NUMBER,
  nuevo_oficio VARCHAR2)
AS
  v_anterior_oficio emple.oficio%TYPE;
BEGIN
  SELECT oficio INTO v_anterior_oficio FROM emple
  WHERE emp_no = num_empleado;
  
  UPDATE emple SET oficio = nuevo_oficio
  WHERE emp_no = num_empleado;
  DBMS_OUTPUT.PUT_LINE(num_empleado || '*Oficio ANterior: ' || v_anterior_oficio ||
  '*Oficio Nuevo: ' || nuevo_oficio);
END cambiar_oficio;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE cambiar_oficio(7902,'DIRECTOR');

OUTPUT>

7902*Oficio ANterior: ANALISTA*Oficio Nuevo: DIRECTOR

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

7) Actividad Propuesta 3

SQL>

CREATE OR REPLACE PROCEDURE cambiar_dept (
  num_empleado NUMBER,
  nuevo_dept NUMBER)
AS
  v_anterior_dept emple.dept_no%TYPE;
BEGIN
  SELECT dept_no INTO v_anterior_dept FROM emple
  WHERE emp_no = num_empleado;
  
  UPDATE emple SET dept_no = nuevo_dept
  WHERE emp_no = num_empleado;
  DBMS_OUTPUT.PUT_LINE(num_empleado || '*Departamento Anterior: ' || v_anterior_dept ||
  '*'*Departamento Nuevo: ' || nuevo_dept);
END cambiar_dept;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE cambiar_dept(7902,99);

OUTPUT>

7934*Departamento Anterior: 10*Departamento Nuevo: 99

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

8) Actividad Propuesta 4

Se marcar�n las llamadas adecuadas con "*":

crear_depart;
crear_depart(50); *
crear_depart('COMPRAS');
crear_depart(50,'COMPRAS); *
crear_depart('COMPRAS',50);
crear_depart('COMPRAS','VALENCIA');
crear_depart(50,'COMPRAS','VALENCIA'); *
crear_depart('COMPRAS',50,'VALENCIA');
crear_depart('VALENCIA','COMPRAS');
crear_depart('VALENCIA',50);

****************************************************************************************

9) Caso practico 5

SQL>

CREATE OR REPLACE PROCEDURE cambiar_divisas (
  cantidad_euros IN NUMBER,
  cambio_actual IN NUMBER,
  cantidad_comision IN OUT NUMBER,
  cantidad_divisas OUT NUMBER)
AS
  pct_comision CONSTANT NUMBER(3,2) := 0.2;
  minimo_comision CONSTANT NUMBER(6) DEFAULT 3;
BEGIN
  IF cantidad_comision IS NULL THEN
    cantidad_comision := GREATEST(cantidad_euros/100*pct_comision, minimo_comision);
  END IF;
  cantidad_divisas := (cantidad_euros - cantidad_comision) * cambio_actual;
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

CREATE OR REPLACE PROCEDURE mostrar_cambio_divisas(
  eur NUMBER,
  cambio NUMBER)
AS
  v_comision NUMBER(9);
  v_divisas NUMBER(9);
BEGIN
  Cambiar_divisas(eur,cambio,v_comision,v_divisas);
  DBMS_OUTPUT.PUT_LINE('Euros: '|| TO_CHAR(eur,'999,999,999.999'));
  DBMS_OUTPUT.PUT_LINE('Divisas por 1�: '|| TO_CHAR(cambio,'999,999,999.999'));
  DBMS_OUTPUT.PUT_LINE('Euros Comisi�n: '|| TO_CHAR(v_comision,'999,999,999.999'));
  DBMS_OUTPUT.PUT_LINE('Cantidad divisas: '|| TO_CHAR(v_divisas,'999,999,999.999'));
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE mostrar_cambio_divisas(2500, 1.220);

OUTPUT>

Euros:        		2,500.000
Divisas por 1�:         1.220
Euros Comisi�n:         5.000
Cantidad divisas:       3,044.000

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

10) Actividad complementaria 1

SQL>

CREATE OR REPLACE PROCEDURE sumar (
  num1 IN NUMBER,
  num2 IN NUMBER)
AS
BEGIN
  DBMS_OUTPUT.PUT_LINE(num1 + num2);
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE sumar(5,10);

OUTPUT>

15

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

11) Actividad complementaria 2

SQL>

CREATE OR REPLACE PROCEDURE reverseString(
  cadena IN VARCHAR2)
AS
  r_cadena VARCHAR2(45);
BEGIN
  FOR i IN REVERSE 1..LENGTH(cadena) LOOP
    r_cadena := r_cadena || SUBSTR(cadena,i,1);
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(r_cadena);
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE reverseString('ola k ase');

OUTPUT>

esa k alo

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

12) Actividad complementaria 3

SQL>

CREATE OR REPLACE PROCEDURE sumar (
  num1 IN NUMBER,
  num2 IN NUMBER,
  res OUT NUMBER)
AS
BEGIN
 res := num1 + num2;
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

CREATE OR REPLACE PROCEDURE reverseString(
  cadena IN VARCHAR2,
  res OUT VARCHAR2)
AS
BEGIN
  FOR i IN REVERSE 1..LENGTH(cadena) LOOP
    res := res || SUBSTR(cadena,i,1);
  END LOOP;
END;

****************************************************************************************

13) Actividad complementaria 4

SQL>

CREATE OR REPLACE PROCEDURE dateYear(
  actual IN DATE,
  resuelta OUT VARCHAR2)
AS
BEGIN
  resuelta := to_char(actual,'yyyy');
END;

****************************************************************************************

14) Actividad complementaria 5

SQL>

DECLARE
  res VARCHAR2(45);
BEGIN
  dateYear(sysdate,res);
  DBMS_OUTPUT.PUT_LINE(res);
END;

OUTPUT>

2014

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

15) Actividad complementaria 6

SQL>

CREATE OR REPLACE PROCEDURE yearsDiff(
  date1 IN DATE,
  date2 IN DATE,
  res OUT INT)
AS
BEGIN
  res := EXTRACT(YEAR FROM date1) - EXTRACT(YEAR FROM date2);
END;

****************************************************************************************

16) Actividad complementaria 7

SQL>

DECLARE
  date1 DATE;
  date2 DATE;
  res INT;
BEGIN
  date1 := TO_DATE('20140101','YYYYMMDD');
  date2 := TO_DATE('20110101','YYYYMMDD');
  yearsDiff(date1,date2,res);
  DBMS_OUTPUT.PUT_LINE(res);
END;

OUTPUT>

3

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

17) Actividad complementaria 8

SQL>

CREATE OR REPLACE PROCEDURE sumar5(
   num1 IN NUMBER,
   num2 IN NUMBER,
   num3 IN NUMBER DEFAULT 0,
   num4 IN NUMBER DEFAULT 0,
   num5 IN NUMBER DEFAULT 0)
AS
BEGIN
  DBMS_OUTPUT.PUT_LINE(num1+num2+num3+num4+num5);
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE sumar5(5,4,6,7,8);

OUTPUT>

30

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

18) Actividad complementaria 9

SQL>

CREATE OR REPLACE PROCEDURE onlyChars(
  inputString IN VARCHAR2,
  res OUT VARCHAR2)
AS
  i NUMBER := 1;
  currentChar CHAR;
BEGIN
  WHILE i <= LENGTH(inputString) LOOP
    currentChar := SUBSTR(inputString,i,1);
    IF (currentChar BETWEEN 'a' AND 'z') OR (currentChar BETWEEN 'A' AND 'Z') THEN
      res := res || currentChar;
    ELSE
      res := res || ' ';
    END IF;
    i := i+1;
  END LOOP;
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

DECLARE
  res VARCHAR2(45);
BEGIN
  onlyChars('b*u3eNas2*',res);
  DBMS_OUTPUT.PUT_LINE(res);
END;

OUTPUT>

b u eNas

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

19) Actividad complementaria 10

SQL>

CREATE OR REPLACE PROCEDURE deleteEmpl(
  numero_em NUMBER)
AS
BEGIN
  DELETE FROM emple WHERE emp_no = numero_em;
END; 

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE deleteEmpl(7934);

OUTPUT>

BORRADO EMPLEADO*7934*MU�OZ

Procedimiento PL/SQL terminado correctamente.

****************************************************************************************

20) Actividad complementaria 11

SQL>

CREATE OR REPLACE PROCEDURE changeDept(
  numero NUMBER,
  localidad VARCHAR2)
AS
BEGIN
  UPDATE dept SET loc = localidad WHERE deptno = numero;
END;

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

EXECUTE changeDept(20,'Sevilla');

*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

SELECT * FROM dept;

OUTPUT>

    DEPTNO DNAME          LOC
---------- -------------- -------------
        10 ACCOUNTING     NEW YORK
        20 RESEARCH       Sevilla
        30 SALES          CHICAGO
        40 OPERATIONS     BOSTON

****************************************************************************************

21) Actividad complementaria 12

SQL>

SELECT * FROM ALL_OBJECTS WHERE OWNER = 'SCOTT' AND OBJECT_TYPE IN ('FUNCTION','PROCEDURE');

OUTPUT>

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
SCOTT                          CAMBIAR_DEPT
                                    24936                PROCEDURE
06/10/14 06/10/14 2014-10-06:20:20:47 VALID   N N N

SCOTT                          CAMBIAR_DIVISAS
                                    24937                PROCEDURE
06/10/14 06/10/14 2014-10-06:20:35:11 VALID   N N N

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -

SCOTT                          CAMBIAR_OFICIO
                                    24935                PROCEDURE
06/10/14 06/10/14 2014-10-06:20:15:18 VALID   N N N

SCOTT                          CHANGEDEPT
                                    24946                PROCEDURE

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
07/10/14 07/10/14 2014-10-07:20:50:38 VALID   N N N

SCOTT                          CON_IVA
                                    24928                FUNCTION
30/09/14 30/09/14 2014-09-30:20:01:06 VALID   N N N

SCOTT                          DATEYEAR

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
                                    24941                PROCEDURE
07/10/14 07/10/14 2014-10-07:19:46:10 VALID   N N N

SCOTT                          DELETEEMPL
                                    24945                PROCEDURE
07/10/14 07/10/14 2014-10-07:20:44:11 VALID   N N N


OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
SCOTT                          MODIFICAR_PRECIO_PRODUCTO
                                    24925                PROCEDURE
30/09/14 30/09/14 2014-09-30:19:55:34 VALID   N N N

SCOTT                          MOSTRAR_CAMBIO_DIVISAS
                                    24938                PROCEDURE
06/10/14 06/10/14 2014-10-06:20:45:33 VALID   N N N

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -

SCOTT                          ONLYCHARS
                                    24944                PROCEDURE
07/10/14 07/10/14 2014-10-07:20:37:21 VALID   N N N

SCOTT                          REVERSESTRING
                                    24940                PROCEDURE

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
06/10/14 07/10/14 2014-10-07:19:38:22 VALID   N N N

SCOTT                          SUMAR
                                    24939                PROCEDURE
06/10/14 07/10/14 2014-10-07:19:37:03 VALID   N N N

SCOTT                          SUMAR5

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
                                    24943                PROCEDURE
07/10/14 07/10/14 2014-10-07:20:09:54 VALID   N N N

SCOTT                          VER_DEPART
                                    24917                PROCEDURE
29/09/14 30/09/14 2014-09-30:20:19:45 VALID   N N N


OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -
SCOTT                          VER_PRECIO
                                    24920                PROCEDURE
29/09/14 30/09/14 2014-09-30:19:38:18 VALID   N N N

SCOTT                          YEARSDIFF
                                    24942                PROCEDURE
07/10/14 07/10/14 2014-10-07:19:57:40 VALID   N N N

OWNER                          OBJECT_NAME
------------------------------ ------------------------------
SUBOBJECT_NAME                  OBJECT_ID DATA_OBJECT_ID OBJECT_TYPE
------------------------------ ---------- -------------- ------------------
CREATED  LAST_DDL TIMESTAMP           STATUS  T G S
-------- -------- ------------------- ------- - - -


16 filas seleccionadas.



****************************************************************************************
