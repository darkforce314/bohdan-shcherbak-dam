0) PL-SQL ECHO / sout / console.log

SQL>

 BEGIN 
   dbms_output.put_line('Hola mundo');
 END;

// Se debe activar output : "set serveroutput on;"

OUTPUT>

Hola mundo

Procedimiento PL/SQL terminado correctamente.

**********************************************************************

1) Definici�n de variables

SQL>

DECLARE nombre VARCHAR(15) := 'BOHDAN';
BEGIN
  dbms_output.put_line('HOLA ' || nombre);
END;

OUTPUT>

HOLA BOHDAN

Procedimiento PL/SQL terminado correctamente.

**********************************************************************

2) Variables de fecha y nombre

SQL>

DECLARE 
  nombre VARCHAR(15) := 'BOHDAN';
  fecha DATE DEFAULT sysdate;
BEGIN
  dbms_output.put_line('HOLA ' || nombre || ' Hoy es: ' || fecha);
END;

OUTPUT>

HOLA BOHDAN Hoy es: 29/09/14

Procedimiento PL/SQL terminado correctamente.

**********************************************************************
3) Devolver el dia actual

SQL>

DECLARE 
  nombre VARCHAR(15) := 'BOHDAN';
  fecha DATE DEFAULT sysdate;
BEGIN
  dbms_output.put_line('HOLA ' || nombre || ' Hoy es: ' || to_char(fecha,'day'));
END;

OUTPUT>

HOLA BOHDAN Hoy es: lunes

Procedimiento PL/SQL terminado correctamente.

**********************************************************************
4) Funci�n IF

SQL>

DECLARE 
  nombre VARCHAR(15) := 'BOHDAN';
  fecha DATE DEFAULT sysdate;
  actual VARCHAR(10) DEFAULT to_char(fecha,'day');
BEGIN
  IF actual LIKE 'lunes%' THEN 
    dbms_output.put_line('Buena semana camarada!');
  END IF;
END;

OUTPUT>

Buena semana camarada!

Procedimiento PL/SQL terminado correctamente.


**********************************************************************
5) IF / ELSIF / ELS

SQL>

DECLARE 
  nombre VARCHAR(15) := 'BOHDAN';
  fecha DATE DEFAULT sysdate;
  actual VARCHAR(10) DEFAULT to_char(fecha,'day');
BEGIN
  IF actual LIKE 'lunes%' THEN 
    dbms_output.put_line('Buena semana camarada!');
  ELSIF actual LIKE 'viernes%' THEN
    dbms_output.put_line('Buena fin de semana camarada!');
  ELSE    
    dbms_output.put_line('Ya queda poco para el fin de semana camarada!');
  END IF;
END;

OUTPUT>

Buena semana camarada!

Procedimiento PL/SQL terminado correctamente.

**********************************************************************
6) Bucle FOR

SQL>

DECLARE 
  numero INT DEFAULT 100;
BEGIN
  FOR i IN REVERSE 1..100 LOOP
    dbms_output.put_line('HOLA ' || numero);
    numero := numero-1;
  END LOOP;
END;

// Al escribir "REVERSE", el bucle empieza por el final

OUTPUT>

.
.
.
HOLA 9
HOLA 8
HOLA 7
HOLA 6
HOLA 5
HOLA 4
HOLA 3
HOLA 2
HOLA 1

Procedimiento PL/SQL terminado correctamente.

**********************************************************************
7) Bucle WHILE 

SQL>

DECLARE 
  numero INT DEFAULT 0;
BEGIN
  WHILE numero <= 100 LOOP
    dbms_output.put_line('Hola ' || numero);
    numero := numero+1;
  END LOOP;
END;

OUTPUT>

.
.
.
Hola 97
Hola 98
Hola 99
Hola 100

Procedimiento PL/SQL terminado correctamente.

**********************************************************************

8) Bucle WHILE s�lo pares 

SQL>

DECLARE 
  numero INT DEFAULT 0;
BEGIN
  WHILE numero <= 100 LOOP
    IF (MOD(numero, 2) = 0) THEN
      dbms_output.put_line('Hola ' || numero);
    END IF;
    numero := numero+1;
  END LOOP;
END;

OUTPUT>

.
.
.
Hola 94
Hola 96
Hola 98
Hola 100

Procedimiento PL/SQL terminado correctamente.


**********************************************************************