1) Ejemplo 1
SQL>

CREATE OR replace TRIGGER audit_subida_salario 
  AFTER UPDATE OF salario ON emple 
  FOR EACH ROW 
BEGIN 
    INSERT INTO auditaremple 
    VALUES ('Subida salarial al Empleado: ' ||:old.emp_no ); 
END; 

************************************************************

UPDATE emple SET salario=2000 WHERE emp_no=7566;

OUTPUT>

Subida salarial al Empleado: 7566

2) Ejemplo 2
SQL>

CREATE OR replace TRIGGER audit_borrado_emple 
  BEFORE DELETE ON emple 
  FOR EACH ROW 
BEGIN 
    INSERT INTO auditaremple 
    VALUES ('Empleado Borrado: '
                 ||:old.emp_no 
                 ||' Apellido: ' 
                 ||:old.apellido 
                 ||' Departamento: ' 
                 ||:old.dept_no); 
END; 

************************************************************

DELETE emple WHERE emp_no=7566;

OUTPUT>

Empleado Borrado: 7566 Apellido: FERNANDEZ Departamento: 20

3) Propuesto 1
SQL>

CREATE OR replace TRIGGER cambiocincoporciento 
  BEFORE UPDATE ON emple 

FOR EACH ROW 
BEGIN 
    IF ( ( :old.salario / 100 )* 5  < :new.salario ) THEN 
      INSERT INTO auditaremple 
      VALUES      (SYSDATE 
                   || ' Empleado: ' 
                   || :old.emp_no 
                   || ', Sueldo Antiguo: ' 
                   || :old.salario 
                   || ', Sueldo Nuevo: ' 
                   || :new.salario); 
    END IF; 
END; 

************************************************************

UPDATE emple SET salario = 500 WHERE emp_no=7566;

OUTPUT>

12/01/2015 Empleado: 7566, Sueldo Antiguo: 100, Sueldo Nuevo: 6

4) Complementario 1
SQl>
CREATE OR REPLACE TRIGGER comp1
AFTER INSERT OR DELETE ON EMPLE

FOR EACH ROW
BEGIN
	IF INSERTING THEN
		INSERT INTO auditaremple 
		VALUES (TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')) || ' Empleado: '|| :new.emp_no || ' Apellido: ' || :new.apellido || ' > INSERTADO');
	ELSIF DELETING THEN
		INSERT INTO auditaremple 
		VALUES (TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')) || ' Empleado: '|| :old.emp_no || ' Apellido: ' || :old.apellido || ' > BORRADO');
	END IF;
END;

************************************************************

DELETE * FROM EMPLE;

OUTPUT>

.
.
.
13/01/2015 11:32:11 Empleado: 7566 Apellido: FERNANDEZ > BORRADO
.
.
.

5) Complementario 2
SQl>

CREATE OR replace TRIGGER comp2 
  BEFORE UPDATE ON emple 

FOR EACH ROW 
DECLARE data VARCHAR2(200); 
BEGIN 
    data := TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')
              || ' Empleado ' 
              || :old.emp_no 
              || 'Apellido ' 
              || :old.apellido 
              || 'Modificacion: '; 

    IF :new.apellido != :old.apellido THEN 
      data := data 
                || ' Antiguo apellido ' 
                || :old.apellido 
                || ' Nuevo apellido: ' 
                || :new.apellido 
                || ' '; 
    END IF; 

    IF :new.oficio != :old.oficio THEN 
      data := data 
                || ' Antiguo oficio ' 
                || :old.oficio 
                || ' Nuevo oficio: ' 
                || :new.oficio 
                || ' '; 
    END IF; 

    IF :new.dir != :old.dir THEN 
      data := data 
                || ' Antiguo dir ' 
                || :old.dir 
                || ' Nuevo dir: ' 
                || :new.dir 
                || ' '; 
    END IF; 

    IF :new.fecha_alt != :old.fecha_alt THEN 
      data := data 
                || ' Antiguo fecha_alt ' 
                || :old.fecha_alt 
                || ' Nuevo fecha_alt: ' 
                || :new.fecha_alt 
                || ' '; 
    END IF; 

    IF :new.salario != :old.salario THEN 
      data := data 
                || ' Antiguo salario ' 
                || :old.salario 
                || ' Nuevo salario: ' 
                || :new.salario 
                || ' '; 
    END IF; 

    IF :new.comision != :old.comision THEN 
      data := data 
                || ' Antiguo comision ' 
                || :old.comision 
                || ' Nuevo comision: ' 
                || :new.comision 
                || ' '; 
    END IF; 

    IF :new.dept_no != :old.dept_no THEN 
      data := data 
                || ' Antiguo dept_no ' 
                || :old.dept_no 
                || ' Nuevo dept_no: ' 
                || :new.dept_no 
                || ' '; 
    END IF; 

    INSERT INTO auditaremple 
    VALUES      (data); 
END; 

OUTPUT>
