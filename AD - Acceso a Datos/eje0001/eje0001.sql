	INSTRUCCIONES:
	==============

-Salva este fichero con las iniciales de tu nombre y apellidos,
 en el directorio "C:\ALUMNOS\". La extension del fichero debe ser '.sql'.
	
-Pon tu nombre al ejercicio y lee atentamente todas las preguntas.

-Donde ponga "SQL>", copiarás las sentencias SQL que has utilizado.

-SÓLO donde ponga "RESULTADOS:" copiarás el resultado que SQL*Plus te devuelve.

-RECUERDA: guardar, cada cierto tiempo, el contenido de este fichero. 

************************************************************************

Nombre: Bohdan Shcherbak

************************************************************************
	Descripción de las tablas:
	==========================

HABILIDADES
-----------
# COD_HA	CHAR(5)		Código Habilidad
  DESC_HA	VARCHAR2(30)	Descripción de Habilidad (O) (U)

CENTROS
-------
# COD_CE	CHAR(4)		Código del Centro
* DIRECTOR_CE	NUMBER(6)	Director del Centro
  NOMB_CE	VARCHAR2(30)	Nombre del Centro (O)
  DIRECC_CE	VARCHAR2(50)	Dirección del Centro (O)
  POBLAC_CE	VARCHAR2(15)	Población del Centro (O)

DEPARTAMENTOS
-------------
# COD_DE	CHAR(5)		Código del Departamento
* DIRECTOR_DE	NUMBER(6)	Director del Departamento
* DEPTJEFE_DE	CHAR(5)		Departamento del que depende
* CENTRO_DE	CHAR(4)		Centro trabajo (O)
  NOMB_DE	VARCHAR2(40)	Nombre del Departamento (O)
  PRESUP_DE	NUMBER(11)	Presupuesto del Departamento (O)
  TIPODIR_DE	CHAR(1)		Tipo de Director del Departamento (O)

EMPLEADOS
---------
# COD_EM	NUMBER(6)	Código del Empleado
* DEPT_EM	CHAR(5)		Departamento del Empleado (O)
  EXTTEL_EM	CHAR(9)		Extensión telefónica
  FECINC_EM	DATE		Fecha de incorporación del Empleado (O)
  FECNAC_EM	DATE		Fecha de nacimiento del Empleado (O)
  DNI_EM	VARCHAR2(9)	DNI del Empleado (U)
  NOMB_EM	VARCHAR2(40)	Nombre del Empleado (O)
  NUMHIJ_EM	NUMBER(2)	Número de hijos del Empleado (O)
  SALARIO_EM	NUMBER(9)	Salario Anual del Empleado (O)

HIJOS
-----
#*PADRE_HI	NUMBER(6)	Código del Empleado
# NUMHIJ_HI	NUMBER(2)	Número del hijo del Empleado
  FECNAC_HI	DATE		Fecha de nacimiento del Hijo (O)
  NOMB_HI	VARCHAR2(40)	Nombre del Hijo (O)

HABIEMPL
-----------
#*CODHA_HE	CHAR(5)		Código de la Habilidad
#*CODEM_HE	NUMBER(6)	Código del Empleado


Nota: 
	# PRIMARY KEY
	* FOREIGN KEY
	(O) Obligatorio
	(U) Único

************************************************************************
1.- Crea las tablas anteriores indicando las claves primarias y las restricciones (O) y (U).

SQL>

CREATE TABLE HABILIDADES (
  COD_HA CHAR(5),
  DESC_HA VARCHAR(30) NOT NULL UNIQUE,
  PRIMARY KEY (COD_HA)
  );

CREATE TABLE CENTROS (
  COD_CE CHAR(5),
  DIRECTOR_CE NUMBER(6),
  NOMB_CE	VARCHAR(30) NOT NULL,
  DIRECC_CE	VARCHAR(50) NOT NULL,
  POBLAC_CE VARCHAR(15) NOT NULL,
  PRIMARY KEY (COD_CE),
  FOREIGN KEY (DIRECTOR_CE) REFERENCES EMPLEADOS (COD_EM)
  );

CREATE TABLE DEPARTAMENTOS (
  COD_DE CHAR(5),
  DIRECTOR_DE NUMBER(6),
  DEPTJEFE_DE CHAR(5),
  CENTRO_DE	CHAR(4) NOT NULL,
  NOMB_DE VARCHAR(40) NOT NULL,
  PRESUP_DE NUMBER(11) NOT NULL,
  TIPODIR_DE CHAR(1) NOT NULL,
  PRIMARY KEY (COD_DE),
  FOREIGN KEY (DIRECTOR_DE) REFERENCES EMPLEADOS (COD_EM),
  FOREIGN KEY (DEPJEFE_DE) REFERENCES DEPARTAMENTOS (COD_DE),
  FOREIGN KEY (CENTRO_DE) REFERENCES CENTROS (COD_CE)
  );

CREATE TABLE EMPLEADOS(
  COD_EM NUMBER(6),
  DEPT_EM CHAR(5) NOT NULL,
  EXTTEL_EM	CHAR(9),
  FECINC_EM DATE NOT NULL,
  FECNAC_EM	DATE NOT NULL,
  DNI_EM VARCHAR(9) UNIQUE,
  NOMB_EM VARCHAR(40) NOT NULL,
  NUMHIJ_EM	NUMBER(2) NOT NULL,
  SALARIO_EM NUMBER(9) NOT NULL,
  PRIMARY KEY (COD_EM),
  FOREIGN KEY (DEPT_EM) REFERENCES DEPARTAMENTOS (COD_DE)
  );

CREATE TABLE HIJOS(
  PADRE_HI	NUMBER(6),
  NUMHIJ_HI	NUMBER(2),
  FECNAC_HI	DATE NOT NULL,
  NOMB_HI VARCHAR(40) NOT NULL,
  PRIMARY KEY (PADRE_HI, NUMHIJ_HI),
  FOREIGN KEY (PADRE_HI) REFERENCES EMPLEADOS (COD_EM)
  );

CREATE TABLE HABIEMPL(
  CODHA_HE CHAR(5),
  CODEM_HE NUMBER(6),
  PRIMARY KEY (CODHA_HE, CODEM_HE),
  FOREIGN KEY (CODHA_HE) REFERENCES HABILIDADES (COD_HA),
  FOREIGN KEY (CODEM_HE) REFERENCES EMPLEADOS (COD_EM)
  );

************************************************************************
2.- Ejecuta las órdenes del fichero "Datos.txt" con el comando START.

SQL>

START c:/Datos.txt

************************************************************************
3.- Añade las restricciones de clave ajena a las tablas que las tengan.

SQL> 

 ALTER TABLE CENTROS
  ADD CONSTRAINT f_centros_1
  FOREIGN KEY (DIRECTOR_CE)
  REFERENCES EMPLEADOS (COD_EM);
  
 
 ALTER TABLE DEPARTAMENTOS
  ADD CONSTRAINT f_departamentos_1
  FOREIGN KEY (DIRECTOR_DE)
  REFERENCES EMPLEADOS (COD_EM);
 
 ALTER TABLE DEPARTAMENTOS
  ADD CONSTRAINT f_departamentos_2
  FOREIGN KEY (DEPJEFE_DE)
  REFERENCES DEPARTAMENTOS (COD_DE);
  
 ALTER TABLE DEPARTAMENTOS
  ADD CONSTRAINT f_departamentos_3
  FOREIGN KEY (CENTRO_DE)
  REFERENCES CENTROS (COD_CE);
  
  
 ALTER TABLE EMPLEADOS
  ADD CONSTRAINT f_empleados_1
  FOREIGN KEY (DEPT_EM)
  REFERENCES DEPARTAMENTOS (COD_DE);
    
    
 ALTER TABLE HIJOS
  ADD CONSTRAINT f_hijos_1
  FOREIGN KEY (PADRE_HI)
  REFERENCES EMPLEADOS (COD_EM);
    
    
 ALTER TABLE HABIEMPL
  ADD CONSTRAINT f_habiempl_1
  FOREIGN KEY (CODHA_HE)
  REFERENCES HABILIDADES (COD_HA);
      
 ALTER TABLE HABIEMPL
  ADD CONSTRAINT f_habiempl_2
  FOREIGN KEY (CODEM_HE)
  REFERENCES EMPLEADOS (COD_EM);

************************************************************************
4.- Añade las siguientes restricciones:
	- Todos los campos numéricos son positivos.
	- Todos los nombres y descripciones deben estar en mayúsculas.
	- Todos los empleados eran mayores de edad cuando se incorporaron.
	- El Tipo de Director es 'F' o 'P'.
 Si no pudieras añadir alguna de las restricciones anteriores modifica los valores de las columnas correspondientes para añadirlas.

SQL>

 ALTER TABLE HABILIDADES
  ADD CONSTRAINT f_centros_positivo
  CHECK (upper(DESC_HA) = DESC_HA);
  
 ALTER TABLE CENTROS
  ADD CONSTRAINT f_centros_positivo
  CHECK (DIRECTOR_CE>=0 
    and upper(NOMB_CE) = NOMB_CE 
    and upper(DIRECC_CE) = DIRECC_CE 
    and upper(POBLAC_CE) = POBLAC_CE);
        
 ALTER TABLE DEPARTAMENTOS
  ADD CONSTRAINT f_dep_positivo
  CHECK (DIRECTOR_DE>=0 
    and PRESUP_DE>=0 
    and upper(NOMB_DE) = NOMB_DE)
    and (TIPODIR_DE in ('F','P')); 
  
 ALTER TABLE EMPLEADOS
  ADD CONSTRAINT f_emp_positivo
  CHECK (COD_EM>=0 
    and NUMHIJ_EM>=0 
    and SALARIO_EM>=0 
    and upper(DNI_EM) = DNI_EM 
    and upper(NOMB_EM) = NOMB_EM 
    and MONTHS_BETWEEN(FECINC_EM,FECNAC_EM)/12 >18;
  
 ALTER TABLE HIJOS
  ADD CONSTRAINT f_hijos_positivo
  CHECK (PADRE_HI>=0 
    and NUMHIJ_HI>=0 
    and upper(NOMB_HI) = NOMB_HI);
    
 ALTER TABLE HABIEMPL
  ADD CONSTRAINT f_habiempl_codhab_positivo
  CHECK (CODHA_HE>=0);

************************************************************************
5.- Añade la columna NIVEL_HE NUMBER(2), a la tabla HABIEMPL con las siguientes restricciones:
	- Es obligatorio.
	- Por defecto vale 5.
	- Valores válidos entre 1 y 10.

SQL>

ALTER TABLE HABIEMPL
 ADD NIVEL_HE NUMBER(2) NOT NULL DEFAULT 5
 CONSTRAINT f_habiempl_new
 CHECK  (NIVEL_HE >= 1 and NIVEL_HE <= 10);

************************************************************************
6.- Listar el código y nombre de los empleados cuyo código sea distinto de 1, 4, 6, 8 ó 10.

SQL>

SELECT COD_EM, NOMB_EM FROM EMPLEADOS WHERE COD_EM NOT IN(1, 4, 6, 8, 10);

RESULTADO:

COD_EM NOMB_EM
--------------------------------------------------
         5 ALADA VERAZ, JUANA
         7 FORZADO LÓPEZ, JUAN
         9 MANDO CORREA, ROSA
         2 MANRIQUE BACTERIO, LUISA
         3 MONFORTE CID, ROLDÁN

************************************************************************
7.- Listar el nombre de los empleados que no tienen extensión telefónica.

SQL>

SELECT NOMB_EM FROM EMPLEADOS WHERE EXTTEL_EM IS NULL;

RESULTADO:

NOMB_EM
----------------------------------------
FORZADO LÓPEZ, JUAN
MACULLAS ALTO, ELOISA
PÉREZ MUÑOZ, ALFONSO

************************************************************************
8.- Listado del código, nombre y presupuesto de los departamentos ordenado por criterio descendente de presupuesto anual.

SQL>

SELECT COD_DE, NOMB_DE, PRESUP_DE FROM DEPARTAMENTOS ORDER BY PRESUP_DE DESC;

RESULTADO:

COD_D NOMB_DE                                   PRESUP_DE
----- ---------------------------------------- ----------
PROZS PRODUCCIÓN ZONA SUR                       108000000
DIRGE DIRECCIÓN GENERAL                          26000000
INYDI INVESTIGACIÓN Y DISEÑO                     25000000
ADMZS ADMINISTRACIÓN ZONA SUR                    14000000
VENZS VENTAS ZONA SUR                            13500000
JEFZS JEFATURA FÁBRICA ZONA SUR                   6200000

************************************************************************
9.- Listar el nombre del empleado y el nombre y fecha de nacimiento de su hijo/a para aquellos empleados con un único hijo. Ordenar por fecha de nacimiento de los hijos.

SQL>

SELECT EMPLEADOS.NOMB_EM, HIJOS.NOMB_HI, HIJOS.FECNAC_HI 
    FROM EMPLEADOS, HIJOS 
    WHERE EMPLEADOS.COD_EM = HIJOS.PADRE_HI 
    AND EMPLEADOS.NUMHIJ_EM = 1 ORDER BY HIJOS.FECNAC_HI DESC;

RESULTADO:
NOMB_EM
----------------------------------------
NOMB_HI                                  FECNAC_HI
---------------------------------------- ----------
MACULLAS ALTO, ELOISA
FUERTE MASCULLAS, ANACLETO               14/03/1994

MONFORTE CID, ROLDÁN
MONFORTE LEMOS, JESÚS                    12/09/1990

RUIZ DE LOPERA, MANUEL
RUIZ DENIL, SON                          07/06/1989


NOMB_EM
----------------------------------------
NOMB_HI                                  FECNAC_HI
---------------------------------------- ----------
ALADA VERAZ, JUANA
PASTORA ALADA, MATEO                     06/03/1982

************************************************************************
10.- Listar el nombre de los departamentos y del departamento del que dependen (sólo para los departamentos dependientes).

SQL>

SELECT DEPARTAMENTOS.NOMB_DE NOMBRE_DEPARTAMENTO, DEPARTAMENTO_PADRE.NOMB_DE DEPARTAMENTO_JEFE 
    FROM DEPARTAMENTOS, DEPARTAMENTOS DEPARTAMENTO_PADRE
    WHERE DEPARTAMENTOS.DEPTJEFE_DE=DEPARTAMENTO_PADRE.COD_DE;

RESULTADO:

NOMBRE_DEPARTAMENTO
----------------------------------------
DEPARTAMENTO_JEFE
----------------------------------------
INVESTIGACIÓN Y DISEÑO
DIRECCIÓN GENERAL

PRODUCCIÓN ZONA SUR
JEFATURA FÁBRICA ZONA SUR

VENTAS ZONA SUR
ADMINISTRACIÓN ZONA SUR


************************************************************************
11.- Listar el NIF, nombre del empleado y el nombre del dpto. al que se encuentra asignado ordenado por dpto. y dentro de cada dpto por el nombre de empleado.

SQL>

SELECT EMPLEADOS.DNI_EM DNI_EMPLEADO, EMPLEADOS.NOMB_EM NOMBRE_EMPLEADO, DEPARTAMENTOS.NOMB_DE NOMBRE_DEPARTAMENTO FROM EMPLEADOS, DEPARTAMENTOS
 WHERE EMPLEADOS.DEPT_EM=DEPARTAMENTOS.COD_DE ORDER BY DEPARTAMENTOS.COD_DE, NOMBRE_EMPLEADO;

RESULTADO:

DNI_EMPLE NOMBRE_EMPLEADO                          NOMBRE_DEPARTAMENTO
--------- ---------------------------------------- ----------------------------------------
38223822T ALADA VERAZ, JUANA                       ADMINISTRACIÓN ZONA SUR
21452145V RUIZ DE LOPERA, MANUEL                   DIRECCIÓN GENERAL
21232123K MANRIQUE BACTERIO, LUISA                 INVESTIGACIÓN Y DISEÑO
26452645D GOZQUE ALTANERO, CARLOS                  JEFATURA FÁBRICA ZONA SUR
47124712D FORZADO LÓPEZ, JUAN                      PRODUCCIÓN ZONA SUR
32133213H MACULLAS ALTO, ELOISA                    PRODUCCIÓN ZONA SUR
11311131D MANDO CORREA, ROSA                       PRODUCCIÓN ZONA SUR
32933293D PÉREZ MUÑOZ, ALFONSO                     PRODUCCIÓN ZONA SUR
23822382D MONFORTE CID, ROLDÁN                     VENTAS ZONA SUR
38293829L TOPAZ ILLÁN, CARLOS                      VENTAS ZONA SUR

************************************************************************
12.- Listar el salario mínimo, máximo y medio para cada dpto. indicando el código de dpto. al que pertenece el dato.

SQL>

SELECT EMPLEADOS.DEPT_EM CODIGO, 
MIN(EMPLEADOS.SALARIO_EM) SALARIO_MINIMO,
MAX(EMPLEADOS.SALARIO_EM) SALARIO_MAXIMO,
AVG(EMPLEADOS.SALARIO_EM) SALARIO_MEDIO 
FROM EMPLEADOS GROUP BY EMPLEADOS.DEPT_EM;

RESULTADO:

CODIG SALARIO_MINIMO SALARIO_MAXIMO SALARIO_MEDIO
----- -------------- -------------- -------------
ADMZS        6200000        6200000       6200000
DIRGE        7200000        7200000       7200000
INYDI        4500000        4500000       4500000
JEFZS        5000000        5000000       5000000
PROZS        1300000        3100000       1900000
VENZS        3200000        5200000       4200000

************************************************************************
13.- Listar el salario promedio de los empleados.

SQL>

SELECT AVG(EMPLEADOS.SALARIO_EM) MEDIA FROM EMPLEADOS;

RESULTADO:

     MEDIA
----------
   3890000

************************************************************************
14.- Listar el nombre de los hijos del empleado que se apellida 'Correa'.

SQL>

SELECT HIJOS.NOMB_HI NOMBRE_HIJO, EMPLEADOS.NOMB_EM NOMBRE_EMPLEADO FROM HIJOS, EMPLEADOS 
    WHERE HIJOS.PADRE_HI=EMPLEADOS.COD_EM 
    AND UPPER(EMPLEADOS.NOMB_EM) LIKE '%CORREA%';

RESULTADO:

NOMBRE_HIJO                              NOMBRE_EMPLEADO
---------------------------------------- ----------------------------------------
LEÓN MANDO, ELVIRA                       MANDO CORREA, ROSA
LEÓN MANDO, PLÁCIDO                      MANDO CORREA, ROSA

************************************************************************
15.- Listar el nombre de los departamentos en los que la suma de los sueldos es igual o mayor al 25% del presupuesto.

SQL>

SELECT DEPARTAMENTOS.NOMB_DE, 
  SUM(EMPLEADOS.SALARIO_EM) TOTAL_SALARIO
  FROM DEPARTAMENTOS, EMPLEADOS 
  WHERE DEPARTAMENTOS.COD_DE = EMPLEADOS.DEPT_EM
  GROUP BY DEPARTAMENTOS.COD_DE, DEPARTAMENTOS.PRESUP_DE, DEPARTAMENTOS.NOMB_DE
  HAVING SUM(EMPLEADOS.SALARIO_EM) >= DEPARTAMENTOS.PRESUP_DE*0.25; 

RESULTADO:

NOMB_DE                                  TOTAL_SALARIO
---------------------------------------- -------------
ADMINISTRACIÓN ZONA SUR                        6200000
DIRECCIÓN GENERAL                              7200000
JEFATURA FÁBRICA ZONA SUR                      5000000
VENTAS ZONA SUR                                8400000


************************************************************************
16.- Listar los departamentos que tengan algún empleado que gane más de 500.000 ptas al mes. (Recuerda que el salario es anual).

SQL>

SELECT DEPARTAMENTOS.NOMB_DE, EMPLEADOS.SALARIO_EM 
    FROM DEPARTAMENTOS, EMPLEADOS 
    WHERE DEPARTAMENTOS.COD_DE = EMPLEADOS.DEPT_EM 
    AND EMPLEADOS.SALARIO_EM/12 > 500000;

RESULTADO:

NOMB_DE                                  SALARIO_EM
---------------------------------------- ----------
ADMINISTRACIÓN ZONA SUR                     6200000
DIRECCIÓN GENERAL                           7200000

************************************************************************
17.- Crear la tabla TEMP(CODEMP, NOMDEPT, NOMEMP, SALEMP) cuyas columnas tienen el mismo tipo y tamaño las similares existentes en la BD. Insertar en dicha tabla el código de empleado, nombre de dpto, nombre de empleado y salario de los empleados de los centros de MURCIA.

SQL>

CREATE TABLE TEMP (CODEMP, NOMDEPT, NOMEMP, SALEMP)
AS SELECT COD_DE, NOMB_DE, NOMB_EM, SALARIO_EM FROM EMPLEADOS, DEPARTAMENTOS
	WHERE COD_DE= DEPT_EM AND CENTRO_DE IN (SELECT COD_CE FROM CENTROS
							WHERE POBLAC_CE='MURCIA');

RESULTADO:

Tabla creada.

************************************************************************
18.- Incrementar en un 10% los salarios de los empleados que ganen menos de 5.000.000 de ptas.

SQL>

UPDATE EMPLEADOS
SET SALARIO_EM= SALARIO_EM + SALARIO_EM* 0.1 
	WHERE SALARIO_EM < 5000000;

RESULTADO:

5 filas actualizadas.

************************************************************************
19.- Deshacer la operación anterior.

SQL>

ROLLBACK;

RESULTADO:

Rollback terminado.

************************************************************************
20.- Borrar la tabla TEMP y todas las anteriores.

SQL>

DROP TABLE TEMP CASCADE CONSTRAINT;
DROP TABLE HABILIDADES CASCADE CONSTRAINT;
DROP TABLE CENTROS CASCADE CONSTRAINT;
DROP TABLE DEPARTAMENTOS CASCADE CONSTRAINT;
DROP TABLE EMPLEADOS CASCADE CONSTRAINT;
DROP TABLE HIJOS CASCADE CONSTRAINT;
DROP TABLE HABIEMPL CASCADE CONSTRAINT;

RESULTADO:

Tabla borrada.


Tabla borrada.


Tabla borrada.


Tabla borrada.


Tabla borrada.


Tabla borrada.


Tabla borrada.

************************************************************************
