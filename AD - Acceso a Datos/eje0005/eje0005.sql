SQL>

Declare 
  CURSOR c1 IS
  SELECT APELLIDO FROM EMPLE WHERE DEPT_NO=20;
  v_apellido VARCHAR2(10);
BEGIN
  OPEN c1;
  LOOP
    FETCH c1 INTO v_apellido;
    DBMS_OUTPUT.PUT_LINE(To_CHAR(c1%ROWCOUNT,'99.') || v_apellido);
    EXIT WHEN c1%NOTFOUND;
  END LOOP;
  CLOSE c1;
END;

OUTPUT>

1.SANCHEZ
2.JIMENEZ
3.GIL
4.ALONSO
5.FERNANDEZ

/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
SQL>

Declare 
  CURSOR c1 IS
  SELECT APELLIDO FROM EMPLE WHERE DEPT_NO=20;
  v_apellido VARCHAR2(10);
BEGIN
  OPEN c1;
  LOOP
    FETCH c1 INTO v_apellido;
    EXIT WHEN c1%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE(To_CHAR(c1%ROWCOUNT,'99.') || v_apellido);
  END LOOP;
  CLOSE c1;
END;

OUTPUT>

1.SANCHEZ
2.JIMENEZ
3.GIL
4.ALONSO
5.FERNANDEZ

/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/
SQL>

DECLARE 
  CURSOR c1 IS
  SELECT APELLIDO FROM EMPLE WHERE DEPT_NO=20;
  v_apellido VARCHAR2(10);
BEGIN
  OPEN c1;
    FETCH c1 INTO v_apellido;
    WHILE c1%FOUND
  LOOP
    DBMS_OUTPUT.PUT_LINE(To_CHAR(c1%ROWCOUNT,'99.') || v_apellido);
    FETCH c1 INTO v_apellido;
  END LOOP;
  CLOSE c1;
END;

OUTPUT>

1.SANCHEZ
2.JIMENEZ
3.GIL
4.ALONSO
5.FERNANDEZ

/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/