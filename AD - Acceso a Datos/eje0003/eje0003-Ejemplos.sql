
1)

SQL>

 BEGIN 
   dbms_output.put_line('Hola mundo');
 END;

// Se debe activar output : "set serveroutput on;"

OUTPUT>

Hola mundo

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

2)

SQL>

DECLARE 
  v_num NUMBER;
BEGIN
  SELECT count(*) INTO v_num
    FROM productos;
  DBMS_OUTPUT.PUT_LINE(v_num);
END;

OUTPUT>

9

Procedimiento PL/SQL terminado correctamente.

DESCRIPCI�N>

Este bloque PL/SQL cuenta el numero de filas en la tabla productos y devuelve
el numero resultante por consola.

*************************************************************************************

4)

SQL>

DECLARE 
  v_num NUMBER;
BEGIN
  SELECT count(*)
    FROM productos;
  DBMS_OUTPUT.PUT_LINE(v_num);
END;

OUTPUT>

ERROR : se espera una cl�usula INTO en esta sentencia SELECT

*************************************************************************************

5)

OUTPUT>

9

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

3)

SQL>

EXECUTE ver_depart(10)

OUTPUT>

NUM depart: 10 * Nombre dep: CONTABILIDAD * Localidad: SEVILLA

Procedimiento PL/SQL terminado correctamente.


*************************************************************************************

7)

CABECERA>

 CREATE OR REPLACE
 PROCEDURE modificar_precio_producto
  (numproducto NUMBER, nuevoprecio NUMBER)
  AS
    v_precioant NUMBER(5);

Nombre del procedimiento>

  modificar_precio_producto

Los paramentros del procedimiento>

  numproducto NUMBER, nuevoprecio NUMBER

Las variables locales>

  v_precioant NUMBER(5)

El comienzo y el final del bloque PL/SQL>

  CREATE

  END modificar_precio_producto;

El comienzo y el final de las decciones declarativas, >
ejecuci�n y de gesti�n de excepciones. >

declaracion:

AS
  v_precioant NUMBER(5);

excepci�n es:

	EXCEPTION
    	  WHEN NO_DATA_FOUND THEN
      		DBMS_OUTPUT.PUT_LINE('No encontrado producto '|| numproducto);

�Que hace la clausula INTO?>

A�ade un valor a la variable seleci�nada

�Que hace WHEN NO_DATA_FOUND?

Entra en la funci�n WHEN cuando no hay datos disponibles para su procesamiento.

�Por que no tiene la clausula DECLARE?

En vez de DECLARE se ha utilizado AS para realizar la declaraci�n de la variable v_precioant.

*************************************************************************************