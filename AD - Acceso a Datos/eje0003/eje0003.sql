1)

SQL>

DECLARE
    v_num_empleados NUMBER(2);
BEGIN
  INSERT INTO depart VALUES (99, 'PROVISIONAL', NULL);
  UPDATE emple SET dept_no = 99
    WHERE dept_no = 20;
  v_num_empleados := SQL%ROWCOUNT;
  DELETE FROM depart
    WHERE dept_no = 20;
    DBMS_OUTPUT.PUT_LINE(v_num_empleados || ' Empleados ubicados en PROVISIONAL');
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, 'Error en aplicación');
  END;

OUTPUT>

5 Empleados ubicados en PROVISIONAL

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

2) 

SQL>

DECLARE
  v_ape VARCHAR2(10);
  v_oficio VARCHAR2(10);
BEGIN
  SELECT apellido, oficio INTO v_ape, v_oficio
    FROM EMPLE WHERE EMP_NO = 7900;
  DBMS_OUTPUT.PUT_LINE(v_ape||'*'||v_oficio);
END;

OUTPUT>

JIMENO*EMPLEADO

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

3) 

SQL>

DECLARE
  v_ape VARCHAR2(10);
  v_oficio VARCHAR2(10);
BEGIN
  SELECT apellido, oficio INTO v_ape, v_oficio
    FROM EMPLE WHERE EMP_NO = 7900;
  DBMS_OUTPUT.PUT_LINE(v_ape||'*'||v_oficio);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR(-20000, 'ERROR no hay datos');
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR(-20000, 'ERROR demasiados datos');
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20000, 'ERROR de la aplicación');
END;

OUTPUT>

JIMENO*EMPLEADO

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

4)

SQL>

CREATE OR REPLACE TRIGGER audit_borrado_emple
    BEFORE DELETE
    ON emple
  FOR EACH ROW
    BEGIN
      DBMS_OUTPUT.PUT_LINE('BORRADO EMPLEADO'||'*'||:old.emp_no||'*'||:old.apellido);
    END;

OUTPUT>

Disparador creado.

*************************************************************************************

5)

SQL>

DECLARE
  v_nom CLIENTES.NOMBRE%TYPE;
BEGIN
  SELECT nombre INTO v_nom
    FROM clientes
    WHERE NIF=&vn_cli;
  DBMS_OUTPUT.PUT_LINE(v_nom);
END;

OUTPUT>

Introduzca un valor para vn_cli: antiguo   4:     WHERE NIF=&vn_cli;
nuevo   4:     WHERE NIF=131341;

*************************************************************************************

6)

SQL>

CREATE OR REPLACE 
PROCEDURE ver_depart (numdepart NUMBER) 
AS
  v_dnombre VARCHAR2(14);
  v_localidad VARCHAR2(14);
BEGIN
  SELECT dnombre, loc INTO v_dnombre, v_localidad
    FROM depart
    WHERE dept_no = numdepart;
  DBMS_OUTPUT.PUT_LINE('NUM depart: '||numdepart||' * Nombre dep: '||v_dnombre||
                        ' * Localidad: '||v_localidad);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('No se ha encontrado el departamento');
END ver_depart;

// EXECUTE ver_depart(10)

OUTPUT>

NUM depart: 30 * Nombre dep: VENTAS * Localidad: BARCELONA

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

7.1)

SQL>

CREATE OR REPLACE
  PROCEDURE ver_precio(v_num_producto NUMBER)
AS
  v_precio NUMBER;
  BEGIN
    SELECT precio_uni INTO v_precio
      FROM productos
      WHERE cod_producto = v_num_producto;
      DBMS_OUTPUT.PUT_LINE('Precio = '||v_precio);
  END;

// EXECUTE ver_precio(1)
OUTPUT>

Precio = 15000

Procedimiento PL/SQL terminado correctamente.

*************************************************************************************

7.2)

SQL>

CREATE OR REPLACE
PROCEDURE modificar_precio_producto
  (numproducto NUMBER, nuevoprecio NUMBER)
  AS
    v_precioant NUMBER(5);
  BEGIN
    SELECT precio_uni INTO v_precioant
      FROM productos
      WHERE cod_producto = numproducto;
    IF (v_precioant * 0.20) > (nuevoprecio - v_precioant) THEN
      UPDATE productos SET precio_uni = nuevoprecio
        WHERE cod_producto = numproducto;
    ELSE
      DBMS_OUTPUT.PUT_LINE('ERROR, modificación supera 20%');
    END IF;    
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('No encontrado producto '|| numproducto);
END modificar_precio_producto;

// EXECUTE modificar_precio_producto(1,1400)

OUTPUT>

Procedimiento PL/SQL terminado correctamente.	

*************************************************************************************

7.3)

SQL>

CREATE OR REPLACE
FUNCTION con_iva (
  cantidad NUMBER,
  tipo NUMBER DEFAULT 16)
  
  RETURN NUMBER AS
    v_resultado NUMBER (10,2) DEFAULT 0;
  BEGIN
    v_resultado := cantidad * (1 + (tipo / 100));
    RETURN(v_resultado);
END con_iva;

OUTPUT>

Función creada.

*************************************************************************************