
--************************************************************************************************
1.- Dise�a el procedimiento 'MediasAsig' que reciba como par�metro el c�digo de una asignatura y actualice el campo 'Notamedia_as' de la tabla 'Asignaturas' al valor medio de las notas de los alumnos que est�n matriculados. Para ello se utilizar� el valor del campo 'Nota_ma'. Para el c�lculo de la media de los alumnos repetidores se considerar� el campo 'Nota_ma' de la siguiente manera:
	- Si est� aprobado, un 5, independientemente de la nota que tenga.
	- Si est� suspenso, el valor de su nota, el campo 'Nota_ma'.

C�digo:

--************************************************************************************************
2.- Dise�a la funci�n 'CalculaMedia' que reciba como par�metro el c�digo de un alumno y devuelva la media de las notas de las asignaturas en las que est� matriculado.

C�digo:

--************************************************************************************************
3.- Dise�a el procedimiento 'MediasAlumno' que modifique en todos los registros, el campo 'Notamedia_al' de la tabla 'Alumnos' al valor de la media de las notas de las asignaturas en las que est� matriculado.

C�digo:

--************************************************************************************************
4.- Dise�a el procedimiento 'CuentaNotas' que reciba como par�metros de entrada el c�digo de una asignatura y el valor de una nota, y devuelva el n�mero de alumnos que tienen esa nota de los matriculados en esa asignatura y cuantos de ellos son repetidores.
(Nota: Recuerda la opci�n OUT en los argumentos de un procedimiento)

C�digo:

--************************************************************************************************
5.- Dise�a el procedimiento 'ListaAsignatura' que reciba como argumento el c�digo de una asignatura y muestre el nombre de la asignatura, el nombre del profesor que la imparte y los nombres de los alumnos matriculados en ella, indicando si es o no repetidor y el n�mero de asignaturas en las que se ha matriculado.
------------------------------------------------------

C�digo:

--************************************************************************************************

