﻿
--************************************************************************************************
1.- Dise�a el procedimiento 'MediasAsig' que reciba como par�metro el c�digo de una asignatura y actualice el campo 'Notamedia_as' de la tabla 'Asignaturas' 
al valor medio de las notas de los alumnos que est�n matriculados. Para ello se utilizar� el valor del campo 'Nota_ma'. Para el c�lculo de la media de los alumnos 
repetidores se considerar� el campo 'Nota_ma' de la siguiente manera:
	- Si est� aprobado, un 5, independientemente de la nota que tenga.
	- Si est� suspenso, el valor de su nota, el campo 'Nota_ma'.

C�digo:

CREATE OR REPLACE PROCEDURE MediasAsig(
    v_cod_asig ASIGNATURAS.CODIGO_AS%TYPE)
AS
    CURSOR c_matricula IS 
      SELECT nota_ma, repite_ma FROM matriculas WHERE codas_ma = v_cod_asig;
    v_cont_nota NUMBER(3) DEFAULT 0;
    v_cont_alumnos NUMBER(3) DEFAULT 0;
    v_media NUMBER(5,2) DEFAULT 0;
BEGIN
    FOR rs in c_matricula LOOP
      /* Recorremos los registros de la asignatura obteniendo la nota segun los criterios dados */
      IF (rs.repite_ma = 'S') THEN
          IF(rs.nota_ma>=5) THEN
              v_cont_nota := v_cont_nota + 5;
          ELSE
              v_cont_nota := v_cont_nota + rs.nota_ma;
          END IF;
      ELSIF (rs.repite_ma = 'N') THEN
          v_cont_nota := v_cont_nota + rs.nota_ma;
      END IF;
      /* Se amplia el contador de alumnos*/
      v_cont_alumnos := v_cont_alumnos + 1;
    END LOOP;    
    /* Se calcula la media*/
    v_media := v_cont_nota / v_cont_alumnos;
    /*Se actualiza la media de la asignatura*/
    UPDATE asignaturas SET notamedia_as = v_media WHERE codigo_as = v_cod_asig;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('No se ha actualizado ninguna fila.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE|| '-' ||SQLERRM );
END MediasAsig;

//Prueba

EXECUTE MediasAsig(1);

 CODIGO_AS NOMBRE_AS                      PROFESOR_AS NOTAMEDIA_AS
---------- ------------------------------ ----------- ------------
         1 Desarrollo de interfaces                 1         5,29

--************************************************************************************************
2.- Dise�a la funci�n 'CalculaMedia' que reciba como par�metro el c�digo de un alumno y devuelva la media de las notas de las asignaturas en las que est� matriculado.

C�digo:

CREATE OR REPLACE FUNCTION CalculaMedia(v_cod_alumno ALUMNOS.CODIGO_AL%TYPE)
RETURN NUMBER
IS
    v_media NUMBER(5,2);
BEGIN
      /* Usamos AVG para hayar la media de las notas de un alumno*/
      SELECT AVG(nota_ma) INTO v_media FROM matriculas WHERE codal_ma = v_cod_alumno GROUP BY codal_ma;
      RETURN v_media;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('No se ha encontrado ningun registro');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE|| '-' ||SQLERRM );
END CalculaMedia;

//Prueba

BEGIN
	DBMS_OUTPUT.PUT_LINE(CalculaMedia(1));
END;

8,5

Procedimiento PL/SQL terminado correctamente.

--************************************************************************************************
3.- Dise�a el procedimiento 'MediasAlumno' que modifique en todos los registros, el campo 'Notamedia_al' de la tabla 'Alumnos' al valor de la media 
de las notas de las asignaturas en las que est� matriculado.

C�digo:

CREATE OR REPLACE PROCEDURE MediasAlumno
AS
    CURSOR c_alumnos IS 
      SELECT codigo_al, notamedia_al FROM alumnos FOR UPDATE;
    v_media NUMBER(5,2) DEFAULT 0;
BEGIN
    FOR rs in c_alumnos LOOP
        /* Recorremos la tabla alumnos y usando la funcion del ejercicio anterior,
         hayamos la media y actualizamos*/
        v_media := CalculaMedia(rs.codigo_al);
        /*Actualizamos el alumno*/   
        UPDATE alumnos SET notamedia_al = v_media WHERE CURRENT OF c_alumnos;      
    END LOOP; 
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('No se ha actualizado ninguna fila.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE|| '-' ||SQLERRM );
END MediasAlumno;

//Prueba

EXECUTE MediasAlumno;

 CODIGO_AL APELLIDOS_AL         NOMBRE_AL  DOMICILIO_AL         LOCALIDAD_ NOTAMEDIA_AL
---------- -------------------- ---------- -------------------- ---------- ------------
         1 SANCHEZ LOPEZ        MARIA      c/ Feria 100         SEVILLA             8,5
         2 MARTIN VIZCAINO      JESUS      c/ Sierpes 3         SEVILLA            4,67
         3 VERDE PEREZ          JOSE A.    c/ Sierpes 3         SEVILLA            7,33
         4 AGUILAR BERNARDEZ    DANIEL     c/ Sierpes 3         SEVILLA            7,33
         5 FRANCIS MONTES       JESUS      c/ Real 3            CASTILLEJA            4
         6 ARAGON GARCIA        DANIEL     c/ Sierpes 3         SEVILLA               8
         7 PEREZ DOMINGUEZ      DIEGO J.   c/ Sierpes 3         SEVILLA             1,5
         8 LOPEZ MARIN          DAVID      c/ Real 3            CASTILLEJA          7,5

--************************************************************************************************
4.- Dise�a el procedimiento 'CuentaNotas' que reciba como par�metros de entrada el c�digo de una asignatura y el valor de una nota, y devuelva el n�mero de alumnos 
que tienen esa nota de los matriculados en esa asignatura y cuantos de ellos son repetidores.
(Nota: Recuerda la opci�n OUT en los argumentos de un procedimiento)

C�digo:

CREATE OR REPLACE PROCEDURE CuentaNotas(
    v_cod_asig IN MATRICULAS.CODAS_MA%TYPE,
    v_nota_asig IN MATRICULAS.NOTA_MA%TYPE,
    v_numero_al OUT NUMBER,
    v_al_rep OUT NUMBER)
AS
BEGIN
      SELECT COUNT(*) INTO v_numero_al FROM matriculas WHERE codas_ma = v_cod_asig AND nota_ma = v_nota_asig;
      SELECT COUNT(*) INTO v_al_rep FROM matriculas WHERE codas_ma = v_cod_asig AND nota_ma = v_nota_asig AND repite_ma = 'S';
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('No se ha encontrado ningun registro');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE|| '-' ||SQLERRM );
END CuentaNotas;



//Prueba1

DECLARE
    v_total_alum NUMBER DEFAULT 0;
    v_repetidores NUMBER DEFAULT 0;
BEGIN
  CuentaNotas(1,7,v_total_alum,v_repetidores);
	DBMS_OUTPUT.PUT_LINE('Alumnos en total: ' || v_total_alum);
	DBMS_OUTPUT.PUT_LINE('Repetidores: ' || v_repetidores);
END;

REsultado:

Alumnos en total: 2
Repetidores: 2

Procedimiento PL/SQL terminado correctamente.

//Prueba2

DECLARE
    v_total_alum NUMBER DEFAULT 0;
    v_repetidores NUMBER DEFAULT 0;
BEGIN
  CuentaNotas(2,5,v_total_alum,v_repetidores);
	DBMS_OUTPUT.PUT_LINE('Alumnos en total: ' || v_total_alum);
	DBMS_OUTPUT.PUT_LINE('Repetidores: ' || v_repetidores);
END;

REsultado:

Alumnos en total: 2
Repetidores: 0

Procedimiento PL/SQL terminado correctamente.


--************************************************************************************************
5.- Dise�a el procedimiento 'ListaAsignatura' que reciba como argumento el c�digo de una asignatura y muestre el nombre de la asignatura, 
el nombre del profesor que la imparte y los nombres de los alumnos matriculados en ella, indicando si es o no repetidor y el n�mero de asignaturas en las que se ha matriculado.
------------------------------------------------------

C�digo:

CREATE OR REPLACE PROCEDURE ListaAsignatura(
    v_cod_asig IN ASIGNATURAS.CODIGO_AS%TYPE)
AS
    /*Variables primera parte*/
    v_nombre_asignatura ASIGNATURAS.NOMBRE_AS%TYPE;
    v_nombre_profesor PROFESORES.NOMBRE_PR%TYPE;
    v_apellidos_profesor PROFESORES.APELLIDOS_PR%TYPE;
    /*Variables segunda parte*/
    CURSOR c_matricula IS 
      SELECT codas_ma, codal_ma, codigo_al, 
      nombre_al, apellidos_al, repite_ma 
      FROM matriculas, alumnos WHERE codas_ma = v_cod_asig 
      AND codal_ma = codigo_al;
     /*Variables tercera parte*/
     v_cont NUMBER; 
     
BEGIN
      /* Primero hacemos un select para ver el nombre de la asignatura y su profesor */
      SELECT nombre_as, nombre_pr, apellidos_pr 
      INTO v_nombre_asignatura, v_nombre_profesor, v_apellidos_profesor 
      FROM asignaturas, profesores WHERE profesor_as = codigo_pr 
      AND codigo_as = v_cod_asig;
      
      /*Mostramos*/
      DBMS_OUTPUT.PUT_LINE('----------------------------------------');
      DBMS_OUTPUT.PUT_LINE('Asignatura: ' || v_nombre_asignatura);
      DBMS_OUTPUT.PUT_LINE('Profesor: ' || v_nombre_profesor || ' ' || v_apellidos_profesor);
      DBMS_OUTPUT.PUT_LINE('----------------------------------------');
      
      /* Proseguimos con la segunda parte y recorremos el cursor para ver los alumnos que dan la asignatura*/
      DBMS_OUTPUT.PUT_LINE('Alumnos de la asignatura: ');
      DBMS_OUTPUT.PUT_LINE('----------------------------------------');
      FOR rs in c_matricula LOOP
          DBMS_OUTPUT.PUT_LINE('Nombre: ' || rs.nombre_al || ' ' || rs.apellidos_al);
          DBMS_OUTPUT.PUT_LINE('Repetidor: ' || rs.repite_ma);
          /*Obtenemos las asignaturas a las que asiste el alumno*/
          SELECT COUNT(codas_ma) INTO v_cont FROM matriculas WHERE codal_ma = rs.codal_ma;          
          DBMS_OUTPUT.PUT_LINE('Asignaturas dadas por el alumno: ' || v_cont);
          DBMS_OUTPUT.PUT_LINE('----------------------------------------');
      END LOOP;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
	      DBMS_OUTPUT.PUT_LINE('No se ha encontrado ningun registro');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLCODE|| '-' ||SQLERRM );
END ListaAsignatura;

//Prueba

Execute ListaAsignatura(1);

----------------------------------------
Asignatura: Desarrollo de interfaces
Profesor: ALBERTO RIQUELME CALVO
----------------------------------------
Alumnos de la asignatura:
----------------------------------------
Nombre: JESUS MARTIN VIZCAINO
Repetidor: N
Asignaturas dadas por el alumno: 3
----------------------------------------
Nombre: JOSE A. VERDE PEREZ
Repetidor: S
Asignaturas dadas por el alumno: 3
----------------------------------------
Nombre: DANIEL AGUILAR BERNARDEZ
Repetidor: N
Asignaturas dadas por el alumno: 3
----------------------------------------
Nombre: JESUS FRANCIS MONTES
Repetidor: N
Asignaturas dadas por el alumno: 2
----------------------------------------
Nombre: DANIEL ARAGON GARCIA
Repetidor: S
Asignaturas dadas por el alumno: 2
----------------------------------------
Nombre: DIEGO J. PEREZ DOMINGUEZ
Repetidor: N
Asignaturas dadas por el alumno: 2
----------------------------------------
Nombre: DAVID LOPEZ MARIN
Repetidor: N
Asignaturas dadas por el alumno: 2
----------------------------------------

Procedimiento PL/SQL terminado correctamente.



--************************************************************************************************

