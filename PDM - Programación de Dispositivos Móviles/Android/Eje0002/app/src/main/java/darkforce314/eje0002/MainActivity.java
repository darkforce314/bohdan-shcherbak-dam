package darkforce314.eje0002;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    String name,email,pass;
    int repeats,rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.name = "";
        this.email = "";
        this.pass = "";
        this.repeats = 0;
        this.rating = 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void formBtnClick(View view){
        Intent intent = new Intent(this,FormActivity.class);
        intent.putExtra("name",this.name);
        intent.putExtra("email",this.email);
        intent.putExtra("pass",this.pass);
        startActivityForResult(intent,1);
    }

    public void rateBtnClick(View view){
        Intent intent = new Intent(this,RateActivity.class);
        startActivityForResult(intent,2);
    }

    public void dataBtnClick(View view){
        Intent intent = new Intent(this,DataActivity.class);
        intent.putExtra("name",this.name);
        intent.putExtra("email",this.email);
        intent.putExtra("repeats",this.repeats);
        intent.putExtra("rating",this.rating);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            this.name = data.getStringExtra("name");
            this.email = data.getStringExtra("email");
            this.pass  = data.getStringExtra("pass");
        }else if(requestCode == 2 && resultCode == RESULT_OK && data != null) {
            this.rating += data.getIntExtra("rating",0);
            this.repeats++;
        }
    }
}
