package darkforce314.eje0002;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;


public class DataActivity extends ActionBarActivity {

    TextView namelabel,emaillabel,repeatslabel,ratelabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        this.namelabel= (TextView) findViewById(R.id.nameLabel);
        this.emaillabel = (TextView) findViewById(R.id.emailLabel);
        this.repeatslabel = (TextView) findViewById(R.id.repeatsLabel);
        this.ratelabel = (TextView) findViewById(R.id.ratingLabel);

        Bundle bundle = getIntent().getExtras();
        this.namelabel.setText(this.namelabel.getText() + bundle.getString("name"));
        this.emaillabel.setText(this.emaillabel.getText() + bundle.getString("email"));
        this.repeatslabel.setText(this.repeatslabel.getText() +""+ bundle.getInt("repeats"));
        if (bundle.getInt("repeats")>0) {
            this.ratelabel.setText(this.ratelabel.getText() + "" + bundle.getInt("rating") / bundle.getInt("repeats"));
        }else{
            this.ratelabel.setText(this.ratelabel.getText() + "0");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
