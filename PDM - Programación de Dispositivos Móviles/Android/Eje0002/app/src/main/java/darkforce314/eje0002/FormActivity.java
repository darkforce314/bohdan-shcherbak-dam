package darkforce314.eje0002;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class FormActivity extends ActionBarActivity {

    EditText name,email,pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        this.name = (EditText) findViewById(R.id.formNameInput);
        this.email = (EditText) findViewById(R.id.formEmailInput);
        this.pass = (EditText) findViewById(R.id.formPassInput);

        Bundle bundle = getIntent().getExtras();
        this.name.setText(bundle.getString("name"));
        this.email.setText(bundle.getString("email"));
        this.pass.setText(bundle.getString("pass"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonOkClick(View viev){
        Intent output = new Intent();

        output.putExtra("name", this.name.getText().toString());
        output.putExtra("email", this.email.getText().toString());
        output.putExtra("pass", this.pass.getText().toString());

        setResult(RESULT_OK, output);
        this.finish();
    }
    public void buttonCancelClick(View viev){
        Intent output = new Intent();
        setResult(RESULT_CANCELED, output);
        this.finish();
    }
}
