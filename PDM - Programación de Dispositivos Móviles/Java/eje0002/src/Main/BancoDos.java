package Main;

/**
 *
 * @author darkforce314
 */
public class BancoDos {
    /*
     - Crear cuenta sin saldo inicial.
     - Crear cuenta con saldo inicial.
     - Operar con una cuenta. Permite seleccionar una cuenta y abre el siguiente submenu:
     ---- Ingresar dinero en una cuenta.
     ---- Sacar dinero de una cuenta
     ---- Listar una cuenta en concreto.
     ---- Volver al menú principal.
     - Listar todas las cuentas del banco.
     */
    private static String titular;
    private static int input, cuenta, saldo;
    private static CuentaDos data;

    public static void main(String[] args) {
        printMenu();
    }

    //Funciónes para iprimir menús en la consola y su lógica
    private static void printMenu() {
        clearCts();
        System.out.println("Menú Principal");
        System.out.println("-------------------------------------");
        System.out.println("1. Crear cuenta sin saldo");
        System.out.println("2. Crear cuenta con saldo");
        System.out.println("3. Operar con una cuenta");
        System.out.println("4. Listar todas las cuentas del banco\n");
        System.out.println("-------------------------------------");
        System.out.println("0. SALIR                             |");
        System.out.println("-------------------------------------");
        input = Entrada.entero();
        switch (input) {
            case 0:
                clearCts();
                System.exit(0);
                break;
            case 1:
                clearCts();
                printTitularTrace();
                data = new CuentaDos(titular);
                clearData();
                printMenu();
                break;
            case 2:
                clearCts();
                printTitularTrace();
                clearCts();
                printSaldoTrace();
                data = new CuentaDos(titular,saldo);
                clearData();
                printMenu();
                break;
            case 3:
                printSubMenu();
                break;
            case 4:
                clearCts();
                CuentaDos.listarCuentas();
                printResultTrace();
                clearData();
                printMenu();
                break;

            default:
                clearCts();
                clearData();
                printMenu();
                break;
        }
    }

    private static void printSubMenu() {
        clearCts();
        clearData();
        System.out.println("Operaciónes con las cuentas");
        System.out.println("-------------------------------------");
        System.out.println("1. Ingresar dinero en una cuenta");
        System.out.println("2. Sacar dinero de una cuenta");
        System.out.println("3. Listar una cuenta en concreto\n");
        System.out.println("-------------------------------------");
        System.out.println("0. VOLVER      3"
                + "Cuentas disponibles: "+data.totalCuentas+"|");
        System.out.println("-------------------------------------");
        input = Entrada.entero();
        switch (input) {
            case 0:
                clearCts();
                clearData();
                printMenu();
                break;
            case 1:                
                clearCts();
                printCuentaTrace();
                clearCts();
                printIngresoTrace();
                data.ingresar(cuenta, saldo);
                clearData();
                printSubMenu();
                break;
            case 2:                
                clearCts();
                printCuentaTrace();
                clearCts();
                printSacarTrace();
                data.buscar(cuenta).sacar(saldo);
                clearData();
                printSubMenu();
                break;
            case 3:                                
                clearCts();
                printCuentaTrace();
                clearCts();
                System.out.println(data.buscar(cuenta).toString());
                printResultTrace();
                clearData();
                printSubMenu();
                break;

            default:
                clearCts();
                clearData();
                printSubMenu();
                break;
        }
    }

    //Funciónes para pedir variable por consola
    private static void printTitularTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Ingrese el NOMBRE del titular:       |");
        System.out.println("-------------------------------------");
        titular = Entrada.cadena();
    }

    private static void printCuentaTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Ingrese el NUMERO de la cuenta:      |");
        System.out.println("-------------------------------------");
        cuenta = Entrada.entero();
    }

    private static void printSaldoTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Ingrese el SALDO inicial:            |");
        System.out.println("-------------------------------------");
        saldo = Entrada.entero();
    }

    private static void printIngresoTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Ingrese el SALDO a ingresar:         |");
        System.out.println("-------------------------------------");
        saldo = Entrada.entero();
    }

    private static void printSacarTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Ingrese el SALDO a sacar:            |");
        System.out.println("-------------------------------------");
        saldo = Entrada.entero();
    }    

    private static void printResultTrace() {
        System.out.println("-------------------------------------");
        System.out.println("Pulse INTRO para volver:             |");
        System.out.println("-------------------------------------");
        Entrada.cadena();
    }

    //Funciónes secundarias
    //Limpiar la consola
    private static void clearCts() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    //Limpiar los datos
    private static void clearData() {
        titular = null;
        input = 0;
        cuenta = 0;
        saldo = 0;
    }
}
