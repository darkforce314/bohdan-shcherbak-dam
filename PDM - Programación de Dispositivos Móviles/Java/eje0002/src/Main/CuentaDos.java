package Main;

/**
 *
 * @author darkforce314
 */
public class CuentaDos {

    //Declaración de variables
    public String titular;
    public int numero, saldo;
    public CuentaDos siguienteCuenta;
    public static int totalCuentas;
    private static CuentaDos cuentaInicial = null;

    //Constructores
    public CuentaDos(String titular) {
        this.titular = titular;
        this.saldo = 0;
        addCuenta();
    }

    public CuentaDos(String titular, int saldo) {
        this.titular = titular;
        this.saldo = saldo;
        addCuenta();
    }

    //Métodos públicos
    public static CuentaDos buscar(int numero) {
        CuentaDos aux = cuentaInicial;
        if (existe(numero)) {
            CuentaDos cuentaAux = cuentaInicial;
            while (cuentaAux.numero != numero) {
                cuentaAux = cuentaAux.siguienteCuenta;
                if (cuentaAux.numero == numero) {
                    aux = cuentaAux;
                }
            }
        }
        return aux;
    }

    public static boolean ingresar(int numero, int cantidad) {
        boolean res = false;
        if (cantidad > 0) {
            try {
                buscar(numero).saldo += cantidad;
                res = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public boolean sacar(int cantidad) {
        boolean res = false;
        if (cantidad > 0) {
            try {
                CuentaDos aux = buscar(numero);
                if (aux.saldo - cantidad > 0) {
                    aux.saldo -= cantidad;
                    res = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public static void listarCuentas() {
        CuentaDos cuentaAux = cuentaInicial;
        while (cuentaAux != null) {
            System.out.println(cuentaAux);
            cuentaAux = cuentaAux.siguienteCuenta;         
        }
    }

    @Override
    public String toString() {
        return "Cuenta "+ this.numero + ", Titular: " + this.titular
                + ", saldo: " + this.saldo + "€";
    }

    //Métodos privados
    private static boolean existe(int numero) {
        boolean res = false;
        CuentaDos cuentaAux = cuentaInicial;
        while (cuentaAux.numero != numero) {
            cuentaAux = cuentaAux.siguienteCuenta;
            if (cuentaAux.numero == numero) {
                res = true;
            }
        }
        return res;
    }

    private static void updateTotal() {
        int res = 0;
        CuentaDos cuentaAux = cuentaInicial;
        while (cuentaAux != null) {
            cuentaAux = cuentaAux.siguienteCuenta;
            res++;
        }
        totalCuentas = res;
    }
    //Método principal de la clase, añade una cuenta
    //depenndiendo del estado de la lista, la designa como 
    //"inicial" o añade link a la anterior cuenta, apuntando a si misma 
    private void addCuenta() {
        this.siguienteCuenta = null;
        if (cuentaInicial == null) {
            cuentaInicial = this;
            this.numero = 0;
        } else {
            CuentaDos cuentaAux = cuentaInicial;
            int count = 1;
            while (cuentaAux.siguienteCuenta != null) {
                cuentaAux = cuentaAux.siguienteCuenta;
                count++;
            }
            this.numero = count++;
            cuentaAux.siguienteCuenta = this;
        }
        updateTotal();
    }
}
