/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objs;

import java.io.Serializable;

/**
 *
 * @author darkforce314
 */
public class Package implements Serializable{
    private String type;
    private Object data;

    public Package() {
        this.type = "ECHO";
        this.data = null;
    }
    public Package(String type){
        this.type = type;
        this.data = null;
    }
    public Package(String type, Object data) {
        this.type = type;
        this.data = data;
    }
    
    public String getType(){
        return this.type;
    }
    
    public Object getData(){
        return this.data;
    }
}
