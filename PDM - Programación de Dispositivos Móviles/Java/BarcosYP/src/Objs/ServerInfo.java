/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objs;

import Assets.Engine;
import Assets.Server;
import Assets.SubnetUtils.SubnetInfo;
import java.io.Serializable;

/**
 *
 * @author darkforce314
 */
public class ServerInfo implements Serializable,LocalInterfaces.infoInterface{
    private String name;
    private SubnetInfo info;
    private int maxClients,clients;

    public ServerInfo() {
        this.name = Server.name;
        this.info = Engine.hostInfo;
        this.maxClients = Server.maxClients;
        this.clients = Server.clients;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public SubnetInfo getInfo() {
        return this.info;
    }
    
}
