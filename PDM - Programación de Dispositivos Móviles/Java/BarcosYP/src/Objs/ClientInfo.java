/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objs;

import Assets.Engine;
import Assets.SubnetUtils.SubnetInfo;
import java.io.Serializable;

/**
 *
 * @author darkforce314
 */
public class ClientInfo implements Serializable,LocalInterfaces.infoInterface{
    private String name;
    private SubnetInfo info;

    public ClientInfo() {
        this.name = "CLIENT-TEST";
        this.info = Engine.hostInfo;
    }

    public ClientInfo(String name) {
        this.name = name;
        this.info = Engine.hostInfo;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public SubnetInfo getInfo() {
        return this.info;
    }
}
