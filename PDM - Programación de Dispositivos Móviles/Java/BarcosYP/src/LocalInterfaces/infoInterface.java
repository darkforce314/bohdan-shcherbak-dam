/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LocalInterfaces;

import Assets.SubnetUtils.SubnetInfo;

/**
 *
 * @author darkforce314
 */
public interface infoInterface {
    String name = "DEFAULT";
    SubnetInfo info  = null;
    
    public String getName();
    public SubnetInfo getInfo();
}
