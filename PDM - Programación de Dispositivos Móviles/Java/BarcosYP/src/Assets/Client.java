/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assets;

import Assets.SubnetUtils.SubnetInfo;
import LocalInterfaces.infoInterface;
import Objs.ServerInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;

/**
 *
 * @author darkforce314
 */
public class Client {

    SubnetInfo hostInfo;
    int port = 314;
    LinkedList<String> serversAwaylable = new LinkedList<>();

    public Client(SubnetInfo ln, int port) {
        this.hostInfo = ln;
        this.port = port;
    }

    public void scan() {
        for (String address : hostInfo.getAllAddresses()) {
            (new Thread() {
                public void run() {
                    ping(address, port);
                }
            }).start();
        }
    }

    private boolean ping(String ip, int port) {
        try {
            Socket s = new Socket(ip, port);
            //s.setSoLinger(true, 0);
            getServerInfo(s);
            serversAwaylable.add(ip);
            s.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private ServerInfo getServerInfo(Socket socket) {
        try {
            ObjectOutputStream objectOutput = new ObjectOutputStream(socket.getOutputStream());
            objectOutput.writeObject(new Objs.Package("INFOREQUEST"));
            ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream());
            Objs.Package object = (Objs.Package) objectInput.readObject();            
            infoInterface aux = (infoInterface) object.getData();
            System.out.println("Server INFO: IP-"+aux.getInfo().getAddress()+" Name-"+aux.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void mensage(String ip) {
        Socket socket = new Socket();
        try {
            String mensaje = "Hola Mundo!";
            // Creamos el socket y establecemos la conexión con el servidor
            socket = new Socket(ip, port);
            // Establecemos un timeout de 30 segs
            socket.setSoTimeout(30000);
            System.out.println("CLIENTE: Conexion establecida con "
                    + ip + " al puerto " + port);
            // Establecemos el canal de entrada
            BufferedReader sEntrada = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            // Establecemos el canal de salida
            PrintWriter sSalida = new PrintWriter(socket.getOutputStream(), true);
            System.out.println("CLIENTE: Enviando " + mensaje);
            // Enviamos el mensaje al servidor
            sSalida.println(mensaje);
            // Recibimos la respuesta del servidor
            String recibido = sEntrada.readLine();
            System.out.println("CLIENTE: Recibido " + recibido);
            // Cerramos los flujos y el socket para liberar la conexión
            sSalida.close();
            sEntrada.close();

        } catch (SocketTimeoutException e) {
            System.err.println("30 segs sin recibir nada");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
