/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assets;

import java.io.*;
import java.net.*;
import java.util.regex.*;
import java.util.LinkedList;
import Assets.SubnetUtils.SubnetInfo;

/**
 *
 * @author darkforce314
 */
public class Engine {

    public static SubnetInfo hostInfo = null;
    public Server server;
    public Client client;

    public Engine() {
        getHostInfo();
    }

    public void startServer(int port, String name, int maxClients) {
        this.server = new Server(port, name, maxClients);
        this.server.start();
    }

    public void startClient(int port) {
        this.client = new Client(hostInfo, port);
    }

    private void getHostInfo() {
        try {
            String os = System.getProperty("os.name").toLowerCase();
            if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
                LinkedList<String> ips = getLinuxHostInfo("eth0");
                if (ips != null) {
                    SubnetUtils utils = new SubnetUtils(ips.get(0), ips.get(2));
                    this.hostInfo = utils.getInfo();
                    System.out.println(this.hostInfo.getCidrSignature());
                } else {
                    System.err.println("Error recuperando datos del HOST en UNIX");
                }
            } else {
                InetAddress ip = InetAddress.getLocalHost();
                NetworkInterface networkInterface = NetworkInterface.getByInetAddress(ip);
                int netPrefix = networkInterface.getInterfaceAddresses().get(0).getNetworkPrefixLength();
                SubnetUtils utils = new SubnetUtils(ip.getHostAddress() + "/" + netPrefix);
                this.hostInfo = utils.getInfo();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private LinkedList<String> getLinuxHostInfo(String type) {
        BufferedReader outputReader = null;
        String command = "ifconfig " + type;
        String line;
        LinkedList<String> result = null;
        try {
            Process proc = Runtime.getRuntime().exec(command);
            outputReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            while (outputReader.ready()) {
                line = outputReader.readLine();
                // see if the netmask is in this line
                if (line.contains("Másc") || line.contains("Mask")) {
                    result = getIpsFromString(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private LinkedList<String> getIpsFromString(String data) {
        LinkedList<String> result = new LinkedList<>();
        LinkedList<String> strings = new LinkedList<>();
        while (data.length() >= 8) {
            int startIndex = data.indexOf(":") + 1;
            if (startIndex > 0) {
                data = data.substring(startIndex, data.length());
                int stopIndex = data.indexOf(" ");
                if (stopIndex > 0) {
                    strings.add(data.substring(0, stopIndex));
                    data = data.substring(stopIndex, data.length());
                }
            } else {
                strings.add(data.substring(0, data.length()));
                data = "";
            }
        }

        String IPADDRESS_PATTERN
                = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
                + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);

        for (String content : strings) {
            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                result.add(matcher.group());
            } else {
                result.add("0.0.0.0");
            }
        }
        return result;
    }
}
