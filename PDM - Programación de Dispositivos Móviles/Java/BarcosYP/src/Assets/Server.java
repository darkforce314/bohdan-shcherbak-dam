/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;

/**
 *
 * @author darkforce314
 */
public class Server extends Thread {

    private ServerSocket server;

    public static int port;
    public static String name;
    public static int maxClients;
    public static int clients;
    public static LinkedList<ServerThread> connections = new LinkedList<>();

    public Server() {
        this.port = 314;
        this.name = "SERVER-TEST";
        this.maxClients = 2;
        this.clients = 0;
    }

    public Server(int port, String name, int maxClients) {
        this.port = port;
        this.name = name;
        this.maxClients = maxClients;
        this.clients = 0;
    }

    @Override
    public void run() {
        try {
            // Creamos el socket del servidor
            server = new ServerSocket(port);
            // Establecemos un timeout de 30 segs
            server.setSoTimeout(30000);
            System.out.println("Servidor INICIADO");
            while (true) {
                // Sólo si hay espacio disponible en el servidor
                if (clients < maxClients) {
                    // Esperamos posibles conexiones
                    Socket conn = server.accept();
                    // Creamos un objeto ThreadServidor, pasándole la nueva conexión
                    ServerThread sThread = new ServerThread(clients++, conn);
                    // Iniciamos su ejecución con el método start()
                    sThread.start();
                    // Sumamos 1 a la cantidad de clientes actual
                    this.clients++;
                    // Añadimos Thread a la lista de las conexiónes actuales
                    connections.add(sThread);
                }
                // Borramos las conexiónes interrumpidas de la lista
                for (int i = 0; i < connections.size(); i++) {
                    if (connections.get(i).isInterrupted()) {
                        connections.remove(i);
                    }
                }
            }

        } catch (SocketTimeoutException e) {
            System.err.println("30 segs sin recibir nada");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                //Cerramos el socket
                server.close();
                System.out.println("Servidor PARADO");
            } catch (IOException ex) {
                System.err.println("Error al cerrar Server");
            }
        }

    }

}
