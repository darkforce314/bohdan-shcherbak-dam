/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assets;

import Objs.ClientInfo;
import java.net.Socket;

/**
 *
 * @author darkforce314
 */
public class ClientThread extends Thread{
    
    Socket conn;

    public ClientThread(Socket conn) {
        this.conn = conn;
    }
    
    
    @Override
    public void run() {
        
    }
    
    private Objs.Package decissionMaker(Objs.Package data) {
        Objs.Package result = null;
        switch (data.getType()) {
            case "ECHO":
                result = data;
                break;
            case "INFOREQUEST":
                ClientInfo info = new ClientInfo();
                result = new Objs.Package("INFORESPONSE", info);
                break;            
            case "INFORESPONSE":
                // Operaciónes con los datos de cliente
                break;
            case "DATA":
                // Operaciónes con los datos aqui
                break;
            case "ERROR":
                System.err.println("Servidor ha sufrido un error: "+(String) data.getData());
                break;
            case "CONNCLOSE": {
                try {
                    conn.close();
                    this.interrupt();
                } catch (Exception e) {
                    System.err.println("Error cerrando conexión con el cliente OUTPUT>"+e.getMessage());
                }
            }
            break;
        }
        return result;
    }
}
