/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Assets;

import Objs.ServerInfo;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author darkforce314
 */
public class ServerThread extends Thread {

    int id;
    Socket conn;
    ObjectInputStream objectInput;
    ObjectOutputStream objectOutput;

    //Constructor con el socket al que vamos a escuchar
    public ServerThread(int id, Socket conn) {
        this.id = id;
        this.conn = conn;
    }

    @Override
    public void run() {
        try {
            /*
             // Establecemos el canal de entrada
             BufferedReader sEntrada = new BufferedReader(new InputStreamReader(conn.getInputStream()));
             // Establecemos el canal de salida            
             PrintWriter sSalida = new PrintWriter(conn.getOutputStream(), true);
             // Recibimos el mensaje del cliente
             String recibido = sEntrada.readLine();
             // Enviamos el eco al cliente
             sSalida.println(recibido);
             // Cerramos los flujos
             sSalida.close();
             sEntrada.close();
             */
            if (conn.getInputStream().available() > 0) {
                objectInput = new ObjectInputStream(conn.getInputStream());
                Objs.Package input = (Objs.Package) objectInput.readObject();
                Objs.Package output = decissionMaker(input);
                if (output != null) {
                    objectOutput = new ObjectOutputStream(conn.getOutputStream());
                    objectOutput.writeObject(output);
                    System.out.println("S:" + Server.name + " THREAD ID:" + id +"// RESPONDIENDO A: "+input.getType()+" CON: "+output.getType());
                } else {
                    System.out.println("S:" + Server.name + " THREAD ID:" + id +"// RECIBIDO: "+input.getType());
                }
                objectInput.close();
                objectOutput.close();
                postProccess(input);
            }
            if (conn.isClosed()) {
                //System.err.println("Interrumpiendo el hilo...");
                this.interrupt();
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {

        }
    }

    private Objs.Package decissionMaker(Objs.Package data) {
        Objs.Package result = null;
        switch (data.getType()) {
            case "ECHO":
                result = data;
                break;
            case "INFOREQUEST":
                ServerInfo info = new ServerInfo();
                result = new Objs.Package("INFORESPONSE", info);
                break;
            case "INFORESPONSE":
                // Operaciónes con los datos de cliente
                break;
            case "DATA":
                // Operaciónes con los datos aqui
                break;
            case "ERROR":
                System.err.println("Cliente ha sufrido un error: " + (String) data.getData());
                break;
            case "CONNCLOSE": {
                try {
                    //System.err.println("Cerrando la conexión...");
                    conn.close();
                } catch (Exception e) {
                    System.err.println("Error cerrando conexión con el cliente OUTPUT>" + e.getMessage());
                }
            }
            break;
        }
        return result;
    }

    private void postProccess(Objs.Package data) {
        switch (data.getType()) {
            case "ECHO":
                decissionMaker(new Objs.Package("CONNCLOSE"));
                break;
            case "INFOREQUEST":
                decissionMaker(new Objs.Package("CONNCLOSE"));
                break;
            case "INFORESPONSE":
                // Operaciónes con los datos de cliente
                break;
            case "DATA":
                // Operaciónes con los datos aqui
                break;
            case "ERROR":
                decissionMaker(new Objs.Package("CONNCLOSE"));
                break;
        }
    }
}
