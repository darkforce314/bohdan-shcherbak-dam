/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eje0001;

/**
 *
 * @author alumno
 */
public class Eje0001 {

    static BancoUno banco = new BancoUno();

    static int cta, cantidad;
    static String texto;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        printMenu();
    }

    private static void printMenu() {
        clearCts();
        System.out.println("MENU PRINCIPAL:");
        System.out.println("1. Ingresar dinero");
        System.out.println("2. Sacar dinero");
        System.out.println("3. Ver el saldo");
        System.out.println("4. Ver el titular");
        System.out.println("5. Cambiar el Tilular");
        System.out.println("6. Imprimir datos de la Cuenta");
        System.out.println("7. Imprimir descripción del banco");
        System.out.println("---------------------------------");
        System.out.println("8. Nuevo titular");
        System.out.println("9. Eliminar un titular");
        System.out.println("\n");
        System.out.println("0. Para Salir\t X. Para realizar una operación");

        int cta, cantidad;
        String texto;
        int opc = Entrada.entero();
        switch (opc) {
            case 0:
                System.out.println("> Cerrando la aplicación");
                clearCts();
                System.exit(1);
                break;
            case 1:
                clearCts();
                System.out.println("MENU de ingresos:");
                cta = printSelectCta();
                clearCts();
                System.out.println("MENU de ingresos:");
                System.out.println("En este momento hay " + banco.getSaldo(cta) + "€");
                System.out.println("\n");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>");
                cantidad = printSelectCantidad();
                clearCts();
                if (banco.ingresar(cta, cantidad)) {
                    System.out.println();
                    printResult("Ingreso completado exitosamente!");
                } else {
                    printResult("Hubo un error al realizar el ingreso!");
                }
                break;
            case 2:
                clearCts();
                System.out.println("MENU de retirada:");
                cta = printSelectCta();
                clearCts();
                System.out.println("MENU de retirada:");
                System.out.println("En este momento hay " + banco.getSaldo(cta) + "€");
                System.out.println("\n");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>");
                cantidad = printSelectCantidad();
                clearCts();
                if (banco.sacar(cta, cantidad)) {
                    System.out.println();
                    printResult("Retirada completada exitosamente!");
                } else {
                    printResult("Hubo un error al realizar la retirada!");
                }
                break;
            case 3:
                clearCts();
                System.out.println("Ver el Saldo");
                cta = printSelectCta();
                clearCts();
                System.out.println("Ver el Saldo");
                printResult("Cuenta contiene :" + banco.getSaldo(cta));
                break;
            case 4:
                clearCts();
                System.out.println("Ver el Titular");
                cta = printSelectCta();
                clearCts();
                System.out.println("Ver el Titular");
                printResult("Cuenta contiene :" + banco.getTitular(cta));
                break;
            case 5:
                clearCts();
                System.out.println("Cambiar el Titular");
                cta = printSelectCta();
                clearCts();
                System.out.println("Cambiar el Titular");
                System.out.println("En este momento El Titular es " + banco.getTitular(cta));
                System.out.println("\n");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>");
                System.out.println();
                texto = printSelectTexto();
                banco.setTitular(cta, texto);
                clearCts();
                printResult("El titular ha cambiado!");
                break;
            case 6:
                clearCts();
                System.out.println("Datos de la cuenta");
                cta = printSelectCta();
                clearCts();
                System.out.println("Datos de la cuenta");
                printResult(banco.ToString(cta));
                break;
            case 7:
                clearCts();
                printResult(banco.descripcionCompleta());
                break;
            case 8:
                clearCts();
                System.out.println("Elige el nombre del titular");
                texto = printSelectTexto();
                clearCts();
                System.out.println("Saldo inicial:");
                System.out.println(">Pulsa INTRO para saltar este paso<");
                cantidad = printSelectCantidad();
                if (cantidad > 0) {
                    banco.addTitular(texto, cantidad);
                } else {
                    banco.addTitular(texto);
                }
                clearVars();
                clearCts();
                printResult(banco.printList());
                break;
            case 9:
                clearCts();
                System.out.println("Datos de la cuenta");
                cta = printSelectCta();
                clearCts();
                System.out.println("Datos de la cuenta");
                printResult(banco.ToString(cta));
                break;
            default:
                printMenu();
                break;
        }
    }

    private static int printSelectCta() {
        int res = 0;
        do {
            clearCts();
            System.out.println("Selecióne una Cuenta:");
            System.out.println("1." + banco.cta1.getTitular());
            System.out.println("2." + banco.cta2.getTitular());
            System.out.println("\n");
            System.out.println(">>>>>>>>>>>>>>>>>>>>>");
            res = Entrada.entero();
        } while (res > 3 && 0 > res);
        return res;
    }

    private static int printSelectCantidad() {
        System.out.println("Selecióne una cantidad: \n");
        return Entrada.entero();
    }

    private static String printSelectTexto() {
        System.out.println("Inserte texto: \n");
        return Entrada.cadena();
    }

    private static void printResult(String res) {
        System.out.println(res);
        System.out.println("\n");
        System.out.println(">>>>>>>>>>>>>>>>>>>>>");
        System.out.println("Pulse INTRO para volver:");
        Entrada.cadena();
        printMenu();
    }
    
    private static void printDeleteTitular(){
        
    }
    
    private static void clearVars(){
        cantidad = 0;
        texto = "";
    }
    
    private static void clearCts() {
        System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }
}
