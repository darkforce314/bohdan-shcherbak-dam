/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eje0001;

import java.io.Serializable;

/**
 *
 * @author toni
 */
public class CuentaUno implements Serializable{

    private String titular;
    private int saldo;

    CuentaUno(String titular) {
        this.titular = titular;
        this.saldo = 0;
    }

    CuentaUno(String titular, int saldo) {
        this.titular = titular;
        this.saldo = saldo;
    }

    //Getters
    public String getTitular() {
        return titular;
    }

    public int getSaldo() {
        return saldo;
    }

    //Setters
    public void setTitular(String titular) {
        this.titular = titular;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    //Metodos de la clase
    
    public void ingresar(int masSaldo) {
        this.saldo = this.saldo + masSaldo;
    }
    
    public boolean sacar(int dineroSacado){
        boolean sacado=false;
        int res =  this.saldo-dineroSacado;
        if(dineroSacado>0 && res>=0){
            sacado=true;
            this.saldo = res;
        }        
        return sacado;
    }
    
    public String descripcionCompleta(){
        return "Titular: "+this.titular+"\n"+
                "Saldo: "+this.saldo;
    }
    
    public String ToString(){
        return this.titular+"*"+this.saldo;
    }
}
