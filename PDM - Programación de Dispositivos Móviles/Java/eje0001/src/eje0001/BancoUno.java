/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eje0001;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

/**
 *
 * @author alumno
 */
public class BancoUno {

    static CuentaUno cta1, cta2;

    LinkedList<CuentaUno> db = new LinkedList<CuentaUno>();

    public BancoUno() {
        this.cta1 = new CuentaUno("Bohdan");
        this.cta2 = new CuentaUno("Toni", 30);
    }

    private CuentaUno getCta(int cuenta) {
        if (cuenta == 1) {
            return this.cta1;
        } else {
            return this.cta2;
        }
    }

    public boolean ingresar(int cuenta, int cantidad) {
        boolean intent = false;
        if (cuenta < 3 && cuenta > 0) {
            getCta(cuenta).ingresar(cantidad);
            intent = true;
        }
        return intent;
    }

    public boolean sacar(int cuenta, int cantidad) {
        boolean intent = false;
        if (cuenta < 3 && cuenta > 0) {
            getCta(cuenta).sacar(cantidad);
            intent = true;
        }
        return intent;
    }

    public int getSaldo(int cuenta) {
        return getCta(cuenta).getSaldo();
    }

    public int getTitular(int cuenta) {
        return getCta(cuenta).getSaldo();
    }

    public void setTitular(int cuenta, String titular) {
        getCta(cuenta).setTitular(titular);
    }

    public String ToString(int cuenta) {
        return getCta(cuenta).ToString();
    }

    public String descripcionCompleta() {
        return "Cuenta 1: " + this.cta1.getTitular() + " -> " + this.cta1.getSaldo() + "\n"
                + "Cuenta 2: " + this.cta2.getTitular() + " -> " + this.cta2.getSaldo();
    }

    public boolean addTitular(String nombre) {
        boolean res = false;
        this.db.add(new CuentaUno(nombre, 0));
        writeToFile(this.db);
        return res;
    }

    public boolean addTitular(String nombre, int cantidad) {
        boolean res = false;
        this.db.add(new CuentaUno(nombre, cantidad));
        writeToFile(this.db);
        return res;
    }

    public String printList() {
        String res = "";
        int aux = 1;
        for (CuentaUno cta : this.db) {
            res+=aux+". "+cta.getTitular()+" -> "+cta.getSaldo()+"\n";
            aux++;
        }   
        return res;
    }

    public boolean removeTitular(int num) {
        boolean res = false;
        int count = 0;
        for (CuentaUno cta : this.db) {
            if (count == num) {
                try {
                    this.db.remove(cta);
                    res = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            count++;
        }
        return res;
    }

    public boolean pullFile() {
        try {
            this.db = readFromFile();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void writeToFile(LinkedList<CuentaUno> db) {
        try {
            FileOutputStream fos = new FileOutputStream("db.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(db);
            fos.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static LinkedList<CuentaUno> readFromFile() {
        LinkedList<CuentaUno> res = new LinkedList<CuentaUno>();
        try {
            FileInputStream fis = new FileInputStream("db.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = ois.readObject();
            res = (LinkedList<CuentaUno>) obj;
        } catch (Exception e) {
            System.out.println(e);
        }
        return res;
    }
}
