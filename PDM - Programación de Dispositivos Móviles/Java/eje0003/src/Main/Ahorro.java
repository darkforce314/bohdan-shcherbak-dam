/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Main;

/**
 *
 * @author darkforce314
 */
public class Ahorro extends Cuenta{
    float interes;
    
    Ahorro(){
        super();
    }
    Ahorro(String titular, float saldo, int numero, float interes){
        super(titular,saldo,numero);
        this.interes = interes;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(float interes) {
        this.interes = interes;
    }

    @Override
    public String toString() {
        return super.numero+" ["+super.titular+"] "
                +" ["+super.fechaApertura+"] "+" ["+super.saldo+"] "
                +" ["+super.saldo+"] "+" [Ahorro] "+" ["+this.interes+"] ";
    }

    @Override
    public void descripcionCompleta() {
        System.out.println(super.numero+" ["+super.titular+"] "
                +" ["+super.fechaApertura+"] "+" ["+super.saldo+"] "
                +" ["+super.saldo+"] "+" [Ahorro] "+" ["+this.interes+"] ");
    }

    @Override
    public float getDato() {
        return this.interes;
    }
    
    
}
