package Main;

import java.util.ArrayList;

/**
 *
 * @author darkforce314
 */
public class ConsoleMenu {
    ArrayList db;
    
    ConsoleMenu(){
        
    }
    
    class Node {
        String name,content,output;
        int inputType;
        
        public Node(){
            this.name = "MENU EXAMPLE";
            this.content = "Content example";
            this.inputType = 0;
        }
        
        public Node(String name, String content) {
            this.name = name;
            this.content = content;
            this.inputType = 0;
        }
        
        public Node(String name, String content, int inputType) {
            this.name = name;
            this.content = content;
            this.inputType = inputType;
        }

        public Node(String name, String content, String output, int inputType) {
            this.name = name;
            this.content = content;
            this.output = output;
            this.inputType = inputType;
        }       
        
    }
}
