/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.util.Random;

/**
 *
 * @author darkforce314
 *
 * • Crear nueva cuenta (corriente o ahorro). • Mostrar todos los datos de una
 * cuenta buscada por su número (usando descripcionCompleta). • Listar todo.
 * Mostrará: número, titular, saldo, fecha de apertura, tipo de cuenta y la
 * comisión y la periodicidad (si es Cta. corriente) o el interés (si es Cta.
 * ahorro) de todas las cuentas. Se debe usar toString y getTipo. • Ingresar
 * dinero en una cuenta por su número. Mostrando el antiguo y el nuevo saldo. •
 * Cambiar la comisión de una Cta corriente (por su número) mostrando la antigua
 * y nueva comisión (con gets, sets e instanceof). • Mostrar el Titular, saldo y
 * tipo de interés de las cuentas que tengan el tipo de interés en entre dos
 * dados (con gets e instanceof).. • Salir del programa.
 */
public class Banco {

    private static final int charsPerColumn = 100;

    static Lista db = new Lista();
    static int number, intAux;
    static float data;
    static String titular, stringAux;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Initial input of data   
        Corriente a = new Corriente("Bohdan", 1224, 1143, 1243.1F, "mensual");
        Corriente c = new Corriente("Toni", 300, 1144, 1243.1F, "anual");
        Ahorro b = new Ahorro("Toni", 1224, 1445, 1243.1F);
        db.add(a);
        db.add(c);
        db.add(b);
        Object val = search(1143);
        System.out.println(val instanceof Corriente);
        //Initial input of data
        printMainMenu();
    }

    public static void printMainMenu() {
        int opc;
        Object aux;
        System.out.println("MENU PRINCIPAL");
        System.out.println("****************************************************");
        System.out.println("1. Crear una nueva cuenta");
        System.out.println("2. Supervisar una cuenta");
        System.out.println("3. Ver una cuenta completa");
        System.out.println("4. Ingresar dinero en una cuenta");
        System.out.println("5. Cambiar la comisión (Cuntas corrientes)");
        System.out.println("6. Comparador de cuentas");
        System.out.println("****************************************************");
        System.out.println("7. Borrar una cuenta");
        System.out.println("8. Cambiar tipo de cuenta");
        System.out.println("****************************************************");
        System.out.println("0. Salir");
        System.out.println("****************************************************");
        System.out.println("Introduzca un numero:\n");
        opc = Entrada.entero();
        switch (opc) {
            case 0:
                System.exit(0);
                break;
            case 1:
                boolean type = printTypeChice();
                Banco.number = printNewNumberChoice();
                Banco.titular = printTitularTrace();
                Banco.data = printDataTrace();
                float interes,
                 comision;
                String periodicidad;
                if (type) {
                    clearCts();
                    System.out.println("Introduzca la comisión");
                    System.out.println("****************************************************");
                    System.out.println("Introduzca un numero y pulsa ENTER:\n");
                    comision = (float) Entrada.real();
                    do {
                        clearCts();
                        System.out.println("Introduzca la periodicidad");
                        System.out.println("****************************************************");
                        System.out.println("Introduzca M/A para elegir ente\nMensual y Anual:\n");
                        periodicidad = Entrada.cadena();
                    } while (!periodicidad.contains("M") && !periodicidad.contains("A"));
                    if (periodicidad.contains("M")) {
                        periodicidad = "mensual";
                    } else {
                        periodicidad = "anual";
                    }
                    db.add(new Corriente(titular, data, number, comision, periodicidad));
                } else {
                    clearCts();
                    System.out.println("Introduzca la comisión");
                    System.out.println("****************************************************");
                    System.out.println("Introduzca un numero y pulsa ENTER:\n");
                    interes = (float) Entrada.real();
                    db.add(new Ahorro(titular, data, number, interes));
                }
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 2:
                clearCts();
                Banco.number = printNumberChoice();
                aux = search(number);

                if (aux instanceof Ahorro) {
                    Ahorro aux1 = (Ahorro) aux;
                    printResult(aux1.toString());
                } else if (aux instanceof Corriente) {
                    Corriente aux1 = (Corriente) aux;
                    printResult(aux1.toString());
                } else {
                    System.out.println("ERROR AL PROCESAR: " + aux.getClass().toString());
                }
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 3:
                clearCts();
                Banco.number = printNumberChoice();
                aux = search(number);

                if (aux instanceof Ahorro) {
                    Ahorro aux1 = (Ahorro) aux;
                    printResult(aux1.toString());
                } else if (aux instanceof Corriente) {
                    Corriente aux1 = (Corriente) aux;
                    printResult(aux1.toString());
                } else {
                    System.out.println(aux.getClass().toString());
                }
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 4:
                clearCts();
                number = printNumberChoice();
                data = printDataTrace();
                aux = search(number);
                if (aux instanceof Ahorro) {
                    Ahorro aux1 = (Ahorro) aux;
                    aux1.setSaldo(data);
                } else if (aux instanceof Corriente) {
                    Corriente aux1 = (Corriente) aux;
                    aux1.setSaldo(data);
                } else {
                    System.out.println("ERROR AL PROCESAR: " + aux.getClass().toString());
                }
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 5:
                do {
                    number = printNumberChoice();
                    aux = search(number);
                } while (aux instanceof Corriente);
                data = printDataTrace();
                Corriente aux1 = (Corriente) aux;
                aux1.comision = data;
                printResult("Operación realizada con éxito!");
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 6:
                clearCts();
                Cuenta cuenta1,
                 cuenta2;
                cuenta1 = (Cuenta) search(printNumberChoice());
                cuenta2 = (Cuenta) search(printNumberChoice());
                printResult(cuenta1.titular + " - " + cuenta2.titular + "\n"
                        + cuenta1.numero + " - " + cuenta2.numero + "\n"
                        + cuenta1.saldo + " - " + cuenta2.saldo + "\n"
                        + cuenta1.getFechaAperturaString() + " - " + cuenta2.getFechaAperturaString() + "\n");
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 7:
                clearCts();
                number = printNumberChoice();
                Cuenta auxDel = (Cuenta) search(number);
                Cuenta data = null;
                for (int i = 1; i < db.size(); i++) {
                    data = (Cuenta) db.get(i);
                    if (data.numero == number) {
                        db.remove(i);
                    }
                }
                printResult("SE HA ELIMINADO la cuenta Numero: " + data.numero + " Titular: " + data.titular);
                clearCts();
                clearData();
                printMainMenu();
                break;
            case 8:
                clearCts();
                number = printNumberChoice();
                aux = search(number);
                Cuenta data1 = null;
                for (int i = 1; i < db.size(); i++) {
                    data1 = (Cuenta) db.get(i);
                    if (data1.numero == number) {
                        db.remove(i);
                    }
                }
                if (aux instanceof Ahorro) {
                    Ahorro auxAh = (Ahorro) aux;
                    comision = printDataTrace();
                    do {
                        clearCts();
                        System.out.println("Introduzca la periodicidad");
                        System.out.println("****************************************************");
                        System.out.println("Introduzca M/A para elegir ente\nMensual y Anual:\n");
                        periodicidad = Entrada.cadena();
                    } while (!periodicidad.contains("M") && !periodicidad.contains("A"));
                    if (periodicidad.contains("M")) {
                        periodicidad = "mensual";
                    } else {
                        periodicidad = "anual";
                    }
                    Corriente res = new Corriente(auxAh.titular,auxAh.saldo,auxAh.numero,comision,periodicidad);
                    db.add(res);
                } else if (aux instanceof Corriente) {
                    Corriente auxCr = (Corriente) aux;
                    interes = printDataTrace();                    
                    Ahorro res = new Ahorro(auxCr.titular,auxCr.saldo,auxCr.numero,interes);
                    db.add(res);
                } else {
                    System.out.println("ERROR AL PROCESAR: " + aux.getClass().toString());
                }
                clearCts();
                clearData();
                printMainMenu();
                break;

            default:
                clearCts();
                clearData();
                printMainMenu();
                break;
        }
    }

    private static boolean printTypeChice() {
        clearCts();
        System.out.println("Elija un tipo de cuenta");
        System.out.println("****************************************************");
        System.out.println("Introduzca Y/N para elegir entre Corriente/Ahorro:\n");
        String aux = Entrada.cadena();
        if (aux.contains("Y") || aux.contains("y")) {
            return true;
        } else {
            return false;
        }
    }

    private static int printNumberChoice() {
        int aux;
        boolean valid = true;
        do {
            clearCts();
            if (!valid) {
                System.out.println("Elija un numero de cuenta válido");
            } else {
                System.out.println("Elija un numero de cuenta");
            }
            System.out.println("****************************************************");
            System.out.println("Escriba un numero de cuenta,\n o pulse INTRO para generar uno aleatorio:\n");
            aux = Entrada.entero();
            if (aux <= 0) {
                aux = getRandomNumber();
            }
        } while (!exists(aux));

        return aux;
    }

    private static int printNewNumberChoice() {
        int aux;
        boolean valid = true;
        do {
            clearCts();
            if (!valid) {
                System.out.println("Elija un numero de cuenta válido");
            } else {
                System.out.println("Elija un numero de cuenta");
            }
            System.out.println("****************************************************");
            System.out.println("Escriba un numero de cuenta,\n o escriba '0' para generar uno aleatorio:\n");
            aux = Entrada.entero();
            if (aux <= 0) {
                aux = getRandomNumber();
            }
        } while (exists(aux));

        return aux;
    }

    private static String printTitularTrace() {
        clearCts();
        System.out.println("Escriba el nombre del Titular");
        System.out.println("****************************************************");
        System.out.println("Escriba el texto y pulse ENTER:\n");
        return Entrada.cadena();
    }

    private static float printDataTrace() {
        clearCts();
        System.out.println("Introduzca un numero");
        System.out.println("****************************************************");
        System.out.println("Escriba un numero y pulse ENTER:\n");
        return (float) Entrada.real();
    }

    private static void printResult(String data) {
        clearCts();
        System.out.println(data);
        System.out.println("****************************************************");
        String aux = Entrada.cadena();
    }

    private static int getRandomNumber() {
        Random random = new Random();
        int randomNumber;
        do {
            randomNumber = random.nextInt(9999 - 1) + 1;
        } while (exists(randomNumber));
        return randomNumber;
    }

    private static boolean exists(int numero) {
        boolean res = false;

        for (int i = 1; i < db.size(); i++) {
            Cuenta aux = (Cuenta) db.get(i);
            if (aux.numero == numero) {
                res = true;
            }
        }
        return res;
    }

    private static Object search(int numero) {
        Object res = null;

        for (int i = 1; i < db.size(); i++) {
            Object aux = db.get(i);
            Cuenta subAux = (Cuenta) aux;
            if (subAux.numero == numero) {
                res = aux;
            }
        }
        return res;
    }

    private static void clearCts() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    private static void clearData() {
        Banco.number = 0;
        Banco.intAux = 0;
        Banco.titular = "";
        Banco.stringAux = "";
    }
}
