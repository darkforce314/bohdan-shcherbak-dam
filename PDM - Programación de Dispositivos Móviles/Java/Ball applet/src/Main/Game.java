package Main;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;

/**
 *
 * @author darkforce314
 */
public class Game extends Applet implements Runnable, MouseListener {

    private Image i;
    private Graphics doubleG;
    LinkedList<Ball> balls = new LinkedList<Ball>();
    Ball b;
    Ball b1;

    public void init() {
        setSize(800, 600);
        addMouseListener(this);
    }

    public void start() {
        Thread fps = new Thread(this);
        fps.start();
    }

    public void stop() {

    }

    public void destroy() {

    }

    @Override
    public void update(Graphics g) {
        //Double buffering prevents flickering beetwen frames
        if (i == null) {
            i = createImage(this.getSize().width, this.getSize().height);
            doubleG = i.getGraphics();
        }

        doubleG.setColor(getBackground());
        doubleG.fillRect(0, 0, this.getSize().width, this.getSize().height);

        doubleG.setColor(getForeground());
        paint(doubleG);

        g.drawImage(i, 0, 0, this);
    }

    public void paint(Graphics g) {
        for (int j = 0; j < balls.size(); j++) {
            balls.get(j).paint(g);
        }
    }

    //FPS thread definition
    @Override
    public void run() {
        while (true) {
            for (int j = 0; j < balls.size(); j++) {
                balls.get(j).update(this);
            }
            //Repaint after move
            repaint();
            try {
                //17ms for 30 fps
                Thread.sleep(17);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        PointerInfo a = MouseInfo.getPointerInfo();
        Point b = a.getLocation();
        Double x = b.getX();
        Double y = b.getY();
        System.out.println(x + " - " + y);
        Ball bAux = new Ball(x.intValue()-40, y.intValue()-40, Color.RED);
        this.balls.add(bAux);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("Entra");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("Sale");
    }
}
