/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Ball {

    private int x = 400;
    private int y = 25;
    private double dx = 0;
    private double dy = 0;
    private int radius = 20;
    private double gravity = 15;
    private double energyloss = .65;
    private double dt = .2;
    private Color color = Color.GREEN;
    Random rand = new Random();

    public Ball() {
    }

    public Ball(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        this.color = new Color(r, g, b);
        dx = rand.nextInt(100) - 50;
        dy = rand.nextInt(100) - 50;
    }

    public void update(Game gm) {
        if (x + dx >= gm.getWidth() - radius - 1) {
            x = gm.getWidth() - radius - 1;
            dx = -dx;
        } else if (x + dx <= 0 + radius) {
            x = 0 + radius;
            dx = -dx;
        } else {
            x += dx;
        }
        if (y == gm.getHeight() - radius - 1) {
            dx *= energyloss;
            if (Math.abs(dx) < .8) {
                dx = 0;
                dy = 0;
            }
        }
        if (y > gm.getHeight() - radius - 1) {
            y = gm.getHeight() - radius - 1;
            dy *= energyloss;
            dy = -dy;
        } else {
            //velocity formula
            dy += gravity * dt;
            //position formula
            y += dy * dt + .5 * gravity * dt * dt;
        }
    }

    public void paint(Graphics g) {
        g.setColor(this.color);
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }
}
