package ConInterfaz;
import java.io.*;
/////////////////////////////////////////////////////////////////
// Aplicaci�n para trabajar con la clase CBanco y la jerarqu�a
// de clases derivadas de CCuenta
//

public class Test
{
  // Para la entrada de datos se utiliza la clase Leer
  public static CCuenta leerDatos(int op)
  {
    CCuenta obj = null;
    String nombre, cuenta;
    double saldo, tipoi, mant;
    System.out.println("Nombre.................: ");
    nombre = Leer.dato();
    System.out.println("Cuenta.................: ");
    cuenta = Leer.dato();
    System.out.println("Saldo..................: ");
    saldo = Leer.datoDouble();
    System.out.println("Tipo de inter�s........: ");
    tipoi = Leer.datoDouble();
    if (op == 1)
    {
      System.out.println("Mantenimiento..........: ");
      mant = Leer.datoDouble();
      obj = new CCuentaAhorro(nombre, cuenta, saldo, tipoi, mant);
    }
    else
    {
      int transex;
      double imptrans;
      System.out.println("Importe por transacci�n: ");
      imptrans = Leer.datoDouble();
      System.out.println("Transacciones exentas..: ");
      transex = Leer.datoInt();
      if (op == 2)
        obj = new CCuentaCorriente(nombre, cuenta, saldo, tipoi,
                                   imptrans, transex);
      else
        obj = new CCuentaCorrienteConIn(nombre, cuenta, saldo,
                                        tipoi, imptrans, transex);
    }
    return obj;
  }
  
  public static int men�()
  {
    System.out.print("\n\n");
    System.out.println("1. INSERTAR una Cuenta nueva: ");
    System.out.println("2. Mostrar una Cuenta (Nombre, cuenta y saldo) ");
    System.out.println("3. Buscar siguiente Cta (del mismo nombre o cuenta de la opci�n 2)");
    System.out.println("4. Ingresar: ");
    System.out.println("5. Reintegro: ");    
    System.out.println("6. Eliminar una Cuenta");
    System.out.println("7. Mantenimiento (a 1� de mes se calculan intereses, comisiones...)");
    System.out.println("0. Salir");    
    System.out.println();
    System.out.println("   Opci�n: ");
    int op;
    do
      op = Leer.datoInt();
    while (op < 0 || op > 7);
    return op;
  }
  
  public static void main(String[] args)
  {
    // Definir una referencia al flujo est�ndar de salida: flujoS
    // Para entenderlo piensa que miembro out de la clase System en un objeto PrintStream
    PrintStream flujoS = System.out;
    
    // Crear un objeto banco vac�o (con cero elementos)
    CBanco banco = new CBanco();

    int opci�n = 0, pos = -1;
    String cadenabuscar = null;
    String nombre, cuenta;
    double cantidad;
    boolean eliminado = false;

    do
    {
      opci�n = men�();
      switch (opci�n)
      {
        case 0: // salir
          banco = null;
          break;
        case 1: // a�adir
          flujoS.println("Tipo de cuenta: 1-(CA), 2-(CC), 3-CCI  ");
          do
            opci�n = Leer.datoInt();
          while (opci�n < 1 || opci�n > 3);
          banco.a�adir(leerDatos(opci�n));
          break;
        case 2: // saldo
          flujoS.println("Nombre o cuenta a buscar (el texto puede ser total o parcial): ");
          cadenabuscar = Leer.dato();
          pos = banco.buscar(cadenabuscar, 0);
          if (pos == -1)
            if (banco.longitud() != 0)
              flujoS.println("b�squeda fallida");
            else
              flujoS.println("no hay clientes");
          else
          {
            flujoS.println(banco.clienteEn(pos).obtenerNombre());
            flujoS.println(banco.clienteEn(pos).obtenerCuenta());
            flujoS.println(banco.clienteEn(pos).estado());
          }
          break;
        case 3: // buscar siguiente
          pos = banco.buscar(cadenabuscar, pos + 1);
          if (pos == -1)
            if (banco.longitud() != 0)
              flujoS.println("b�squeda fallida");
            else
              flujoS.println("no hay clientes");
          else
          {
            flujoS.println(banco.clienteEn(pos).obtenerNombre());
            flujoS.println(banco.clienteEn(pos).obtenerCuenta());
            flujoS.println(banco.clienteEn(pos).estado());
          }
          break;
       case 4: // ingreso.  Los casos 4 y 5 comparten c�digo (no hay break)
       case 5: // reintegro
          flujoS.println("Cuenta: "); cuenta = Leer.dato();
          pos = banco.buscar(cuenta, 0);
          if (pos == -1)
            if (banco.longitud() != 0)
              flujoS.println("b�squeda fallida");
            else
              flujoS.println("no hay clientes");
          else
          {
            flujoS.println("Cantidad: "); cantidad = Leer.datoDouble();
            if (opci�n == 3)
              banco.clienteEn(pos).ingreso(cantidad);
            else
              banco.clienteEn(pos).reintegro(cantidad);
          }
          break;
       
        case 6: // eliminar
          flujoS.println("Cuenta: "); cuenta = Leer.dato();
          eliminado = banco.eliminar(cuenta);
          if (eliminado)
            flujoS.println("registro eliminado");
          else
            if (banco.longitud() != 0)
              flujoS.println("cuenta no encontrada");
            else
              flujoS.println("no hay clientes");
          break;
        case 7: // mantenimiento
          for (pos = 0; pos < banco.longitud(); pos++)
          {
            banco.clienteEn(pos).comisiones();
            banco.clienteEn(pos).intereses();
          }
          break;      
      }
    }
    while(opci�n != 0);
  }
}
/////////////////////////////////////////////////////////////////
