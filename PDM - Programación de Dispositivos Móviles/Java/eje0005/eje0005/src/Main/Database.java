/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.io.*;
import javax.swing.*;
import java.util.*;

/**
 *
 * @author darkforce314
 */
public class Database {

    static List<Object> db;
    public String defaultFile;

    public Database() {
        this.db = new LinkedList();
        this.defaultFile = "db.dat";
    }

    public Database(String file) {
        this.db = new LinkedList();
        this.defaultFile = file;
    }

    public LinkedList<Object> getCorrienteList() {
        Collections.sort(db, null);
        LinkedList<Object> res = new LinkedList();
        Iterator it = db.iterator();
        while (it.hasNext()) {
            Object aux = it.next();
            if (aux instanceof Corriente) {
                res.add(aux);
            }
        }
        return res;
    }

    public LinkedList<Object> getAhorroList() {
        LinkedList<Object> res = new LinkedList();
        Iterator it = db.iterator();
        while (it.hasNext()) {
            Object aux = it.next();
            if (aux instanceof Ahorro) {
                res.add(aux);
            }
        }
        return res;
    }

    public boolean createCorrinte(String titular, String saldo, String numero, String comision, String periodicidad) {
        try {
            Corriente data = new Corriente(titular, Float.valueOf(saldo), Integer.valueOf(numero), Float.valueOf(comision), periodicidad);
            db.add(data);
            /*
             Iterator it = db.iterator();
             while (it.hasNext()) {
             System.out.println(it.next().toString());
             }
             */
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean createAhorro(String titular, String saldo, String numero, String interes) {
        try {
            Ahorro data = new Ahorro(titular, Float.valueOf(saldo), Integer.valueOf(numero), Float.valueOf(interes));
            db.add(data);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public LinkedList<Object> multiSearchCuenta(String value, String type) {
        LinkedList<Object> result = new LinkedList<Object>();
        Object obj = null;
        Cuenta cuentaAux = null;
        Corriente cAux = null;
        Ahorro aAux = null;
        Iterator it = db.iterator();
        switch (type) {
            case ("numero"):
                while (it.hasNext()) {
                    obj = it.next();
                    cuentaAux = (Cuenta) obj;
                    if (cuentaAux.numero == Integer.valueOf(value)) {
                        result.add(obj);
                    }
                }
                break;
            case ("titular"):
                while (it.hasNext()) {
                    obj = it.next();
                    cuentaAux = (Cuenta) obj;
                    if (cuentaAux.getTitular().contains(value)) {
                        result.add(obj);
                    }
                }
                break;
            case ("saldo"):
                while (it.hasNext()) {
                    obj = it.next();
                    cuentaAux = (Cuenta) obj;
                    if (cuentaAux.saldo == Float.valueOf(value)) {
                        result.add(obj);
                    }
                }
                break;
            case ("comision"):
                while (it.hasNext()) {
                    obj = it.next();
                    if (obj instanceof Corriente) {
                        cAux = (Corriente) obj;
                        if (cAux.getComision() == Float.valueOf(value)) {
                            result.add(obj);
                        }
                    }
                }
                break;
            case ("interes"):
                while (it.hasNext()) {
                    obj = it.next();
                    if (obj instanceof Ahorro) {
                        aAux = (Ahorro) obj;
                        if (aAux.getInteres() == Float.valueOf(value)) {
                            result.add(obj);
                        }
                    }
                }
                break;
        }
        return result;
    }

    public Object searchCuenta(int number) {
        Object obj = null;
        Iterator it = db.iterator();
        while (it.hasNext()) {
            obj = it.next();
            Cuenta aux = (Cuenta) obj;
            if (aux.numero == number) {
                return obj;
            }
        }
        return null;
    }

    public boolean remove(int number) {
        Iterator it = db.iterator();
        while (it.hasNext()) {
            Cuenta aux = (Cuenta) it.next();
            if (aux.numero == number) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    public boolean exists(int number) {
        Iterator it = db.iterator();
        while (it.hasNext()) {
            Cuenta aux = (Cuenta) it.next();
            if (aux.numero == number) {
                return true;
            }
        }
        return false;
    }

    public void save() {
        save(this.defaultFile);
    }

    public void save(File f) {
        save(f.getAbsolutePath());
    }

    public static void save(String file) {
        ObjectOutputStream oos = null;
        try {
            FileOutputStream fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(db);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error writing Database to file \"" + file + "\"");
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Error closing file \"" + file + "\"");
                }
            }
        }
    }

    public void recover() {
        recover(this.defaultFile);
    }
    
    public void recover(File f) {
        recover(f.getAbsolutePath());
    }

    public void recover(String file) {
        File f = new File(file);
        if (f.exists()) {
            ObjectInputStream oos = null;
            try {
                FileInputStream streamIn = new FileInputStream(file);
                oos = new ObjectInputStream(streamIn);
                db = (LinkedList<Object>) oos.readObject();
            } catch (Exception e) {
                e.printStackTrace();
                //JOptionPane.showMessageDialog(null, "Error reading Database to file \"" + file + "\"");
            } finally {
                try {
                    oos.close();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Error closing file \"" + file + "\"");
                }
            }
        }
    }

    @Override
    public String toString() {
        String res = db.size() + "\n";
        Iterator it = db.iterator();
        while (it.hasNext()) {
            res += it.next().toString() + "\n";
        }
        return res;
    }
}
