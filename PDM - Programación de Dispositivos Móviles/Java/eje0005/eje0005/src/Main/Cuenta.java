/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Main;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author darkforce314
 */
public abstract class Cuenta implements java.io.Serializable,Comparable{
    public int numero;
    public String titular;
    public float saldo;
    public GregorianCalendar fechaApertura;
    static int totalArticulos;
    
    Cuenta(){
        
    }
    
    Cuenta(String titular, float saldo, int numero){
        this.titular = titular;
        this.saldo = saldo;
        this.numero = numero;
        this.fechaApertura = new GregorianCalendar();
    }
    
    public int getNumero() {
        return numero;
    }

    public String getTitular() {
        return titular;
    }

    public float getSaldo() {
        return saldo;
    }

    public GregorianCalendar getFechaApertura() {
        return fechaApertura;
    }
    public String getFechaAperturaString(){        
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy hh:ss");
        fmt.setCalendar(this.fechaApertura);
        return fmt.format(this.fechaApertura.getTime());
    }

    public static int getTotalArticulos() {
        return totalArticulos;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    /*
    public void setNumero(){
        this.numero = (int) (Math.random()*100000);
    }
    */
    public void setTitular(String titular) {
        this.titular = titular;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void setFechaApertura(GregorianCalendar fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public static void setTotalArticulos(int totalArticulos) {
        Cuenta.totalArticulos = totalArticulos;
    }  
    public int compareTo(Cuenta o){
        return Integer.compare(this.numero, o.numero);
    }
    @Override
    public abstract String toString();
    public abstract void descripcionCompleta();
    public abstract float getDato();
}
