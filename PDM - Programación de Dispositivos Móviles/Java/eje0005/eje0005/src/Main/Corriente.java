/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.text.SimpleDateFormat;

/**
 *
 * @author darkforce314
 */
public class Corriente extends Cuenta implements java.io.Serializable,Comparable{

    public float comision;
    public String periodicidad;

    Corriente() {
        super();
    }

    Corriente(String titular, float saldo, int numero, float comision, String periodicidad) {
        super(titular, saldo, numero);
        this.comision = comision;
        this.periodicidad = periodicidad;
    }

    public void setComision(float comision) {
        this.comision = comision;
    }

    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    public float getComision() {
        return comision;
    }

    public String getPeriodicidad() {
        return periodicidad;
    }

    @Override
    public String toString() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy hh:ss");
        fmt.setCalendar(this.fechaApertura);
        String dateFormatted = fmt.format(this.fechaApertura.getTime());
        return super.numero + " [" + super.titular + "] "
                + " [" + dateFormatted + "] " + " [" + super.saldo + "] "
                + " [" + super.saldo + "] " + " [Corriente] " + " [" + this.comision + "] "
                + " [" + this.periodicidad + "] ";
    }

    @Override
    public void descripcionCompleta() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy hh:ss");
        fmt.setCalendar(this.fechaApertura);
        String dateFormatted = fmt.format(this.fechaApertura.getTime());
        System.out.println(super.numero + " [" + super.titular + "] "
                + " [" + dateFormatted + "] " + " [" + super.saldo + "] "
                + " [" + super.saldo + "] " + " [Corriente] " + " [" + this.comision + "] "
                + " [" + this.periodicidad + "] ");
    }

    @Override
    public float getDato() {
        return this.comision;
    }
    
    @Override
    public int compareTo(Object o) {
        Cuenta aux = (Cuenta) o;
        return Integer.compare(this.numero, aux.numero);
    }
    
    public Object[] toArray(){
        return new Object[]{numero,titular,getFechaAperturaString(),saldo,comision,periodicidad};
    }

}
