package Main;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author darkforce314
 */
public class Ui extends javax.swing.JFrame {

    //Config variables
    private static int[] panelSize = {600, 200};
    private static int[] buttonSize = {100, 20};
    private static String[] tiposPeriodicidad = {"mensual", "anual"};
    private static String[] tiposBusqueda = {"numero", "titular", "saldo", "comision", "interes"};
    private String defaultFile = "db.dat";

    //Some internal variables
    private Database db = new Database(defaultFile);
    private Object actualObj = new Object();

    //Errors output string
    private static String error100 = "Cuenta no encontrada";
    private static String error101 = "No se ha podido determinar el tipo de cuenta";
    private static String error102 = "Hubo un problema con seleción de usuario";
    private static String error103 = "El valor introducido no es válido";
    private static String error104 = "No hubo ningún resultado";
    private static String error105 = "El valor introducido no es válido";

    private String[] colsCorriente = {"Numero", "Titular", "Fecha", "Saldo", "Comisión", "Periodicidad"};
    private String[] colsAhorro = {"Numero", "Titular", "Fecha", "Saldo", "Interes"};
    private String[] colsBuscar = {"Numero", "Tipo", "Titular", "Fecha", "Saldo", "Comisión", "Periodicidad", "Interes"};
    private DefaultTableModel modelCorriente = new DefaultTableModel(colsCorriente, 0) {
        @Override
        public boolean isCellEditable(int row, int column) {
            //all cells false
            return false;
        }
    };
    private DefaultTableModel modelAhorro = new DefaultTableModel(colsAhorro, 0) {
        @Override
        public boolean isCellEditable(int row, int column) {
            //all cells false
            return false;
        }
    };
    private DefaultTableModel modelBuscar = new DefaultTableModel(colsBuscar, 0) {
        @Override
        public boolean isCellEditable(int row, int column) {
            //all cells false
            return false;
        }
    };

    //cardPanel card names definions
    private static String DEFAULTPANEL = "Default Card";
    private static String CORRIENTEPANEL = "Corriente Card";
    private static String AHORROPANEL = "Ahorro Card";
    private static String SEARCHPANEL = "Search Panel";

    //Layout components of cardPanel
    private CardLayout layout;
    private JPanel defaultPan, corrientePan, ahorroPan, searchPan;

    //Layout components of defaultPan
    private JLabel defIntroLabel = new JLabel("Bienvenido a Banco Ui, su app "
            + "preferida para administación bancaria.", SwingConstants.CENTER);
    private JButton defCorrienteButton = new JButton("Nueva cuenta Corriente");
    private JButton defAhorroButton = new JButton("Nueva cuenta Ahorro");
    private JButton defExportButton = new JButton("Exportar Datos");
    private JButton defImportButton = new JButton("Importar Datos");

    //Layout components of corrientePan
    private JLabel corIntroLabel = new JLabel("Rellene el formulario y "
            + "pulse botón \"OK\" para crear una nueva cuenta Corriente.", SwingConstants.CENTER);
    private JLabel corNumeroLabel = new JLabel("Numero de cuenta: ");
    private JLabel corTitularLabel = new JLabel("Propietario: ");
    private JLabel corSaldoLabel = new JLabel("Saldo: ");
    private JLabel corComisionLabel = new JLabel("Comisión: ");
    private JLabel corPeriodicidadLabel = new JLabel("Periodicidad: ");
    private JTextField corNumeroField = new JTextField();
    private JTextField corTitularField = new JTextField();
    private JTextField corSaldoField = new JTextField();
    private JTextField corComisionField = new JTextField();
    private JComboBox<String> corPeriodicidadField = new JComboBox<>(tiposPeriodicidad);
    private JButton corVolverButton = new JButton("Volver");
    private JButton corAceptarButton = new JButton("Aceptár");

    //Layout components of ahorroPan
    private JLabel ahIntroLabel = new JLabel("Rellene el formulario y "
            + "pulse botón \"OK\" para crear una nueva cuenta Ahorro.", SwingConstants.CENTER);
    private JLabel ahNumeroLabel = new JLabel("Numero de cuenta: ");
    private JLabel ahTitularLabel = new JLabel("Propietario: ");
    private JLabel ahSaldoLabel = new JLabel("Saldo: ");
    private JLabel ahInteresLabel = new JLabel("Interes: ");
    private JTextField ahNumeroField = new JTextField();
    private JTextField ahTitularField = new JTextField();
    private JTextField ahSaldoField = new JTextField();
    private JTextField ahInteresField = new JTextField();
    private JButton ahVolverButton = new JButton("Volver");
    private JButton ahAceptarButton = new JButton("Aceptár");

    //Layout components of searchPan
    private JLabel searchIntroLabel = new JLabel("Introduzca un valor y "
            + "elija el tipo de cotejamiento, depues pulse botón \"Buscar\"", SwingConstants.CENTER);
    private JLabel searchValorLabel = new JLabel("Valor: ", SwingConstants.RIGHT);
    private JTextField searchValorField = new JTextField();
    private JLabel searchTiposLabel = new JLabel("Tipo:  ", SwingConstants.RIGHT);
    private JComboBox<String> searchTiposField = new JComboBox<>(tiposBusqueda);
    private JButton searchVolverButton = new JButton("Volver");
    private JButton searchBuscarButton = new JButton("Buscar");

    public Ui() {
        initComponents();
        initUi();
        initEventsHandlers();
    }

    private void initUi() {
        //JTabbedPane disable actual Tab
        tabbedPane.setEnabledAt(3, false);
        //JTables models init
        corrienteTab.setModel(modelCorriente);
        ahorroTab.setModel(modelAhorro);
        busquedaTab.setModel(modelBuscar);
        //Button group init
        ButtonGroup groupRadioBtn = new ButtonGroup();
        groupRadioBtn.add(actualIsCorriente);
        groupRadioBtn.add(actualIsAhorro);
        actualIsCorriente.setEnabled(false);
        actualIsAhorro.setEnabled(false);
        //CardLayout init
        layout = new CardLayout();
        cardPane.setLayout(layout);
        //defaultPan ui init             ***************************
        defaultPan = new JPanel();
        defaultPan.setLayout(new BorderLayout());
        //Inner pane
        JPanel defDataPanel = new JPanel();
        defDataPanel.setLayout(new GridLayout(3, 2));
        defDataPanel.add(new JLabel("Creación de Cuentas"));
        defDataPanel.add(new JLabel("Operaciónes Avanzadas"));
        defDataPanel.add(defCorrienteButton);
        defDataPanel.add(defExportButton);
        defDataPanel.add(defAhorroButton);
        defDataPanel.add(defImportButton);
        //Main panel adds
        defaultPan.add(defIntroLabel, BorderLayout.NORTH);
        defaultPan.add(defDataPanel, BorderLayout.SOUTH);
        //card creation
        cardPane.add(defaultPan, DEFAULTPANEL);

        //corrientePan ui init             ***************************
        corrientePan = new JPanel();
        corrientePan.setLayout(new BorderLayout());
        //Inner pane
        JPanel corDataPan = new JPanel();
        corDataPan.setLayout(new GridLayout(3, 4));
        corDataPan.add(corNumeroLabel);
        corDataPan.add(corNumeroField);
        corDataPan.add(corComisionLabel);
        corDataPan.add(corComisionField);
        corDataPan.add(corTitularLabel);
        corDataPan.add(corTitularField);
        corDataPan.add(corPeriodicidadLabel);
        corDataPan.add(corPeriodicidadField);
        corDataPan.add(corSaldoLabel);
        corDataPan.add(corSaldoField);
        corDataPan.add(corVolverButton);
        corDataPan.add(corAceptarButton);
        //Main panel adds
        corrientePan.add(corIntroLabel, BorderLayout.NORTH);
        corrientePan.add(corDataPan, BorderLayout.SOUTH);
        //card creation
        cardPane.add(corrientePan, CORRIENTEPANEL);

        //ahorroPan ui init             ***************************
        ahorroPan = new JPanel();
        ahorroPan.setLayout(new BorderLayout());
        ahorroPan.setSize(panelSize[0], panelSize[1]);
        //Inner pane
        JPanel ahDataPan = new JPanel();
        ahDataPan.setLayout(new GridLayout(3, 4));
        ahDataPan.add(ahNumeroLabel);
        ahDataPan.add(ahNumeroField);
        ahDataPan.add(ahInteresLabel);
        ahDataPan.add(ahInteresField);
        ahDataPan.add(ahTitularLabel);
        ahDataPan.add(ahTitularField);
        ahDataPan.add(ahVolverButton);
        ahDataPan.add(ahAceptarButton);
        ahDataPan.add(ahSaldoLabel);
        ahDataPan.add(ahSaldoField);
        //Main panel adds
        ahorroPan.add(ahIntroLabel, BorderLayout.NORTH);
        ahorroPan.add(ahDataPan, BorderLayout.SOUTH);
        //card creation
        cardPane.add(ahorroPan, AHORROPANEL);

        //searchPan ui init             ***************************
        searchPan = new JPanel();
        searchPan.setLayout(new BorderLayout());
        searchPan.setSize(panelSize[0], panelSize[1]);
        //Inner pane
        JPanel searchDataPan = new JPanel();
        searchDataPan.setLayout(new GridLayout(3, 4));
        searchDataPan.add(searchValorLabel);
        searchDataPan.add(searchValorField);
        searchDataPan.add(searchTiposLabel);
        searchDataPan.add(searchTiposField);
        searchDataPan.add(searchVolverButton);
        searchDataPan.add(searchBuscarButton);
        //Main panel adds
        searchPan.add(searchIntroLabel, BorderLayout.NORTH);
        searchPan.add(searchDataPan, BorderLayout.SOUTH);
        //card creation
        cardPane.add(searchPan, SEARCHPANEL);

        layout.show(cardPane, DEFAULTPANEL);
        tabbedPane.updateUI();
        db.recover();
        refreshTables();
    }

    private void initEventsHandlers() {
        corVolverButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            layout.show(cardPane, DEFAULTPANEL);
        });
        ahVolverButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            layout.show(cardPane, DEFAULTPANEL);
        });
        searchVolverButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            layout.show(cardPane, DEFAULTPANEL);
        });
        defCorrienteButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            tabbedPane.setSelectedIndex(0);
            layout.show(cardPane, CORRIENTEPANEL);
        });
        defAhorroButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            tabbedPane.setSelectedIndex(1);
            layout.show(cardPane, AHORROPANEL);
        });
        tabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                String tab = tabbedPane.getTitleAt(tabbedPane.getSelectedIndex());
                switch (tab) {
                    case ("Busqueda"):
                        layout.show(cardPane, SEARCHPANEL);
                        break;
                    default:
                        layout.show(cardPane, DEFAULTPANEL);
                        break;
                }
            }
        });

        corAceptarButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            if (corrienteValidate()) {
                createCorriente();
            }
        });
        ahAceptarButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            if (ahorroValidate()) {
                createAhorro();
            }
        });

        corrienteTab.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int number = (Integer) corrienteTab.getValueAt(corrienteTab.getSelectedRow(), 0);
                    setActual(number);
                }
            }
        });
        ahorroTab.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int number = (Integer) ahorroTab.getValueAt(ahorroTab.getSelectedRow(), 0);
                    setActual(number);
                }
            }
        });
        busquedaTab.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int number = (Integer) busquedaTab.getValueAt(busquedaTab.getSelectedRow(), 0);
                    setActual(number);
                }
            }
        });

        actualGuardar.addActionListener((java.awt.event.ActionEvent evt) -> {
            if (actualValidate()) {
                saveActual();
            }
        });
        actualEliminar.addActionListener((java.awt.event.ActionEvent evt) -> {
            deleteActual();
        });
        actualIngresar.addActionListener((java.awt.event.ActionEvent evt) -> {
            ingresarActual();
        });
        actualSacar.addActionListener((java.awt.event.ActionEvent evt) -> {
            sacarActual();
        });

        searchValorField.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                if (!(ke.getKeyChar() == 27 || ke.getKeyChar() == 65535)) {
                    buscarValidate();
                }
            }
        });
        searchTiposField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                buscarValidate();
            }
        });
        searchBuscarButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            buscar();
        });

        defImportButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            final JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter(".dat", "dat", "text");
            fc.setFileFilter(filter);
            int returnVal = fc.showDialog(this, "Import data form file");
            if (returnVal == 0) {
                db.recover(fc.getSelectedFile());
                refreshTables();
            }
        });
        defExportButton.addActionListener((java.awt.event.ActionEvent evt) -> {
            final JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter(".dat", "dat", "text");
            fc.setFileFilter(filter);
            fc.setSelectedFile(new File("db.dat"));
            int returnVal = fc.showDialog(this, "Export data to file");
            if (returnVal == 0) {
                db.save(fc.getSelectedFile());
                refreshTables();
            }
        });

        menuSave.addActionListener((java.awt.event.ActionEvent evt) -> {
            db.save();
        });
        menuExit.addActionListener((java.awt.event.ActionEvent evt) -> {
            int n = JOptionPane.showConfirmDialog(
                    this,
                    "Desea salir de la aplicación?",
                    "",
                    JOptionPane.YES_NO_OPTION);
            if (n == 0) {
                System.exit(0);
            }
        });
        menuNewCorriente.addActionListener((java.awt.event.ActionEvent evt) -> {
            tabbedPane.setSelectedIndex(0);
            layout.show(cardPane, CORRIENTEPANEL);
        });
        menuNewAhorro.addActionListener((java.awt.event.ActionEvent evt) -> {
            tabbedPane.setSelectedIndex(1);
            layout.show(cardPane, AHORROPANEL);
        });
        menuInfo.addActionListener((java.awt.event.ActionEvent evt) -> {
            JOptionPane.showMessageDialog(this,
                    "This app was made by Bohdan Shcherback\n"
                            + "2014 IES Majuelo Sevilla\n"
                            + "darkforce314@gmail.com");
        });

    }

    private void refreshTables() {
        LinkedList<Object> data = db.getCorrienteList();
        Iterator it = data.iterator();
        DefaultTableModel model = (DefaultTableModel) corrienteTab.getModel();
        model.setRowCount(0);
        while (it.hasNext()) {
            Corriente obj = (Corriente) it.next();
            model.addRow(obj.toArray());
            corrienteTab.setModel(model);
        }
        data = db.getAhorroList();
        it = data.iterator();
        model = (DefaultTableModel) ahorroTab.getModel();
        model.setRowCount(0);
        while (it.hasNext()) {
            Ahorro obj = (Ahorro) it.next();
            model.addRow(obj.toArray());
            ahorroTab.setModel(model);
        }
    }

    //New Corriente form valiation
    private boolean corrienteValidate() {
        boolean result = true;
        //Corriente number validation
        //-> numeric & not exists in db
        if (corNumeroField.getText().length() < 1) {
            corNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            corNumeroField.setForeground(Color.red);
            result = false;
        } else if (corNumeroField.getText() != "" && Functions.isNumeric(corNumeroField.getText()) == false) {
            corNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            corNumeroField.setForeground(Color.blue);
            result = false;
        } else if (corNumeroField.getText() != "" && db.exists(Integer.valueOf(corNumeroField.getText()))) {
            corNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.orange));
            corNumeroField.setForeground(Color.orange);
            result = false;
        } else {
            corNumeroField.setBorder(actualTabNumero.getBorder());
            corNumeroField.setForeground(Color.black);
        }
        //Corriente titular validation
        //-> only contains alphabet chars
        if (corTitularField.getText().length() < 1) {
            corTitularField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            corTitularField.setForeground(Color.red);
            result = false;
        } else if (corTitularField.getText() != "" && !Functions.isAlpha(corTitularField.getText())) {
            corTitularField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            corTitularField.setForeground(Color.blue);
            result = false;
        } else {
            corTitularField.setBorder(actualTabNumero.getBorder());
            corTitularField.setForeground(Color.black);
        }
        //Corriente saldo validation
        //-> float type compatible
        if (corSaldoField.getText().length() < 1) {
            corSaldoField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            corSaldoField.setForeground(Color.red);
            result = false;
        } else if (corSaldoField.getText() != "" && !Functions.isFloat(corSaldoField.getText())) {
            corSaldoField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            corSaldoField.setForeground(Color.blue);
            result = false;
        } else {
            corSaldoField.setBorder(actualTabNumero.getBorder());
            corSaldoField.setForeground(Color.black);
        }
        //Corriente comision validation
        //-> float type compatible
        if (corComisionField.getText().length() < 1) {
            corComisionField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            corComisionField.setForeground(Color.red);
            result = false;
        } else if (corComisionField.getText() != "" && !Functions.isFloat(corComisionField.getText())) {
            corComisionField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            corComisionField.setForeground(Color.blue);
            result = false;
        } else {
            corComisionField.setBorder(actualTabNumero.getBorder());
            corComisionField.setForeground(Color.black);
        }
        return result;
    }

    //New Ahorro form validation
    private boolean ahorroValidate() {
        boolean result = true;
        //Ahorro number validation
        //-> numeric & not exists in db
        if (ahNumeroField.getText().length() < 1) {
            ahNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            ahNumeroField.setForeground(Color.red);
            result = false;
        } else if (ahNumeroField.getText() != "" && Functions.isNumeric(ahNumeroField.getText()) == false) {
            ahNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            ahNumeroField.setForeground(Color.blue);
            result = false;
        } else if (ahNumeroField.getText() != "" && db.exists(Integer.valueOf(ahNumeroField.getText()))) {
            ahNumeroField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.orange));
            ahNumeroField.setForeground(Color.orange);
            result = false;
        } else {
            ahNumeroField.setBorder(actualTabNumero.getBorder());
            ahNumeroField.setForeground(Color.black);
        }
        //Ahorro titular validation
        //-> only contains alphabet chars
        if (ahTitularField.getText().length() < 1) {
            ahTitularField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            ahTitularField.setForeground(Color.red);
            result = false;
        } else if (ahTitularField.getText() != "" && !Functions.isAlpha(ahTitularField.getText())) {
            ahTitularField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            ahTitularField.setForeground(Color.blue);
            result = false;
        } else {
            ahTitularField.setBorder(actualTabNumero.getBorder());
            ahTitularField.setForeground(Color.black);
        }
        //Ahorro saldo validation
        //-> float type compatible
        if (ahSaldoField.getText().length() < 1) {
            ahSaldoField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            ahSaldoField.setForeground(Color.red);
            result = false;
        } else if (ahSaldoField.getText() != "" && !Functions.isFloat(ahSaldoField.getText())) {
            ahSaldoField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            ahSaldoField.setForeground(Color.blue);
            result = false;
        } else {
            ahSaldoField.setBorder(actualTabNumero.getBorder());
            ahSaldoField.setForeground(Color.black);
        }
        //Ahorro interés validation
        //-> float type compatible
        if (ahInteresField.getText().length() < 1) {
            ahInteresField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            ahInteresField.setForeground(Color.red);
            result = false;
        } else if (ahInteresField.getText() != "" && !Functions.isFloat(ahInteresField.getText())) {
            ahInteresField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            ahInteresField.setForeground(Color.blue);
            result = false;
        } else {
            ahInteresField.setBorder(actualTabNumero.getBorder());
            ahInteresField.setForeground(Color.black);
        }
        return result;
    }

    //New Corriente creation
    private void createCorriente() {
        db.createCorrinte(corTitularField.getText(),
                corSaldoField.getText(),
                corNumeroField.getText(),
                corComisionField.getText(),
                corPeriodicidadField.getSelectedItem().toString());
        refreshTables();
        layout.show(cardPane, DEFAULTPANEL);
        clearCorriente();
        db.save();
    }

    //New Ahorro creation
    private void createAhorro() {
        db.createAhorro(ahTitularField.getText(),
                ahSaldoField.getText(),
                ahNumeroField.getText(),
                ahInteresField.getText());
        refreshTables();
        layout.show(cardPane, DEFAULTPANEL);
        clearAhorro();
        db.save();
    }

    //Clear New Corrinete form
    private void clearCorriente() {
        corNumeroField.setText("");
        corNumeroField.setBorder(searchValorField.getBorder());
        corNumeroField.setForeground(Color.black);
        corTitularField.setText("");
        corTitularField.setBorder(searchValorField.getBorder());
        corTitularField.setForeground(Color.black);
        corSaldoField.setText("");
        corSaldoField.setBorder(searchValorField.getBorder());
        corSaldoField.setForeground(Color.black);
        corComisionField.setText("");
        corComisionField.setBorder(searchValorField.getBorder());
        corComisionField.setForeground(Color.black);
    }

    //Clear New Ahorro form
    private void clearAhorro() {
        ahNumeroField.setText("");
        ahNumeroField.setBorder(ahNumeroField.getBorder());
        ahNumeroField.setForeground(Color.black);
        ahTitularField.setText("");
        ahTitularField.setBorder(searchValorField.getBorder());
        ahTitularField.setForeground(Color.black);
        ahSaldoField.setText("");
        ahSaldoField.setBorder(searchValorField.getBorder());
        ahSaldoField.setForeground(Color.black);
        ahInteresField.setText("");
        ahInteresField.setBorder(searchValorField.getBorder());
        ahInteresField.setForeground(Color.black);
    }

    //Set actual object and tab with content
    private void setActual(int number) {
        clearActual();
        actualObj = db.searchCuenta(number);
        if (actualObj instanceof Corriente) {
            Corriente obj = (Corriente) actualObj;
            tabbedPane.setEnabledAt(3, true);

            actualTabNumero.setEditable(false);
            actualTabComision.setEnabled(true);
            actualTabPeriodicidad.setEnabled(true);
            actualTabInteres.setEnabled(false);

            actualTabNumero.setText(String.valueOf(obj.numero));
            actualTabTitular.setText(obj.titular);
            actualTabSaldo.setText(String.valueOf(obj.getSaldo()));
            actualTabComision.setText(String.valueOf(obj.getComision()));
            if (obj.getPeriodicidad().equals("mensual")) {
                actualTabPeriodicidad.setSelectedIndex(0);
            } else {
                actualTabPeriodicidad.setSelectedIndex(1);
            }

            actualTabInteres.setText("");

            actualIsCorriente.setSelected(true);
            tabbedPane.setSelectedIndex(3);
        } else if (actualObj instanceof Ahorro) {
            Ahorro obj = (Ahorro) actualObj;
            tabbedPane.setEnabledAt(3, true);

            actualTabNumero.setEditable(false);
            actualTabComision.setEnabled(false);
            actualTabPeriodicidad.setEnabled(false);
            actualTabInteres.setEnabled(true);

            actualTabNumero.setText(String.valueOf(obj.numero));
            actualTabTitular.setText(obj.titular);
            actualTabSaldo.setText(String.valueOf(obj.saldo));
            actualTabInteres.setText(String.valueOf(obj.getInteres()));

            actualTabComision.setText("");

            actualIsAhorro.setSelected(true);
            tabbedPane.setSelectedIndex(3);
        } else {
            tabbedPane.setEnabledAt(3, false);
            JOptionPane.showMessageDialog(this,
                    error100, "Error", JOptionPane.ERROR_MESSAGE);
        }
        this.repaint();
    }

    //validate Actual form before save
    private boolean actualValidate() {
        boolean result = true;

        //Actual titular validation
        //-> only contains alphabet chars
        if (actualTabTitular.getText().length() < 1) {
            actualTabTitular.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            actualTabTitular.setForeground(Color.red);
            result = false;
        } else if (actualTabTitular.getText() != "" && !Functions.isAlpha(actualTabTitular.getText())) {
            actualTabTitular.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            actualTabTitular.setForeground(Color.blue);
            result = false;
        } else {
            actualTabTitular.setBorder(actualTabNumero.getBorder());
            actualTabTitular.setForeground(Color.black);
        }
        //Actual saldo validation
        //-> float type compatible
        if (actualTabSaldo.getText().length() < 1) {
            actualTabSaldo.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
            actualTabSaldo.setForeground(Color.red);
            result = false;
        } else if (actualTabSaldo.getText() != "" && !Functions.isFloat(actualTabSaldo.getText())) {
            actualTabSaldo.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
            actualTabSaldo.setForeground(Color.blue);
            result = false;
        } else {
            actualTabSaldo.setBorder(actualTabNumero.getBorder());
            actualTabSaldo.setForeground(Color.black);
        }

        if (actualIsCorriente.isSelected()) {
            //Actual comisión validation
            //-> float type compatible
            if (actualTabComision.getText().length() < 1) {
                actualTabComision.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
                actualTabComision.setForeground(Color.red);
                result = false;
            } else if (actualTabComision.getText() != "" && !Functions.isFloat(actualTabComision.getText())) {
                actualTabComision.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
                actualTabComision.setForeground(Color.blue);
                result = false;
            } else {
                actualTabComision.setBorder(actualTabNumero.getBorder());
                actualTabComision.setForeground(Color.black);
            }
        } else if (actualIsAhorro.isSelected()) {
            //Actual interés validation
            //-> float type compatible
            if (actualTabInteres.getText().length() < 1) {
                actualTabInteres.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
                actualTabInteres.setForeground(Color.red);
                result = false;
            } else if (actualTabInteres.getText() != "" && !Functions.isFloat(actualTabInteres.getText())) {
                actualTabInteres.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.blue));
                actualTabInteres.setForeground(Color.blue);
                result = false;
            } else {
                actualTabInteres.setBorder(actualTabNumero.getBorder());
                actualTabInteres.setForeground(Color.black);
            }
        }
        return result;
    }

    //Clear Actual form
    private void clearActual() {
        actualTabNumero.setText("");
        actualTabNumero.setBorder(ahNumeroField.getBorder());
        actualTabNumero.setForeground(Color.black);
        actualTabTitular.setText("");
        actualTabTitular.setBorder(searchValorField.getBorder());
        actualTabTitular.setForeground(Color.black);
        actualTabSaldo.setText("");
        actualTabSaldo.setBorder(searchValorField.getBorder());
        actualTabSaldo.setForeground(Color.black);
        actualTabComision.setText("");
        actualTabComision.setBorder(searchValorField.getBorder());
        actualTabComision.setForeground(Color.black);
        actualTabInteres.setText("");
        actualTabInteres.setBorder(searchValorField.getBorder());
        actualTabInteres.setForeground(Color.black);
    }

    //Save Actual form status
    private void saveActual() {
        if (actualObj instanceof Corriente) {
            Corriente obj = (Corriente) actualObj;
            db.remove(obj.numero);
            db.createCorrinte(actualTabTitular.getText(),
                    actualTabSaldo.getText(),
                    actualTabNumero.getText(),
                    actualTabComision.getText(),
                    actualTabPeriodicidad.getSelectedItem().toString());
            db.save();
            refreshTables();
            JOptionPane.showMessageDialog(this,
                    "Se han guardado los datos de la cuenta " + obj.numero);
        } else if (actualObj instanceof Ahorro) {
            Ahorro obj = (Ahorro) actualObj;
            db.remove(obj.numero);
            db.createAhorro(actualTabTitular.getText(),
                    actualTabSaldo.getText(),
                    actualTabNumero.getText(),
                    actualTabInteres.getText());
            db.save();
            refreshTables();
            JOptionPane.showMessageDialog(this,
                    "Se han guardado los datos de la cuenta " + obj.numero);
        } else {
            JOptionPane.showMessageDialog(this,
                    error101, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    //Delete Actual
    private void deleteActual() {
        Cuenta obj = (Cuenta) actualObj;
        int n = JOptionPane.showConfirmDialog(
                this,
                "Desea borrar la cuenta " + obj.numero + " ?",
                "",
                JOptionPane.YES_NO_OPTION);
        if (n == 0) {
            db.remove(obj.numero);
            db.save();
            refreshTables();
            tabbedPane.setSelectedIndex(0);
            tabbedPane.setEnabledAt(3, false);
            actualObj = null;
        } else if (n == 1) {
            //nothin
        } else {
            JOptionPane.showMessageDialog(this,
                    error102, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    //Ingresar Actual dialog
    private void ingresarActual() {
        String response = JOptionPane.showInputDialog(this,
                "Introduzca la cantidad: ", "Cuanto dinero desea ingresar?",
                JOptionPane.QUESTION_MESSAGE);
        if (Functions.isFloat(response)) {
            if (actualObj instanceof Corriente) {
                Corriente obj = (Corriente) actualObj;
                obj.setSaldo(obj.getSaldo() + Float.valueOf(response));

                db.remove(obj.numero);
                db.createCorrinte(String.valueOf(obj.titular),
                        String.valueOf(obj.saldo),
                        String.valueOf(obj.numero),
                        String.valueOf(obj.comision),
                        obj.periodicidad);
                db.save();
                refreshTables();
                setActual(obj.numero);
            } else if (actualObj instanceof Ahorro) {
                Ahorro obj = (Ahorro) actualObj;
                obj.setSaldo(obj.getSaldo() + Float.valueOf(response));

                db.remove(obj.numero);
                db.createAhorro(String.valueOf(obj.titular),
                        String.valueOf(obj.saldo),
                        String.valueOf(obj.numero),
                        String.valueOf(obj.interes));
                db.save();
                refreshTables();
                setActual(obj.numero);
            } else {
                JOptionPane.showMessageDialog(this,
                        error101, "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (!Functions.isFloat(response)) {
            JOptionPane.showMessageDialog(this,
                    error103, "Error", JOptionPane.ERROR_MESSAGE);
        } else if (response == null) {
            //nothin 
        }
    }

    //Sacar Actual dialog
    private void sacarActual() {
        String response = JOptionPane.showInputDialog(this,
                "Introduzca la cantidad: ", "Cuanto dinero desea sacar?",
                JOptionPane.QUESTION_MESSAGE);
        if (Functions.isFloat(response)) {
            if (actualObj instanceof Corriente) {
                Corriente obj = (Corriente) actualObj;
                obj.setSaldo(obj.getSaldo() - Float.valueOf(response));

                db.remove(obj.numero);
                db.createCorrinte(String.valueOf(obj.titular),
                        String.valueOf(obj.saldo),
                        String.valueOf(obj.numero),
                        String.valueOf(obj.comision),
                        obj.periodicidad);
                db.save();
                refreshTables();
                setActual(obj.numero);
            } else if (actualObj instanceof Ahorro) {
                Ahorro obj = (Ahorro) actualObj;
                obj.setSaldo(obj.getSaldo() - Float.valueOf(response));

                db.remove(obj.numero);
                db.createAhorro(String.valueOf(obj.titular),
                        String.valueOf(obj.saldo),
                        String.valueOf(obj.numero),
                        String.valueOf(obj.interes));
                db.save();
                refreshTables();
                setActual(obj.numero);
            } else {
                JOptionPane.showMessageDialog(this,
                        error101, "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (!Functions.isFloat(response)) {
            JOptionPane.showMessageDialog(this,
                    error103, "Error", JOptionPane.ERROR_MESSAGE);
        } else if (response == null) {
            //nothin 
        }
    }

    //Buscar form validation
    private boolean buscarValidate() {
        boolean result = true;
        String type = searchTiposField.getSelectedItem().toString();
        switch (type) {
            case ("numero"):
                if (!Functions.isNumeric(searchValorField.getText())) {
                    searchValorField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
                    searchValorField.setForeground(Color.red);
                    result = false;
                } else {
                    searchValorField.setBorder(actualTabNumero.getBorder());
                    searchValorField.setForeground(Color.black);
                }
                break;
            case ("titular"):
                if (!Functions.isAlpha(searchValorField.getText())) {
                    searchValorField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
                    searchValorField.setForeground(Color.red);
                    result = false;
                } else {
                    searchValorField.setBorder(actualTabNumero.getBorder());
                    searchValorField.setForeground(Color.black);
                }
                break;
            default:
                if (!Functions.isFloat(searchValorField.getText())) {
                    searchValorField.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.red));
                    searchValorField.setForeground(Color.red);
                    result = false;
                } else {
                    searchValorField.setBorder(actualTabNumero.getBorder());
                    searchValorField.setForeground(Color.black);
                }
                break;
        }
        return result;
    }

    //Buscar action
    private void buscar() {
        if (buscarValidate()) {
            LinkedList<Object> result = db.multiSearchCuenta(searchValorField.getText(),
                    searchTiposField.getSelectedItem().toString());
            if (result.size() > 0) {
                Iterator it = result.iterator();
                DefaultTableModel model = (DefaultTableModel) busquedaTab.getModel();
                model.setRowCount(0);
                while (it.hasNext()) {
                    Object obj = it.next();
                    if (obj instanceof Corriente) {
                        Corriente aux = (Corriente) obj;
                        Object[] data = {aux.numero,
                            "Corriente",
                            aux.titular,
                            aux.getFechaAperturaString(),
                            aux.getSaldo(),
                            aux.getComision(),
                            aux.getPeriodicidad(),
                            ""};
                        model.addRow(data);
                    } else if (obj instanceof Ahorro) {
                        Ahorro aux = (Ahorro) obj;
                        Object[] data = {aux.numero,
                            "Ahorro",
                            aux.titular,
                            aux.getFechaAperturaString(),
                            aux.getSaldo(),
                            "",
                            "",
                            aux.getInteres()};
                        model.addRow(data);
                    } else {
                        JOptionPane.showMessageDialog(this,
                                error101, "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                busquedaTab.setModel(model);
            } else {
                JOptionPane.showMessageDialog(this,
                        error104,
                        "Warning",
                        JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,
                    error105, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        corrienteTab = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        ahorroTab = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        busquedaTab = new javax.swing.JTable();
        actualTab = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        actualTabNumero = new javax.swing.JTextField();
        actualTabTitular = new javax.swing.JTextField();
        actualTabSaldo = new javax.swing.JTextField();
        actualTabComision = new javax.swing.JTextField();
        actualTabInteres = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        actualEliminar = new javax.swing.JButton();
        actualGuardar = new javax.swing.JButton();
        actualTabPeriodicidad = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        actualIsCorriente = new javax.swing.JRadioButton();
        actualIsAhorro = new javax.swing.JRadioButton();
        actualSacar = new javax.swing.JButton();
        actualIngresar = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        cardPane = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuSave = new javax.swing.JMenuItem();
        menuExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuNewCorriente = new javax.swing.JMenuItem();
        menuNewAhorro = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        menuInfo = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabbedPane.setName("tabbedPane"); // NOI18N

        corrienteTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(corrienteTab);

        tabbedPane.addTab("Corriente", jScrollPane1);

        ahorroTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(ahorroTab);

        tabbedPane.addTab("Ahorro", jScrollPane2);

        busquedaTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(busquedaTab);

        tabbedPane.addTab("Busqueda", jScrollPane3);

        actualTab.setName("actualTab"); // NOI18N

        jLabel1.setText("Numero");

        jLabel2.setText("Titular");

        jLabel3.setText("Saldo");

        jLabel4.setText("Interes");

        jLabel5.setText("Comision");

        jLabel6.setText("Periodicidad");

        actualEliminar.setText("Eliminar");

        actualGuardar.setText("Guardar");

        actualTabPeriodicidad.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "mensual", "anual" }));

        jLabel7.setText("Tipo: ");

        actualIsCorriente.setText("Corriente");
        actualIsCorriente.setFocusable(false);

        actualIsAhorro.setText("Ahorro");

        actualSacar.setText("Sacar");

        actualIngresar.setText("Ingresar");

        javax.swing.GroupLayout actualTabLayout = new javax.swing.GroupLayout(actualTab);
        actualTab.setLayout(actualTabLayout);
        actualTabLayout.setHorizontalGroup(
            actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(actualTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(actualTabLayout.createSequentialGroup()
                        .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(58, 58, 58)
                        .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(actualTabLayout.createSequentialGroup()
                                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(actualTabTitular, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                                    .addComponent(actualTabNumero))
                                .addGap(43, 43, 43)
                                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(actualTabPeriodicidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(actualTabComision)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, actualTabLayout.createSequentialGroup()
                                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(actualTabSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(actualTabInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(actualTabLayout.createSequentialGroup()
                                        .addComponent(actualIsCorriente)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(actualIsAhorro)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 200, Short.MAX_VALUE)
                                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(actualSacar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(actualTabLayout.createSequentialGroup()
                                        .addComponent(actualGuardar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(actualEliminar))
                                    .addComponent(actualIngresar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(actualTabLayout.createSequentialGroup()
                        .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel7))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        actualTabLayout.setVerticalGroup(
            actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(actualTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(actualTabNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actualTabComision, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(actualTabTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(actualTabPeriodicidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(actualTabSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actualIngresar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(actualTabInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actualSacar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(actualTabLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(actualEliminar)
                            .addComponent(actualGuardar)))
                    .addGroup(actualTabLayout.createSequentialGroup()
                        .addGroup(actualTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(actualIsCorriente)
                            .addComponent(actualIsAhorro))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tabbedPane.addTab("Actual", actualTab);

        cardPane.setPreferredSize(new java.awt.Dimension(600, 200));

        javax.swing.GroupLayout cardPaneLayout = new javax.swing.GroupLayout(cardPane);
        cardPane.setLayout(cardPaneLayout);
        cardPaneLayout.setHorizontalGroup(
            cardPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        cardPaneLayout.setVerticalGroup(
            cardPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );

        jMenu1.setText("File");

        menuSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        menuSave.setText("Save");
        jMenu1.add(menuSave);

        menuExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        menuExit.setText("Exit");
        jMenu1.add(menuExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        menuNewCorriente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuNewCorriente.setText("New Corriente");
        jMenu2.add(menuNewCorriente);

        menuNewAhorro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuNewAhorro.setText("New Ahorro");
        jMenu2.add(menuNewAhorro);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("About");

        menuInfo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        menuInfo.setText("Information");
        jMenu3.add(menuInfo);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tabbedPane)
                    .addComponent(progressBar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cardPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cardPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actualEliminar;
    private javax.swing.JButton actualGuardar;
    private javax.swing.JButton actualIngresar;
    private javax.swing.JRadioButton actualIsAhorro;
    private javax.swing.JRadioButton actualIsCorriente;
    private javax.swing.JButton actualSacar;
    private javax.swing.JPanel actualTab;
    private javax.swing.JTextField actualTabComision;
    private javax.swing.JTextField actualTabInteres;
    private javax.swing.JTextField actualTabNumero;
    private javax.swing.JComboBox actualTabPeriodicidad;
    private javax.swing.JTextField actualTabSaldo;
    private javax.swing.JTextField actualTabTitular;
    private javax.swing.JTable ahorroTab;
    private javax.swing.JTable busquedaTab;
    private javax.swing.JPanel cardPane;
    private javax.swing.JTable corrienteTab;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JMenuItem menuExit;
    private javax.swing.JMenuItem menuInfo;
    private javax.swing.JMenuItem menuNewAhorro;
    private javax.swing.JMenuItem menuNewCorriente;
    private javax.swing.JMenuItem menuSave;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables
}
