/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import static bin.Launcher.toBufferedImage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author darkforce314
 */
public class Story {

    String name = "Default Story";
    String description = "Default Story description";
    String intro = "defaultIntro.jpg";          //Any image with >= 600x400px
    String map = "defaultMap.jpg";              //Any image with >= 600x400px
    String mainPj = "defaultMainPj";            //Any "PNG" image with == 40x60px
    String dialogBox = "dialogBox.png";         //Any image with == 600x50px
    Image introImage, mapImage, dialogBoxImage, mainPjImg;
    BufferedImage mapBuffered;
    LinkedList<String> pjs = new LinkedList<>();
    LinkedList<Image> pjsImg = new LinkedList<>();
    LinkedList<Step> steps = new LinkedList<>();

    int status = 0;
    int cX = 0;
    int cY = 0;
    int nX = 0;
    int nY = 0;
    int timePassed = 0;
    int timeSpeed = 100;
    boolean showingDialog = false;

    public Story() {
    }

    public Story(String name) {

        File jsonFile = new File(getClass().getResource("/storylines/" + name + ".json").getFile());
        JSONParser parser = new JSONParser();
        try {

            Object obj = parser.parse(new FileReader(jsonFile));

            JSONObject jsonStory = (JSONObject) obj;
            //Getting "Story" params
            this.name = (String) jsonStory.get("name");
            this.description = (String) jsonStory.get("description");
            this.intro = (String) jsonStory.get("intro");
            this.map = (String) jsonStory.get("map");
            this.mainPj = (String) jsonStory.get("mainPj");

            //Getting all "pj" instances of the story
            JSONArray jsonPjs = (JSONArray) jsonStory.get("pjs");
            Iterator<String> iterator = jsonPjs.iterator();
            while (iterator.hasNext()) {
                this.pjs.add(iterator.next());
            }

            //looping throw all "steps"
            JSONArray stepsArr = (JSONArray) jsonStory.get("steps");
            for (int i = 0; i < stepsArr.size(); i++) {
                JSONObject jsonStep = (JSONObject) stepsArr.get(i);
                //Getting "Step" params 
                String location = (String) jsonStep.get("location");
                int x = Integer.valueOf((String) jsonStep.get("x"));
                int y = Integer.valueOf((String) jsonStep.get("y"));
                String message = (String) jsonStep.get("message");

                LinkedList<Dialog> dialogs = new LinkedList<>();
                //looping throw all "dialogs" 
                JSONArray dialogsArr = (JSONArray) jsonStep.get("dialogs");
                for (int j = 0; j < dialogsArr.size(); j++) {
                    JSONObject jsonDialog = (JSONObject) dialogsArr.get(j);
                    //Getting "Dialog" params
                    String pj = (String) jsonDialog.get("pj");
                    String dmessage = (String) jsonDialog.get("message");

                    Dialog dialogObj = new Dialog(pj, dmessage);
                    dialogs.add(dialogObj);
                }
                Step stepObj = new Step(x, y, location, message, dialogs);
                steps.add(stepObj);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void loadImages() {
        ClassLoader cl = this.getClass().getClassLoader();
        this.map = cl.getResource("images/" + this.map).getPath();
        this.intro = cl.getResource("images/" + this.intro).getPath();
        this.dialogBox = cl.getResource("images/" + this.dialogBox).getPath();
        this.mainPj = cl.getResource("pjs/" + this.mainPj + ".png").getPath();
        try {
            this.mapImage = ImageIO.read(new File(this.map));
            this.introImage = ImageIO.read(new File(this.intro));
            this.dialogBoxImage = ImageIO.read(new File(this.dialogBox));
            this.mainPjImg = ImageIO.read(new File(this.mainPj));
            for (int i = 0; i < pjs.size(); i++) {
                String aux = cl.getResource("pjs/" + this.pjs.get(i) + ".png").getPath();
                this.pjsImg.add(ImageIO.read(new File(aux)));
            }

            this.mapBuffered = toBufferedImage(mapImage);
        } catch (IOException ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        String result = "****************************\n\r";
        result += "Nombre: " + name + "\n\r";
        result += "Descripción: " + description + "\n\r";
        result += "Steps >>> \n\r";
        for (int i = 0; i < steps.size(); i++) {
            result += steps.get(i).toString() + "\n\r";
        }
        return result;
    }

    public void paint(Graphics g, Launcher content) {
        if (status == 0) {
            g.drawImage(introImage, 0, 0, 600, 400, content);
            status = 1;
            cX = steps.get(status - 1).x;
            cY = steps.get(status - 1).y;
            nX = steps.get(status).x;
            nY = steps.get(status).y;
            try {
                Thread.sleep(1500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Story.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            travel(g, content);
        }
    }

    private void travel(Graphics g, Launcher content) {
        if (status >= 1) {
            if (timePassed < timeSpeed) {
                if (cX < nX) {
                    cX++;
                }
                if (cY < nY) {
                    cY++;
                }
                BufferedImage img = mapBuffered.getSubimage(cX - 160, cY - 110, 300, 200);
                g.drawImage(img, 0, 0, 600, 400, null);
                g.drawImage(mainPjImg, content.getWidth() / 2 - (mainPjImg.getWidth(content) / 2), content.getHeight() / 2 - (mainPjImg.getHeight(content) / 2), content);
                timePassed++;
                if (cX == nX && cY == nY) {
                    showingDialog = true;
                    showDialog(g, content);
                }
            }
            if (timePassed == timeSpeed) {
                timePassed = 0;
                if (status < steps.size() && showingDialog == false) {
                    status++;
                    nX = steps.get(status - 1).x;
                    nY = steps.get(status - 1).y;
                }
            }
        }
    }

    private void showDialog(Graphics g, Launcher content) {
        
        g.drawImage(dialogBoxImage, 0,
                content.getHeight() - dialogBoxImage.getHeight(content),
                content.getWidth(),
                dialogBoxImage.getHeight(content),
                content);
        g.setColor(Color.WHITE);
        g.drawString("Hola que ase", 20, content.getHeight() - 20);
    }
}
