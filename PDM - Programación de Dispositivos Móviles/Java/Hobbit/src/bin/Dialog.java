/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

/**
 *
 * @author darkforce314
 */
public class Dialog {
    
    private String pjName = "default";
    String message = "Default message";

    public Dialog() {
    }

    public Dialog(String pjName, String message) {
        this.pjName = pjName;
        this.message= message;
    }
    
    public Image getPjImage(){        
        Toolkit tk = Toolkit.getDefaultToolkit();
        URL resource = Dialog.class.getClassLoader().getResource("pjs/"+this.pjName+".png");
        return tk.createImage(resource);
    }
    public String getPjName(){
        String result;
        switch(this.pjName){
            case("bilbo"):
                result = "Bilbo Bolsón";
                break;
            case("gandalf"):
                result = "Gandalf el Gris";
                break;
            case("thorin"):
                result = "Thorin Escudo de Roble";
                break;
            case("smaug"):
                result = "Smaug el Terrible";
                break;
            default:
                result = "Default";
                break;
        }
        return result;
    }
    
    @Override
    public String toString(){
        return this.getPjName()+" : "+this.message;
    }
}
