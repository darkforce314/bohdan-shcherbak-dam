/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import java.util.LinkedList;

/**
 *
 * @author darkforce314
 */
public class Step {

    int x = 0;
    int y = 0;
    String location = "Default location";
    String message = "Default message";
    LinkedList<Dialog> dialogs = new LinkedList<>();

    public Step() {
    }

    public Step(int x, int y, String location, String message, LinkedList<Dialog> dialogs) {
        this.x = x;
        this.y = y;
        this.location = location;
        this.message = message;
        this.dialogs = dialogs;
    }

    @Override
    public String toString() {
        String result = "---------------\n\r";
        result += "X = " + x + "\n\r";
        result += "Y = " + y + "\n\r";
        result += "Location: " + location + "\n\r";
        result += "Message: " + message + "\n\r";
        result += "Dialogs >>> \n\r";
        for (int i = 0; i < dialogs.size(); i++) {
            result += dialogs.get(i).toString() + "\n\r";;
        }
        return result;
    }
}
