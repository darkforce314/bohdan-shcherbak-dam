/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author darkforce314
 */
public class Launcher extends Applet implements Runnable {

    private Image i;
    private Graphics doubleG;

    private int mapX = 450;
    private int mapY = 300;

    private Story storyline;

    @Override
    public void init() {
        setSize(600, 400);

        storyline = new Story("hobbit1");
        storyline.loadImages();
        /*
         ClassLoader cl = this.getClass().getClassLoader();
         String mapPath = cl.getResource("images/mapHD.jpg").getPath();
         String introPath = cl.getResource("images/hobbit.jpg").getPath();
         String dialogBoxPath = cl.getResource("images/dialogBox.png").getPath();

         String bilboPath = cl.getResource("pjs/bilbo.png").getPath();
         try {
         this.map = ImageIO.read(new File(mapPath));
         this.intro = ImageIO.read(new File(introPath));
         this.dialogBox = ImageIO.read(new File(dialogBoxPath));

         this.bilbo = ImageIO.read(new File(bilboPath));

         this.mapBuffered = toBufferedImage(map);
         } catch (IOException ex) {
         Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
    }

    @Override
    public void start() {
        Thread fps = new Thread(this);
        fps.start();
    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void paint(Graphics g) {
        /*
        BufferedImage img = storyline.mapBuffered.getSubimage(mapX, mapY, 300, 200);
        //map = Toolkit.getDefaultToolkit().createImage(img.getSource());
        //**g.drawImage(map, 0, 0, this); - metodo no responsivo
        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this); //metodo responsivo
        g.drawImage(storyline.dialogBoxImage, 0, 0,
                getWidth(),
                storyline.dialogBoxImage.getHeight(this),
                this);
        g.drawImage(storyline.dialogBoxImage, 0,
                this.getHeight() - storyline.dialogBoxImage.getHeight(this),
                getWidth(),
                storyline.dialogBoxImage.getHeight(this),
                this);
        g.setColor(Color.WHITE);
        g.drawString("Hola que ase", 20, this.getHeight() - 20);
        g.drawImage(storyline.mainPjImg, this.getWidth() / 2 - (storyline.mainPjImg.getWidth(this) / 2), this.getHeight() / 2 - (storyline.mainPjImg.getHeight(this) / 2), this);
        */
        storyline.paint(g,this);
    }

    @Override
    public void update(Graphics g) {
        //Double buffering prevents flickering beetwen frames
        if (i == null) {
            i = createImage(this.getSize().width, this.getSize().height);
            doubleG = i.getGraphics();
        }

        doubleG.setColor(getBackground());
        doubleG.fillRect(0, 0, this.getSize().width, this.getSize().height);

        doubleG.setColor(getForeground());
        paint(doubleG);

        g.drawImage(i, 0, 0, this);
    }

    @Override
    public void run() {
        while (true) {
             mapX += 1;
             mapY += 1;
            repaint();
            try {
                //17ms for 60 fps
                Thread.sleep(17);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
}
