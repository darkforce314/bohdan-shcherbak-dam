window.onload = function (){
	var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf('/')+1);
	if (filename == "list.html") {
		listNotas();
	};
}

function listNotas() {
	if (localStorage["notes"]) {
		var storedNotes = JSON.parse(localStorage["notes"]);
		var container = document.getElementById("notas");
		for (var i = storedNotes.length - 1; i >= 0; i--) {
			container.innerHTML = container.innerHTML + "<div class='nota'><label name = 'titulo'>"+storedNotes[i].titulo+"</label> <label name = 'contenido'>"+storedNotes[i].contenido+"</label> <label name = 'fecha'>"+storedNotes[i].fecha+"</label></div>";
		};
	}
}

function guardar() {
	var titulo = document.getElementById("titulo").value;
	var contenido = document.getElementById("contenido").value;
	var fecha = document.getElementById("fecha").valueAsDate;

	if (localStorage["notes"]) {
		var storedNotes = JSON.parse(localStorage["notes"]);
		storedNotes.push(
			{titulo: titulo, contenido: contenido, fecha: fecha}
		);
		localStorage["notes"] = JSON.stringify(storedNotes);
		console.log("Guradado en lista existente");
	}else{
		var aux = [];
		aux.push(
			{titulo: titulo, contenido: contenido, fecha: fecha}
		);
		localStorage["notes"] = JSON.stringify(aux);
		console.log("Guradado en nueva lista");
	}
	window.location.href = "list.html";
}

function limpiar() {
	localStorage.removeItem("notes");
}